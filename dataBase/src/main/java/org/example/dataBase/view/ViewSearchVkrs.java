package org.example.dataBase.view;

import org.example.dataBase.controller.VkrsController;
import org.example.dataBase.model.VkrsEntity;
import org.example.dataBase.service.VkrsService;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;

public class ViewSearchVkrs extends JFrame {
    VkrsController vkrsController;
    static int WINDOW_WIDTH = 1350;
    static int WINDOW_HEIGHT = 750;
    static int PANAL_WIDTH = 1100;
    static int PANAL_HEIGHT = 70;
    static int BUTTON_WIDTH = 350;
    static int BUTTON_HEIGHT = 50;

    public ViewSearchVkrs(VkrsController vkrsController1){
        this.vkrsController = vkrsController1;
    }
    int colorC = 0;

    public void visible(ArrayList<VkrsEntity> vkrsEntities)throws SQLException {
        Color color = new Color (240, 240, 240);
        Container c = getContentPane();
        c.setLayout(new BorderLayout());
        setTitle("Просмотр ВКР");
        setMinimumSize(new Dimension(WINDOW_WIDTH, WINDOW_HEIGHT));
        setResizable(false);

        JPanel panel = new JPanel();
        panel.setBorder(new EmptyBorder(10, 10, 10, 10));
        GridBagLayout gridBagLayout = new GridBagLayout();
        gridBagLayout.rowHeights = new int[]{BUTTON_HEIGHT, BUTTON_HEIGHT,BUTTON_HEIGHT,BUTTON_HEIGHT,BUTTON_HEIGHT };
        gridBagLayout.columnWeights = new double[]{1.0};
        gridBagLayout.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0};
        panel.setLayout(gridBagLayout);

        GridBagConstraints gbc = new GridBagConstraints();
        gbc.weightx = 1;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.gridwidth = GridBagConstraints.REMAINDER;
        c.add(panel);
        JScrollPane jScrollPane = new JScrollPane(panel);
        jScrollPane.setPreferredSize(new Dimension(WINDOW_WIDTH, WINDOW_HEIGHT));

        jScrollPane.setLayout(new ScrollPaneLayout());
        c.add(jScrollPane);
        jScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);

        panel.add(article(), gbc);
        Iterator<VkrsEntity> iter = vkrsEntities.iterator();
        while(iter.hasNext()){
            panel.add(createNewJPanel(iter.next()), gbc);
        }
        panel.add(createBut(), gbc);
        panel.setBackground(color);
        pack();
        setPreferredSize(new Dimension(WINDOW_WIDTH, WINDOW_HEIGHT));
        setVisible(true);
    }

    private JPanel createNewJPanel(VkrsEntity model) {
        JPanel pan = new JPanel();
        pan.setPreferredSize(new Dimension(PANAL_WIDTH, PANAL_HEIGHT));
        pan.setLayout(null);
        int otst = 20;
        JLabel fioJ = new JLabel(model.getFIOStudent());
        fioJ.setBounds(otst, PANAL_HEIGHT - BUTTON_HEIGHT - 10, 3*BUTTON_WIDTH , BUTTON_HEIGHT);
        fioJ.setFont(new Font("Times New Roman", NORMAL, 15));
        fioJ.setBackground(Color.gray);
        fioJ.setForeground(Color.DARK_GRAY);
        pan.add(fioJ);
        otst += 3*BUTTON_WIDTH;

        JLabel themeJ = new JLabel(model.getTheme());
        themeJ.setBounds(otst, PANAL_HEIGHT - BUTTON_HEIGHT - 10, 3*BUTTON_WIDTH , BUTTON_HEIGHT);
        themeJ.setFont(new Font("Times New Roman", NORMAL, 15));
        themeJ.setBackground(Color.gray);
        themeJ.setForeground(Color.DARK_GRAY);
        pan.add(themeJ);

        colorC++;
        Color col1 = new Color(250, 250, 250);
        Color col2 = new Color(225, 225, 225);
        if(colorC%2 == 0){
            pan.setBackground(col1);
        }else pan.setBackground(col2 );
        return pan;
    }

    private JPanel article(){
        JPanel pan = new JPanel();
        pan.setPreferredSize(new Dimension(PANAL_WIDTH, PANAL_HEIGHT));
        pan.setLayout(null);
        int otst = 20;
        JLabel fioJ = new JLabel("ФИО студента");
        fioJ.setBounds(otst, PANAL_HEIGHT - BUTTON_HEIGHT - 10, 3*BUTTON_WIDTH , BUTTON_HEIGHT);
        fioJ.setFont(new Font("Times New Roman", NORMAL, 15));
        fioJ.setBackground(Color.gray);
        fioJ.setForeground(Color.DARK_GRAY);
        pan.add(fioJ);
        otst += 3*BUTTON_WIDTH;

        JLabel themeJ = new JLabel("Тема ВКР");
        themeJ.setBounds(otst, PANAL_HEIGHT - BUTTON_HEIGHT - 10, 3*BUTTON_WIDTH , BUTTON_HEIGHT);
        themeJ.setFont(new Font("Times New Roman", NORMAL, 15));
        themeJ.setBackground(Color.gray);
        themeJ.setForeground(Color.DARK_GRAY);
        pan.add(themeJ);
        colorC++;

        if(colorC%2 == 0){
            pan.setBackground(Color.WHITE);

        }else pan.setBackground(Color.LIGHT_GRAY);
        return pan;
    }

    private JPanel createBut() throws SQLException{
        Color color = new Color (240, 240, 240);
        JPanel panel2 = new JPanel();
        panel2.setPreferredSize(new Dimension(PANAL_WIDTH, PANAL_HEIGHT));
        panel2.setLayout(null);

        JButton returnB = new JButton("Назад");
        returnB.setBounds(PANAL_WIDTH - BUTTON_WIDTH, BUTTON_HEIGHT/2, 70, 30 );
        returnB.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });
        returnB.setBackground(color);
        panel2.add(returnB);

        return panel2;
    }


}
