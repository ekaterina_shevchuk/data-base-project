package org.example.dataBase.view;

import org.example.dataBase.controller.StudentsController;
import org.example.dataBase.model.StudentsEntity;
import org.example.dataBase.service.StudentsService;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;

public class ViewStudents extends JFrame {
    static int WINDOW_WIDTH = 1350;
    static int WINDOW_HEIGHT = 750;
    static int PANAL_WIDTH = 1100;
    static int PANAL_HEIGHT = 70;
    static int BUTTON_WIDTH = 350;
    static int BUTTON_HEIGHT = 50;

    StudentsController studentsController;
    int colorC = 0;

    public ViewStudents(ArrayList<StudentsEntity> studentsEntities, StudentsController studentsController1) throws SQLException{
        studentsController = studentsController1;
        Container c = getContentPane();
        c.setLayout(new BorderLayout());
        setTitle("Просмотр Студентов");
        setMinimumSize(new Dimension(WINDOW_WIDTH, WINDOW_HEIGHT));
        setResizable(false);

        JPanel panel = new JPanel();
        panel.setBorder(new EmptyBorder(10, 10, 10, 10));
        GridBagLayout gridBagLayout = new GridBagLayout();
        gridBagLayout.rowHeights = new int[]{BUTTON_HEIGHT, BUTTON_HEIGHT,BUTTON_HEIGHT,BUTTON_HEIGHT,BUTTON_HEIGHT };
        gridBagLayout.columnWeights = new double[]{1.0};
        gridBagLayout.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0};
        panel.setLayout(gridBagLayout);
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.weightx = 1;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.gridwidth = GridBagConstraints.REMAINDER;
        c.add(panel);
        JScrollPane jScrollPane = new JScrollPane(panel);
        jScrollPane.setPreferredSize(new Dimension(WINDOW_WIDTH, WINDOW_HEIGHT));

        jScrollPane.setLayout(new ScrollPaneLayout());
        c.add(jScrollPane);
        jScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
        panel.add(createBut(), gbc);
        panel.add(article(), gbc);

        Iterator<StudentsEntity> iter = studentsEntities.iterator();
        while(iter.hasNext()){
            panel.add(createNewJPanel(iter.next()), gbc);
        }
        Color color = new Color (240, 240, 240);
        panel.setBackground(color);
        pack();
        setPreferredSize(new Dimension(WINDOW_WIDTH, WINDOW_HEIGHT));
        setVisible(true);
    }

    private JPanel createNewJPanel(StudentsEntity model) {
        JPanel pan = new JPanel();
        int otst = 20;
        pan.setPreferredSize(new Dimension(PANAL_WIDTH, PANAL_HEIGHT));
        pan.setLayout(null);

        JLabel fioJ = new JLabel(model.getFIO());
        fioJ.setBounds(otst, PANAL_HEIGHT - BUTTON_HEIGHT - 10, BUTTON_WIDTH, BUTTON_HEIGHT);
        fioJ.setFont(new Font("Times New Roman", NORMAL, 15));
        fioJ.setBackground(Color.gray);
        fioJ.setForeground(Color.DARK_GRAY);
        pan.add(fioJ);
        otst +=BUTTON_WIDTH;
        JLabel groupJ = new JLabel(String.valueOf(model.getNum_gr()));
        groupJ.setBounds(otst, PANAL_HEIGHT - BUTTON_HEIGHT - 10, BUTTON_WIDTH / 3, BUTTON_HEIGHT);
        groupJ.setFont(new Font("Times New Roman", NORMAL, 15));
        groupJ.setBackground(Color.gray);
        groupJ.setForeground(Color.DARK_GRAY);
        pan.add(groupJ);
        otst += BUTTON_WIDTH / 3;
        JLabel bdJ = new JLabel(String.valueOf(model.getBirthDay()));
        bdJ.setBounds(otst, PANAL_HEIGHT - BUTTON_HEIGHT - 10, BUTTON_WIDTH / 3, BUTTON_HEIGHT);
        bdJ.setFont(new Font("Times New Roman", NORMAL, 15));
        bdJ.setBackground(Color.gray);
        bdJ.setForeground(Color.DARK_GRAY);
        pan.add(bdJ);
        otst+=BUTTON_WIDTH / 3;
        JLabel genderJ = new JLabel(String.valueOf(model.getGender()));
        genderJ.setBounds(otst , PANAL_HEIGHT - BUTTON_HEIGHT - 10, BUTTON_WIDTH / 10, BUTTON_HEIGHT);
        genderJ.setFont(new Font("Times New Roman", NORMAL, 15));
        genderJ.setBackground(Color.gray);
        genderJ.setForeground(Color.DARK_GRAY);
        pan.add(genderJ);
        otst += BUTTON_WIDTH / 10;
        JLabel childJ = new JLabel(String.valueOf(model.getCheckChild()));
        childJ.setBounds(otst, PANAL_HEIGHT - BUTTON_HEIGHT - 10, BUTTON_WIDTH / 3, BUTTON_HEIGHT);
        childJ.setFont(new Font("Times New Roman", NORMAL, 15));
        childJ.setBackground(Color.gray);
        childJ.setForeground(Color.DARK_GRAY);
        pan.add(childJ);
        otst += BUTTON_WIDTH / 3;
        JLabel ssJ = new JLabel(String.valueOf(model.getScholarship()));
        ssJ.setBounds(otst, PANAL_HEIGHT - BUTTON_HEIGHT - 10, BUTTON_WIDTH / 3, BUTTON_HEIGHT);
        ssJ.setFont(new Font("Times New Roman", NORMAL, 15));
        ssJ.setBackground(Color.gray);
        ssJ.setForeground(Color.DARK_GRAY);
        pan.add(ssJ);

        colorC++;
        Color col1 = new Color(250, 250, 250);
        Color col2 = new Color(225, 225, 225);
        if(colorC%2 == 0){
            pan.setBackground(col1);
        }else pan.setBackground(col2 );
        return pan;
    }

    private JPanel article(){
        JPanel pan = new JPanel();
        int otst = 20;
        pan.setPreferredSize(new Dimension(PANAL_WIDTH, PANAL_HEIGHT));
        pan.setLayout(null);

        JLabel fioJ = new JLabel("ФИО");
        fioJ.setBounds(otst, PANAL_HEIGHT - BUTTON_HEIGHT - 10, BUTTON_WIDTH, BUTTON_HEIGHT);
        fioJ.setFont(new Font("Times New Roman", NORMAL, 15));
        fioJ.setBackground(Color.gray);
        fioJ.setForeground(Color.DARK_GRAY);
        pan.add(fioJ);
        otst +=BUTTON_WIDTH;
        JLabel groupJ = new JLabel("номер группы");
        groupJ.setBounds(otst, PANAL_HEIGHT - BUTTON_HEIGHT - 10, BUTTON_WIDTH /3, BUTTON_HEIGHT);
        groupJ.setFont(new Font("Times New Roman", NORMAL, 15));
        groupJ.setBackground(Color.gray);
        groupJ.setForeground(Color.DARK_GRAY);
        pan.add(groupJ);
        otst += BUTTON_WIDTH / 3;
        JLabel bdJ = new JLabel("Дата рождения");
        bdJ.setBounds(otst, PANAL_HEIGHT - BUTTON_HEIGHT - 10, BUTTON_WIDTH / 3, BUTTON_HEIGHT);
        bdJ.setFont(new Font("Times New Roman", NORMAL, 15));
        bdJ.setBackground(Color.gray);
        bdJ.setForeground(Color.DARK_GRAY);
        pan.add(bdJ);
        otst+=BUTTON_WIDTH / 3;
        JLabel genderJ = new JLabel("пол");
        genderJ.setBounds(otst , PANAL_HEIGHT - BUTTON_HEIGHT - 10, BUTTON_WIDTH / 10, BUTTON_HEIGHT);
        genderJ.setFont(new Font("Times New Roman", NORMAL, 15));
        genderJ.setBackground(Color.gray);
        genderJ.setForeground(Color.DARK_GRAY);
        pan.add(genderJ);
        otst += BUTTON_WIDTH / 10;
        JLabel childJ = new JLabel("наличие детей");
        childJ.setBounds(otst, PANAL_HEIGHT - BUTTON_HEIGHT - 10, BUTTON_WIDTH / 3, BUTTON_HEIGHT);
        childJ.setFont(new Font("Times New Roman", NORMAL, 15));
        childJ.setBackground(Color.gray);
        childJ.setForeground(Color.DARK_GRAY);
        pan.add(childJ);
        otst += BUTTON_WIDTH / 3;
        JLabel ssJ = new JLabel("размер стипендии");
        ssJ.setBounds(otst, PANAL_HEIGHT - BUTTON_HEIGHT - 10, BUTTON_WIDTH / 2, BUTTON_HEIGHT);
        ssJ.setFont(new Font("Times New Roman", NORMAL, 15));
        ssJ.setBackground(Color.gray);
        ssJ.setForeground(Color.DARK_GRAY);
        pan.add(ssJ);

        colorC++;
        Color col2 = new Color(225, 225, 225);
        pan.setBackground(col2);
        return pan;
    }

    private JPanel createBut() throws SQLException{
        Color color = new Color (240, 240, 240);
        JPanel panel2 = new JPanel();
        panel2.setPreferredSize(new Dimension(PANAL_WIDTH, PANAL_HEIGHT));
        panel2.setLayout(null);
        if (studentsController.getRole() != 0) {
            JButton addNewB = new JButton("Добавить нового студента");
            final AddStudent addStudent = new AddStudent(studentsController);
            addNewB.setBounds(50, BUTTON_HEIGHT / 2, 3 * BUTTON_WIDTH / 4, 30);
            addNewB.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    try {
                        dispose();
                        addStudent.visibleAS();
                    } catch (SQLException eds) {
                        eds.printStackTrace();
                    }
                }
            });
            addNewB.setBackground(color);
            panel2.add(addNewB);
            JButton alterB = new JButton("Изменение информации о студенте");
            final UpdateStudent updateStudent = new UpdateStudent(studentsController);
            alterB.setBounds(60+3*BUTTON_WIDTH/2, BUTTON_HEIGHT/2, 3*BUTTON_WIDTH/4, 30 );
            alterB.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    try {
                        dispose();
                        updateStudent.visibleUS();
                    }catch (SQLException eds){
                        eds.printStackTrace(); }
                }
            });
            alterB.setBackground(color);
            panel2.add(alterB);
        }
        JButton returnB = new JButton("Назад");
        returnB.setBounds(PANAL_WIDTH - BUTTON_WIDTH/2, BUTTON_HEIGHT/2, 70, 30 );
        returnB.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });
        returnB.setBackground(color);
        panel2.add(returnB);

        JButton filtB = new JButton("Расширенный поиск студентов");
        final SearchStudents searchStudents = new SearchStudents(studentsController);
        filtB.setBounds(60+3*BUTTON_WIDTH/4, BUTTON_HEIGHT/2, 3*BUTTON_WIDTH/4, 30 );
        filtB.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    dispose();
                    searchStudents.visible();
                }catch (SQLException eds){
                    eds.printStackTrace(); }
            }
        });
        filtB.setBackground(color);
        panel2.add(filtB);

        return panel2;
    }

}
