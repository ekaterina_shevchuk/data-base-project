package org.example.dataBase.view;

import org.example.dataBase.controller.TeachersController;
import org.example.dataBase.model.StudentsEntity;
import org.example.dataBase.model.TeachersEntity;
import org.example.dataBase.model.TestsEntity;
import org.example.dataBase.service.TeachersService;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;

public class ViewTeachers extends JFrame {
    static int WINDOW_WIDTH = 1350;
    static int WINDOW_HEIGHT = 750;
    static int PANAL_WIDTH = 1100;
    static int PANAL_HEIGHT = 70;
    static int BUTTON_WIDTH = 350;
    static int BUTTON_HEIGHT = 50;

    TeachersController teachersController;
    int colorC = 0;

    public ViewTeachers(ArrayList<TeachersEntity> teachersEntities, TeachersController teachersController1) throws SQLException {
        teachersController = teachersController1;
        Container c = getContentPane();
        c.setLayout(new BorderLayout());
        setTitle("Просмотр преподавателей");
        setMinimumSize(new Dimension(WINDOW_WIDTH, WINDOW_HEIGHT));
        setResizable(false);

        JPanel panel = new JPanel();
        panel.setBorder(new EmptyBorder(10, 10, 10, 10));
        GridBagLayout gridBagLayout = new GridBagLayout();
        gridBagLayout.rowHeights = new int[]{BUTTON_HEIGHT, BUTTON_HEIGHT,BUTTON_HEIGHT,BUTTON_HEIGHT,BUTTON_HEIGHT };
        gridBagLayout.columnWeights = new double[]{1.0};
        gridBagLayout.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0};
        panel.setLayout(gridBagLayout);
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.weightx = 1;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.gridwidth = GridBagConstraints.REMAINDER;
        c.add(panel);
        JScrollPane jScrollPane = new JScrollPane(panel);
        jScrollPane.setPreferredSize(new Dimension(WINDOW_WIDTH, WINDOW_HEIGHT));

        jScrollPane.setLayout(new ScrollPaneLayout());
        c.add(jScrollPane);
        jScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
        panel.add(createBut(), gbc);
        panel.add(article(), gbc);

        Iterator<TeachersEntity> iter = teachersEntities.iterator();
        while(iter.hasNext()){
            panel.add(createNewJPanel(iter.next()), gbc);
        }
        Color color = new Color (240, 240, 240);
        panel.setBackground(color);
        pack();
        setPreferredSize(new Dimension(WINDOW_WIDTH, WINDOW_HEIGHT));
        setVisible(true);
    }

    private JPanel createNewJPanel(TeachersEntity model) {
        JPanel pan = new JPanel();
        int otst = 20;
        pan.setPreferredSize(new Dimension(PANAL_WIDTH, PANAL_HEIGHT));
        pan.setLayout(null);

        JLabel fioJ = new JLabel(model.getFIO());
        fioJ.setBounds(otst, PANAL_HEIGHT - BUTTON_HEIGHT - 10, 5*BUTTON_WIDTH/7, BUTTON_HEIGHT);
        fioJ.setFont(new Font("Times New Roman", NORMAL, 15));
        fioJ.setBackground(Color.gray);
        fioJ.setForeground(Color.DARK_GRAY);
        pan.add(fioJ);
        otst += 5*BUTTON_WIDTH/7;
        JLabel kafJ = new JLabel(model.getName_kaf());
        kafJ.setBounds(otst, PANAL_HEIGHT - BUTTON_HEIGHT - 10, 2*BUTTON_WIDTH / 3, BUTTON_HEIGHT);
        kafJ.setFont(new Font("Times New Roman", NORMAL, 15));
        kafJ.setBackground(Color.gray);
        kafJ.setForeground(Color.DARK_GRAY);
        kafJ.setToolTipText(model.getName_kaf());
        pan.add(kafJ);
        otst += 2*BUTTON_WIDTH / 3;
        JLabel jobJ = new JLabel(model.getName_job());
        jobJ.setBounds(otst, PANAL_HEIGHT - BUTTON_HEIGHT - 10, BUTTON_WIDTH / 3, BUTTON_HEIGHT);
        jobJ.setFont(new Font("Times New Roman", NORMAL, 15));
        jobJ.setBackground(Color.gray);
        jobJ.setForeground(Color.DARK_GRAY);
        pan.add(jobJ);
        otst+=BUTTON_WIDTH / 3;
        JLabel catJ = new JLabel(model.getCategory());
        catJ.setBounds(otst, PANAL_HEIGHT - BUTTON_HEIGHT - 10, BUTTON_WIDTH / 3, BUTTON_HEIGHT);
        catJ.setFont(new Font("Times New Roman", NORMAL, 15));
        catJ.setBackground(Color.gray);
        catJ.setForeground(Color.DARK_GRAY);
        pan.add(catJ);
        otst+=BUTTON_WIDTH / 3;
        JLabel genderJ = new JLabel(model.getGender());
        genderJ.setBounds(otst , PANAL_HEIGHT - BUTTON_HEIGHT - 10, BUTTON_WIDTH / 10, BUTTON_HEIGHT);
        genderJ.setFont(new Font("Times New Roman", NORMAL, 15));
        genderJ.setBackground(Color.gray);
        genderJ.setForeground(Color.DARK_GRAY);
        pan.add(genderJ);
        otst += BUTTON_WIDTH / 10;
        JLabel bdJ = new JLabel(String.valueOf(model.getBirthDate()));
        bdJ.setBounds(otst, PANAL_HEIGHT - BUTTON_HEIGHT - 10, BUTTON_WIDTH / 3, BUTTON_HEIGHT);
        bdJ.setFont(new Font("Times New Roman", NORMAL, 15));
        bdJ.setBackground(Color.gray);
        bdJ.setForeground(Color.DARK_GRAY);
        pan.add(bdJ);
        otst += BUTTON_WIDTH / 3;
        JLabel childJ = new JLabel(String.valueOf(model.getNumChildrens()));
        childJ.setBounds(otst, PANAL_HEIGHT - BUTTON_HEIGHT - 10, BUTTON_WIDTH / 3, BUTTON_HEIGHT);
        childJ.setFont(new Font("Times New Roman", NORMAL, 15));
        childJ.setBackground(Color.gray);
        childJ.setForeground(Color.DARK_GRAY);
        pan.add(childJ);

        colorC++;
        Color col1 = new Color(250, 250, 250);
        Color col2 = new Color(225, 225, 225);
        if(colorC%2 == 0){
            pan.setBackground(col1);
        }else pan.setBackground(col2 );
        return pan;
    }

    private JPanel article(){
        JPanel pan = new JPanel();
        int otst = 20;
        pan.setPreferredSize(new Dimension(PANAL_WIDTH, PANAL_HEIGHT));
        pan.setLayout(null);

        JLabel fioJ = new JLabel("ФИО");
        fioJ.setBounds(otst, PANAL_HEIGHT - BUTTON_HEIGHT - 10, 5*BUTTON_WIDTH/7, BUTTON_HEIGHT);
        fioJ.setFont(new Font("Times New Roman", NORMAL, 15));
        fioJ.setBackground(Color.gray);
        fioJ.setForeground(Color.DARK_GRAY);
        pan.add(fioJ);
        otst += 5*BUTTON_WIDTH/7;
        JLabel kafJ = new JLabel("Название кафедры");
        kafJ.setBounds(otst, PANAL_HEIGHT - BUTTON_HEIGHT - 10, 2*BUTTON_WIDTH /3, BUTTON_HEIGHT);
        kafJ.setFont(new Font("Times New Roman", NORMAL, 15));
        kafJ.setBackground(Color.gray);
        kafJ.setForeground(Color.DARK_GRAY);
        pan.add(kafJ);
        otst += 2*BUTTON_WIDTH / 3;
        JLabel jobJ = new JLabel("Должность");
        jobJ.setBounds(otst, PANAL_HEIGHT - BUTTON_HEIGHT - 10, BUTTON_WIDTH / 3, BUTTON_HEIGHT);
        jobJ.setFont(new Font("Times New Roman", NORMAL, 15));
        jobJ.setBackground(Color.gray);
        jobJ.setForeground(Color.DARK_GRAY);
        pan.add(jobJ);
        otst += BUTTON_WIDTH / 3;
        JLabel catJ = new JLabel("Категория");
        catJ.setBounds(otst, PANAL_HEIGHT - BUTTON_HEIGHT - 10, BUTTON_WIDTH / 3, BUTTON_HEIGHT);
        catJ.setFont(new Font("Times New Roman", NORMAL, 15));
        catJ.setBackground(Color.gray);
        catJ.setForeground(Color.DARK_GRAY);
        pan.add(catJ);
        otst+=BUTTON_WIDTH / 3;
        JLabel genderJ = new JLabel("пол");
        genderJ.setBounds(otst , PANAL_HEIGHT - BUTTON_HEIGHT - 10, BUTTON_WIDTH / 10, BUTTON_HEIGHT);
        genderJ.setFont(new Font("Times New Roman", NORMAL, 15));
        genderJ.setBackground(Color.gray);
        genderJ.setForeground(Color.DARK_GRAY);
        pan.add(genderJ);
        otst += BUTTON_WIDTH / 10;
        JLabel bdJ = new JLabel("дата рождения");
        bdJ.setBounds(otst, PANAL_HEIGHT - BUTTON_HEIGHT - 10, BUTTON_WIDTH / 3, BUTTON_HEIGHT);
        bdJ.setFont(new Font("Times New Roman", NORMAL, 15));
        bdJ.setBackground(Color.gray);
        bdJ.setForeground(Color.DARK_GRAY);
        pan.add(bdJ);
        otst += BUTTON_WIDTH / 3;
        JLabel childJ = new JLabel("количество детей");
        childJ.setBounds(otst, PANAL_HEIGHT - BUTTON_HEIGHT - 10, BUTTON_WIDTH / 3, BUTTON_HEIGHT);
        childJ.setFont(new Font("Times New Roman", NORMAL, 15));
        childJ.setBackground(Color.gray);
        childJ.setForeground(Color.DARK_GRAY);
        pan.add(childJ);

        colorC++;
        Color col1 = new Color(250, 250, 250);
        Color col2 = new Color(225, 225, 225);
        if(colorC%2 == 0){
            pan.setBackground(col1);
        }else pan.setBackground(col2 );
        return pan;
    }

    private JPanel createBut() throws SQLException{
        Color color = new Color (240, 240, 240);
        JPanel panel2 = new JPanel();
        panel2.setPreferredSize(new Dimension(PANAL_WIDTH, PANAL_HEIGHT));
        panel2.setLayout(null);
        if (teachersController.getRole() != 0) {
            JButton addNewB = new JButton("Добавить нового преподавателя");
            final AddTeacher addTeacher = new AddTeacher(teachersController);
            addNewB.setBounds(50, BUTTON_HEIGHT / 2, 3 * BUTTON_WIDTH / 4, 30);
            addNewB.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    try {
                        dispose();
                        addTeacher.visibleAT();
                    } catch (SQLException eds) {
                        eds.printStackTrace();
                    }
                }
            });
            addNewB.setBackground(color);
            panel2.add(addNewB);
            JButton alterB = new JButton("Изменение информации о преподавателе");
            final UpdateTeacher updateTeacher = new UpdateTeacher(teachersController);
            alterB.setBounds(60+3*BUTTON_WIDTH/2, BUTTON_HEIGHT/2, 3*BUTTON_WIDTH/4, 30 );
            alterB.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    try {
                        dispose();
                        updateTeacher.visibleUT();
                    }catch (SQLException eds){
                        eds.printStackTrace(); }
                }
            });
            alterB.setBackground(color);
            panel2.add(alterB);
        }
        JButton returnB = new JButton("Назад");
        returnB.setBounds(PANAL_WIDTH - BUTTON_WIDTH/2, BUTTON_HEIGHT/2, 70, 30 );
        returnB.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });
        returnB.setBackground(color);
        panel2.add(returnB);

        JButton filtB = new JButton("Расширенный поиск реподавателей");
        final SearchTeachers searchTeachers = new SearchTeachers(teachersController);
        filtB.setBounds(70+3*BUTTON_WIDTH/4, BUTTON_HEIGHT/2, 3*BUTTON_WIDTH/4, 30 );
        filtB.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    dispose();
                    searchTeachers.visible();
                }catch (SQLException eds){
                    eds.printStackTrace(); }
            }
        });
        filtB.setBackground(color);
        panel2.add(filtB);
        return panel2;
    }
}
