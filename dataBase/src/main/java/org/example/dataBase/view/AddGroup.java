package org.example.dataBase.view;

import org.example.dataBase.controller.GroupsController;
import org.example.dataBase.model.FacultetsEntity;
import org.example.dataBase.model.GroupsEntity;
import org.example.dataBase.service.FacultetsService;
import org.example.dataBase.service.GroupsService;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;

public class AddGroup extends JFrame {
    static int WINDOW_WIDTH = 1350;
    static int WINDOW_HEIGHT = 750;
    static int PANAL_WIDTH = 1100;
    static int PANAL_HEIGHT = 70;
    static int BUTTON_WIDTH = 350;
    static int BUTTON_HEIGHT = 50;

    final GroupsController groupsController;

    public AddGroup(GroupsController groupsController1)throws SQLException {
        groupsController = groupsController1;
    }

    public void visibleAG() throws SQLException{
        Color color = new Color (240, 240, 240);
        final GroupsEntity groupsEntity = new GroupsEntity();
        Container c = getContentPane();
        c.setLayout(new BorderLayout());
        setTitle("Добавление новой группы");
        setMinimumSize(new Dimension(WINDOW_WIDTH, WINDOW_HEIGHT));
        setResizable(false);

        JPanel panel = new JPanel();
        panel.setBorder(new EmptyBorder(10, 10, 10, 10));
        final GridBagLayout gridBagLayout = new GridBagLayout();
        gridBagLayout.rowHeights = new int[]{BUTTON_HEIGHT, BUTTON_HEIGHT,BUTTON_HEIGHT,BUTTON_HEIGHT,BUTTON_HEIGHT };
        gridBagLayout.columnWeights = new double[]{1.0};
        gridBagLayout.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0};
        panel.setLayout(gridBagLayout);

        GridBagConstraints gbc = new GridBagConstraints();
        gbc.weightx = 1;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.gridwidth = GridBagConstraints.REMAINDER;
        c.add(panel);
        JScrollPane jScrollPane = new JScrollPane(panel);
        jScrollPane.setPreferredSize(new Dimension(WINDOW_WIDTH, WINDOW_HEIGHT));

        jScrollPane.setLayout(new ScrollPaneLayout());
        c.add(jScrollPane);
        jScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);

        GridBagConstraints layConstraints;

        final JTextField idField;
        JLabel grL = new JLabel("Введите номер группы");
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 0;
        layConstraints.gridy = 0;
        panel.add(grL,layConstraints);
        idField = new JTextField("", 10);
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 1;
        layConstraints.gridy = 0;
        panel.add(idField, layConstraints);

        JLabel courseL = new JLabel("Выберите курс");
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 0;
        layConstraints.gridy = 1;
        panel.add(courseL,layConstraints);
        Integer[] courseA = {1,2,3,4};
        final JComboBox courseBox = new JComboBox(courseA);
        courseBox.setAlignmentX(LEFT_ALIGNMENT);
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 1;
        layConstraints.gridy = 1;
        panel.add(courseBox, layConstraints);

        JLabel jLabel = new JLabel("Выберите факультет");
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 0;
        layConstraints.gridy = 2;
        panel.add(jLabel,layConstraints);

        ArrayList<String> facultets = groupsController.getFaultets();
        final JComboBox facBox = new JComboBox();
        Iterator<String> iter = facultets.iterator();
        int k = 0, g = facultets.size();
        while(iter.hasNext()){
            if (k == g) break;
            facBox.addItem(facultets.get(k));
            k++;
        }
        facBox.setAlignmentX(LEFT_ALIGNMENT);
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 1;
        layConstraints.gridy = 2;
        panel.add(facBox, layConstraints);

        JButton addNewB = new JButton("Добавить");
        addNewB.setBounds(50, WINDOW_HEIGHT-BUTTON_HEIGHT, 170, 70 );
        addNewB.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    groupsEntity.setNum_gr(Integer.parseInt(idField.getText()));
                    groupsEntity.setCourse((Integer) courseBox.getSelectedItem());
                    groupsEntity.setName_fa((String) facBox.getSelectedItem());
                    groupsController.addGroup(groupsEntity);
                    ViewSaveSuccess viewSaveSuccess = new ViewSaveSuccess();
                }catch (SQLException eds){
                    ViewMistake viewMistake = new ViewMistake(eds.getMessage());
                    eds.printStackTrace(); }
            }
        });
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 2;
        layConstraints.gridy = 1;
        addNewB.setBackground(color);
        panel.add(addNewB, layConstraints);

        JButton returnB = new JButton("Назад");
        returnB.setBounds(WINDOW_WIDTH-BUTTON_WIDTH, WINDOW_HEIGHT-BUTTON_HEIGHT, 170, 70 );
        final ArrayList<GroupsEntity> groupsEntities = this.groupsController.getAll();
        returnB.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    dispose();
                    ViewGroups viewGroups = new ViewGroups(groupsEntities, groupsController);
                }catch (SQLException eds){
                    ViewMistake viewMistake = new ViewMistake(eds.getMessage());
                    eds.printStackTrace();
                }
            }
        });
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 2;
        layConstraints.gridy = 4;
        returnB.setBackground(color);
        panel.add(returnB, layConstraints);
        color = new Color (240, 240, 240);
        panel.setBackground(color);
        pack();
        setPreferredSize(new Dimension(WINDOW_WIDTH, WINDOW_HEIGHT));
        setVisible(true);
    }
}
