package org.example.dataBase.view;

import org.example.dataBase.controller.TeachersController;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;

public class AdditSearchTch3 extends JFrame {
    static int WINDOW_WIDTH = 1350;
    static int WINDOW_HEIGHT = 750;
    static int PANAL_WIDTH = 1100;
    static int PANAL_HEIGHT = 70;
    static int BUTTON_WIDTH = 350;
    static int BUTTON_HEIGHT = 50;

    private int num_gr, semester;
    private String subject;
    private TeachersController teachersController;

    public AdditSearchTch3(final TeachersController teachersController1) throws SQLException {
        teachersController = teachersController1;
        Container c = getContentPane();
        c.setLayout(new BorderLayout());
        setTitle("Расширенный поиск преподавателей");
        setMinimumSize(new Dimension(WINDOW_WIDTH, WINDOW_HEIGHT));
        setResizable(false);

        JPanel panel = new JPanel();
        panel.setBorder(new EmptyBorder(10, 10, 10, 10));
        GridBagLayout gridBagLayout = new GridBagLayout();
        gridBagLayout.rowHeights = new int[]{BUTTON_HEIGHT, BUTTON_HEIGHT, BUTTON_HEIGHT, BUTTON_HEIGHT, BUTTON_HEIGHT, BUTTON_HEIGHT, BUTTON_HEIGHT, BUTTON_HEIGHT, BUTTON_HEIGHT, BUTTON_HEIGHT, BUTTON_HEIGHT};
        gridBagLayout.columnWeights = new double[]{1.0};
        gridBagLayout.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0};
        panel.setLayout(gridBagLayout);
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.weightx = 1;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.gridwidth = GridBagConstraints.REMAINDER;
        c.add(panel);
        JScrollPane jScrollPane = new JScrollPane(panel);
        jScrollPane.setPreferredSize(new Dimension(WINDOW_WIDTH, WINDOW_HEIGHT));

        jScrollPane.setLayout(new ScrollPaneLayout());
        c.add(jScrollPane);
        jScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
        GridBagConstraints layConstraints;

        JLabel subLabel = new JLabel("Выберите название дисциплины");
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 0;
        layConstraints.gridy = 0;
        panel.add(subLabel,layConstraints);
        ArrayList<String> subjects = teachersController.getSubjects();
        final JComboBox subjectBox = new JComboBox();
        Iterator<String> iter4 = subjects.iterator();
        int k = 0,  g = subjects.size();
        while(iter4.hasNext()){
            if (k == g) break;
            subjectBox.addItem(subjects.get(k));
            k++;
        }
        subjectBox.setAlignmentX(LEFT_ALIGNMENT);
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 1;
        layConstraints.gridy = 0;
        panel.add(subjectBox, layConstraints);

        JLabel jLabel = new JLabel("Выберите номер группы");
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 0;
        layConstraints.gridy = 1;
        panel.add(jLabel,layConstraints);
        ArrayList<Integer> groups = teachersController.getGroups();
        final JComboBox groupBox = new JComboBox();
        Iterator<Integer> iter = groups.iterator();
        k = 0;
        g = groups.size();
        while(iter.hasNext()){
            if (k == g) break;
            groupBox.addItem(groups.get(k));
            k++;
        }

        groupBox.setAlignmentX(LEFT_ALIGNMENT);
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 1;
        layConstraints.gridy = 1;
        panel.add(groupBox, layConstraints);

        JLabel semestrL = new JLabel("Выберите семестр");
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 0;
        layConstraints.gridy = 2;
        panel.add(semestrL,layConstraints);
        Integer[] semestrA = {1,2,3,4,5,6,7,8};
        final JComboBox semestrBox = new JComboBox(semestrA);
        semestrBox.setAlignmentX(LEFT_ALIGNMENT);
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 1;
        layConstraints.gridy = 2;
        panel.add(semestrBox, layConstraints);

        final JButton searchB = new JButton("Поиск");
        searchB.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                num_gr = (Integer) groupBox.getSelectedItem();
                subject = (String)subjectBox.getSelectedItem();
                semester = (Integer) semestrBox.getSelectedItem();
                dispose();
                try{
                    SearchTeachers searchTeachers = new SearchTeachers(teachersController);
                    searchTeachers.setAnsw("11 12 16 ");
                    searchTeachers.setQuery9(num_gr, subject, semester);
                    searchTeachers.callSearch();
                }catch (SQLException eds){eds.printStackTrace();}
            }
        });
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 4;
        layConstraints.gridy = 0;
        panel.add(searchB,layConstraints);

        Color color = new Color (240, 240, 240);
        JButton returnB = new JButton("Назад");
        final SearchTeachers searchTeachers = new SearchTeachers(teachersController);
        returnB.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    dispose();
                    searchTeachers.visible();
                }catch (SQLException eds){eds.printStackTrace();}
            }
        });
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 4;
        layConstraints.gridy = 1;
        panel.add(returnB, layConstraints);

        panel.setBackground(color);
        pack();
        setPreferredSize(new Dimension(WINDOW_WIDTH, WINDOW_HEIGHT));
        setVisible(true);

    }
}
