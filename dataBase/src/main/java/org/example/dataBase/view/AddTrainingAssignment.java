package org.example.dataBase.view;

import org.example.dataBase.controller.TrainingAssignmentController;
import org.example.dataBase.model.TrainingAssignmentEntity;
import org.example.dataBase.model.VkrsEntity;
import org.example.dataBase.service.TrainingAssignmentService;
import org.example.dataBase.service.VkrsService;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;

public class AddTrainingAssignment extends JFrame {
    static int WINDOW_WIDTH = 1350;
    static int WINDOW_HEIGHT = 750;
    static int PANAL_WIDTH = 1100;
    static int PANAL_HEIGHT = 70;
    static int BUTTON_WIDTH = 350;
    static int BUTTON_HEIGHT = 50;
    static int smallBUTTON_WIDTH = 50;
    static int bigBUTTON_WIDTH = 200;
    private ArrayList<Integer> ocupation;
    private JComboBox ocupBox;
    private GridBagConstraints layConstraints;
    private JPanel panel;

    TrainingAssignmentController trainingAssignmentController;

    public AddTrainingAssignment(TrainingAssignmentController trainingAssignmentController1)throws SQLException {
        trainingAssignmentController = trainingAssignmentController1;
    }

    public void visibleAV() throws SQLException{
        Color color = new Color (240, 240, 240);
        final TrainingAssignmentEntity trainingAssignmentEntity = new TrainingAssignmentEntity();
        Container c = getContentPane();
        c.setLayout(new BorderLayout());
        setTitle("Добавление нового учебного поручения");
        setMinimumSize(new Dimension(WINDOW_WIDTH, WINDOW_HEIGHT));
        setResizable(false);

        panel = new JPanel();
        panel.setBorder(new EmptyBorder(10, 10, 10, 10));
        GridBagLayout gridBagLayout = new GridBagLayout();
        gridBagLayout.rowHeights = new int[]{BUTTON_HEIGHT, BUTTON_HEIGHT,BUTTON_HEIGHT,BUTTON_HEIGHT,BUTTON_HEIGHT,BUTTON_HEIGHT,BUTTON_HEIGHT };
        gridBagLayout.columnWeights = new double[]{1.0};
        gridBagLayout.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0};
        panel.setLayout(gridBagLayout);

        GridBagConstraints gbc = new GridBagConstraints();
        gbc.weightx = 1;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.gridwidth = GridBagConstraints.REMAINDER;
        c.add(panel);
        JScrollPane jScrollPane = new JScrollPane(panel);
        jScrollPane.setPreferredSize(new Dimension(WINDOW_WIDTH, WINDOW_HEIGHT));

        jScrollPane.setLayout(new ScrollPaneLayout());
        c.add(jScrollPane);
        jScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);

        final JTextField tchField;

        JLabel subL = new JLabel("Выберете предмет");
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 0;
        layConstraints.gridy = 0;
        panel.add(subL,layConstraints);
        ArrayList<String> subjects = trainingAssignmentController.getAllSubjects();
        final JComboBox subBox = new JComboBox();
        Iterator<String> iter0 = subjects.iterator();
        int k = 0, g = subjects.size();
        while(iter0.hasNext()){
            if (k == g) break;
            subBox.addItem(subjects.get(k));
            k++;
        }
        subBox.setAlignmentX(LEFT_ALIGNMENT);
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 1;
        layConstraints.gridy = 0;
        panel.add(subBox, layConstraints);

        JLabel ocupLabel = new JLabel("Выберите номер учебного плана");
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 0;
        layConstraints.gridy = 1;
        panel.add(ocupLabel,layConstraints);
        ocupation = trainingAssignmentController.getCurriculumNum();
        ocupBox = createCurrNum();
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 1;
        layConstraints.gridy = 1;
        panel.add(ocupBox, layConstraints);

        JLabel tchLabel = new JLabel("Выберите преподавателя");
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 0;
        layConstraints.gridy = 2;
        panel.add(tchLabel,layConstraints);
        ArrayList<String> teachersFIO = trainingAssignmentController.getFIOtch();
        final JComboBox teachersBox = new JComboBox();
        Iterator<String> iter3 = teachersFIO.iterator();
        k = 0;
        g = teachersFIO.size();
        while(iter3.hasNext()){
            if (k == g) break;
            teachersBox.addItem(teachersFIO.get(k));
            k++;
        }
        teachersBox.setAlignmentX(LEFT_ALIGNMENT);
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 1;
        layConstraints.gridy = 2;
        panel.add(teachersBox, layConstraints);


        JButton addNewB = new JButton("Добавить");
        addNewB.setBounds(50, WINDOW_HEIGHT-BUTTON_HEIGHT, 170, 70 );
        addNewB.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    trainingAssignmentEntity.setTitleSub((String) subBox.getSelectedItem());
                    trainingAssignmentEntity.setFIOtch((String) teachersBox.getSelectedItem());
                    trainingAssignmentEntity.setID_t((Integer) ocupBox.getSelectedItem());
                    trainingAssignmentController.addTrainingAssignment(trainingAssignmentEntity);
                }catch (SQLException eds){
                    ViewMistake viewMistake = new ViewMistake(eds.getMessage());
                    eds.printStackTrace(); }
            }
        });
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 2;
        layConstraints.gridy = 1;
        addNewB.setBackground(color);
        panel.add(addNewB, layConstraints);

        JButton returnB = new JButton("Назад");
        returnB.setBounds(WINDOW_WIDTH-smallBUTTON_WIDTH, WINDOW_HEIGHT-BUTTON_HEIGHT, 170, 70 );
        final ArrayList<TrainingAssignmentEntity> trainingAssignmentEntities = this.trainingAssignmentController.getAll();
        returnB.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    dispose();
                    ViewTrainingAssignment viewTrainingAssignment = new ViewTrainingAssignment(trainingAssignmentEntities, trainingAssignmentController);
                    ViewSaveSuccess viewSaveSuccess = new ViewSaveSuccess();
                }catch (SQLException eds){
                    ViewMistake viewMistake = new ViewMistake(eds.getMessage());
                    eds.printStackTrace(); }
            }
        });
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 2;
        layConstraints.gridy = 4;
        returnB.setBackground(color);
        panel.add(returnB, layConstraints);
        color = new Color (240, 240, 240);
        panel.setBackground(color);
        pack();
        setPreferredSize(new Dimension(WINDOW_WIDTH, WINDOW_HEIGHT));
        setVisible(true);
    }

    private JComboBox createCurrNum(){
        final JComboBox currBox = new JComboBox();
        Iterator<Integer> iter2 = ocupation.iterator();
        int k = 0, g = ocupation.size();
        while(iter2.hasNext()){
            if (k == g) break;
            currBox.addItem(ocupation.get(k));
            k++;
        }
        currBox.setAlignmentX(LEFT_ALIGNMENT);
        return currBox;
    }

}
