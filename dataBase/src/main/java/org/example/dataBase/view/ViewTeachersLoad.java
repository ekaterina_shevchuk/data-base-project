package org.example.dataBase.view;

import org.example.dataBase.controller.TeachersController;
import org.example.dataBase.model.ScientificWorkEntity;
import org.example.dataBase.model.TrainingAssignmentEntity;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;

public class ViewTeachersLoad extends JFrame {
    static int WINDOW_WIDTH = 1350;
    static int WINDOW_HEIGHT = 750;
    static int PANAL_WIDTH = 1100;
    static int PANAL_HEIGHT = 70;
    static int BUTTON_WIDTH = 350;
    static int BUTTON_HEIGHT = 50;

    TeachersController teachersController;
    int colorC = 0;

    public ViewTeachersLoad(ArrayList<TrainingAssignmentEntity> trainingAssignmentEntities, ArrayList<Integer> numHours, int sumNumHours, TeachersController teachersController1) throws SQLException {
        teachersController = teachersController1;
        Container c = getContentPane();
        c.setLayout(new BorderLayout());
        setTitle("Просмотр нагрузки  преподавателей");
        setMinimumSize(new Dimension(WINDOW_WIDTH, WINDOW_HEIGHT));
        setResizable(false);

        JPanel panel = new JPanel();
        panel.setBorder(new EmptyBorder(10, 10, 10, 10));
        GridBagLayout gridBagLayout = new GridBagLayout();
        gridBagLayout.rowHeights = new int[]{BUTTON_HEIGHT, BUTTON_HEIGHT,BUTTON_HEIGHT,BUTTON_HEIGHT,BUTTON_HEIGHT };
        gridBagLayout.columnWeights = new double[]{1.0};
        gridBagLayout.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0};
        panel.setLayout(gridBagLayout);
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.weightx = 1;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.gridwidth = GridBagConstraints.REMAINDER;
        c.add(panel);
        JScrollPane jScrollPane = new JScrollPane(panel);
        jScrollPane.setPreferredSize(new Dimension(WINDOW_WIDTH, WINDOW_HEIGHT));

        jScrollPane.setLayout(new ScrollPaneLayout());
        c.add(jScrollPane);
        jScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
        panel.add(article(), gbc);

        Iterator<TrainingAssignmentEntity> iter1 = trainingAssignmentEntities.iterator();
        Iterator<Integer> iter2 = numHours.iterator();
        while(iter1.hasNext()&&(iter2.hasNext())){
            panel.add(createNewJPanel(iter1.next(), iter2.next()), gbc);
        }
        panel.add(sumNumHoursPan(sumNumHours));
        panel.add(createBut(), gbc);
        Color color = new Color (240, 240, 240);
        panel.setBackground(color);
        pack();
        setPreferredSize(new Dimension(WINDOW_WIDTH, WINDOW_HEIGHT));
        setVisible(true);
    }

    private JPanel createNewJPanel(TrainingAssignmentEntity model, int numHours) {
        JPanel pan = new JPanel();
        int otst = 20;
        pan.setPreferredSize(new Dimension(PANAL_WIDTH, PANAL_HEIGHT));
        pan.setLayout(null);

        JLabel fioJ = new JLabel(model.getFIOtch());
        fioJ.setBounds(otst, PANAL_HEIGHT - BUTTON_HEIGHT - 10, 5*BUTTON_WIDTH/7, BUTTON_HEIGHT);
        fioJ.setFont(new Font("Times New Roman", NORMAL, 15));
        fioJ.setBackground(Color.gray);
        fioJ.setForeground(Color.DARK_GRAY);
        pan.add(fioJ);
        otst += 5*BUTTON_WIDTH/7;
        JLabel jobJ = new JLabel(model.getTitleSub());
        jobJ.setBounds(otst, PANAL_HEIGHT - BUTTON_HEIGHT - 10, 5*BUTTON_WIDTH / 7, BUTTON_HEIGHT);
        jobJ.setFont(new Font("Times New Roman", NORMAL, 15));
        jobJ.setBackground(Color.gray);
        jobJ.setForeground(Color.DARK_GRAY);
        pan.add(jobJ);

        otst += 5*BUTTON_WIDTH/7;
        JLabel numHJ = new JLabel(String.valueOf(numHours));
        numHJ.setBounds(otst, PANAL_HEIGHT - BUTTON_HEIGHT - 10, BUTTON_WIDTH / 3, BUTTON_HEIGHT);
        numHJ.setFont(new Font("Times New Roman", NORMAL, 15));
        numHJ.setBackground(Color.gray);
        numHJ.setForeground(Color.DARK_GRAY);
        pan.add(numHJ);

        colorC++;
        Color col1 = new Color(250, 250, 250);
        Color col2 = new Color(225, 225, 225);
        if(colorC%2 == 0){
            pan.setBackground(col1);
        }else pan.setBackground(col2 );
        return pan;
    }

    private JPanel sumNumHoursPan(int sumNumHours) {
        JPanel pan = new JPanel();
        int otst = 20;
        pan.setPreferredSize(new Dimension(PANAL_WIDTH, PANAL_HEIGHT));
        pan.setLayout(null);

        otst += 5*BUTTON_WIDTH/7;
        JLabel jobJ = new JLabel("Итого");
        jobJ.setBounds(otst, PANAL_HEIGHT - BUTTON_HEIGHT - 10, 5*BUTTON_WIDTH / 7, BUTTON_HEIGHT);
        jobJ.setFont(new Font("Times New Roman", NORMAL, 15));
        jobJ.setBackground(Color.gray);
        jobJ.setForeground(Color.DARK_GRAY);
        pan.add(jobJ);

        otst += 5*BUTTON_WIDTH/7;
        JLabel numHJ = new JLabel(String.valueOf(sumNumHours));
        numHJ.setBounds(otst, PANAL_HEIGHT - BUTTON_HEIGHT - 10, BUTTON_WIDTH / 3, BUTTON_HEIGHT);
        numHJ.setFont(new Font("Times New Roman", NORMAL, 15));
        numHJ.setBackground(Color.gray);
        numHJ.setForeground(Color.DARK_GRAY);
        pan.add(numHJ);

        colorC++;
        Color col1 = new Color(250, 250, 250);
        Color col2 = new Color(225, 225, 225);
        if(colorC%2 == 0){
            pan.setBackground(col1);
        }else pan.setBackground(col2 );
        return pan;
    }


    private JPanel article(){
        JPanel pan = new JPanel();
        int otst = 20;

        pan.setPreferredSize(new Dimension(PANAL_WIDTH, PANAL_HEIGHT));
        pan.setLayout(null);

        JLabel fioJ = new JLabel("Фио преподавателя");
        fioJ.setBounds(otst, PANAL_HEIGHT - BUTTON_HEIGHT - 10, 5*BUTTON_WIDTH/7, BUTTON_HEIGHT);
        fioJ.setFont(new Font("Times New Roman", NORMAL, 15));
        fioJ.setBackground(Color.gray);
        fioJ.setForeground(Color.DARK_GRAY);
        pan.add(fioJ);
        otst += 5*BUTTON_WIDTH/7;
        JLabel jobJ = new JLabel("Название предмета");
        jobJ.setBounds(otst, PANAL_HEIGHT - BUTTON_HEIGHT - 10, 5*BUTTON_WIDTH / 7, BUTTON_HEIGHT);
        jobJ.setFont(new Font("Times New Roman", NORMAL, 15));
        jobJ.setBackground(Color.gray);
        jobJ.setForeground(Color.DARK_GRAY);
        pan.add(jobJ);

        otst += 5*BUTTON_WIDTH/7;
        JLabel numHJ = new JLabel("Количество часов");
        numHJ.setBounds(otst, PANAL_HEIGHT - BUTTON_HEIGHT - 10, BUTTON_WIDTH / 3, BUTTON_HEIGHT);
        numHJ.setFont(new Font("Times New Roman", NORMAL, 15));
        numHJ.setBackground(Color.gray);
        numHJ.setForeground(Color.DARK_GRAY);
        pan.add(numHJ);

        colorC++;
        Color col1 = new Color(250, 250, 250);
        Color col2 = new Color(225, 225, 225);
        if(colorC%2 == 0){
            pan.setBackground(col1);
        }else pan.setBackground(col2 );
        return pan;
    }

    private JPanel createBut() throws SQLException{
        Color color = new Color (240, 240, 240);
        JPanel panel2 = new JPanel();
        panel2.setPreferredSize(new Dimension(PANAL_WIDTH, PANAL_HEIGHT));
        panel2.setLayout(null);

        JButton returnB = new JButton("Назад");
        final SearchTeachersLoad searchTeachersLoad = new SearchTeachersLoad(teachersController);
        returnB.setBounds(PANAL_WIDTH - BUTTON_WIDTH, BUTTON_HEIGHT/2, 70, 30 );
        returnB.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    dispose();
                    searchTeachersLoad.visible();
                }catch (SQLException eds){
                    eds.printStackTrace(); }
            }
        });
        returnB.setBackground(color);
        panel2.add(returnB);

        return panel2;
    }
}
