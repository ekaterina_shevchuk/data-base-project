package org.example.dataBase.view;

import javax.swing.*;
import javax.swing.border.EmptyBorder;

import org.example.dataBase.controller.DataBaseController;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;

public class AutorisationPage extends JFrame {
    static int WINDOW_WIDTH = 800;
    static int WINDOW_HEIGHT = 500;
    static int PANAL_WIDTH = 750;
    static int PANAL_HEIGHT = 150;
    static int BUTTON_WIDTH = 100;
    static int BUTTON_HEIGHT = 40;

    private int id;
    private String password;

    public AutorisationPage(final DataBaseController dataBaseController, final String role, final int roleInt){
        Container c = getContentPane();
        c.setLayout(new BorderLayout());
        setTitle("Вход");
        setMinimumSize(new Dimension(WINDOW_WIDTH, WINDOW_HEIGHT));
        setResizable(false);

        JPanel panel = new JPanel();
        panel.setBorder(new EmptyBorder(10, 10, 10, 10));
        GridBagLayout gridBagLayout = new GridBagLayout();
        gridBagLayout.rowHeights = new int[]{BUTTON_HEIGHT, BUTTON_HEIGHT, BUTTON_HEIGHT, BUTTON_HEIGHT, BUTTON_HEIGHT,BUTTON_HEIGHT, BUTTON_HEIGHT, BUTTON_HEIGHT, BUTTON_HEIGHT, BUTTON_HEIGHT, BUTTON_HEIGHT};
        gridBagLayout.columnWeights = new double[]{1.0};
        gridBagLayout.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0};
        panel.setLayout(gridBagLayout);
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.weightx = 1;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.gridwidth = GridBagConstraints.REMAINDER;
        c.add(panel);
        JScrollPane jScrollPane = new JScrollPane(panel);
        jScrollPane.setPreferredSize(new Dimension(WINDOW_WIDTH, WINDOW_HEIGHT));

        jScrollPane.setLayout(new ScrollPaneLayout());
        c.add(jScrollPane);
        jScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
        GridBagConstraints layConstraints;

        Color color = new Color (240, 240, 240);
        JLabel idL = new JLabel("Введите свой ID");
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 0;
        layConstraints.gridy = 3;
        panel.add(idL,layConstraints);

        final JTextField idField = new JTextField("", 10);
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 1;
        layConstraints.gridy = 3;
        /*idField.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
               id = Integer.parseInt(idField.getText());
            }
        });*/
        panel.add(idField, layConstraints);

        JLabel passL = new JLabel("Введите свой пароль");
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 0;
        layConstraints.gridy = 4;
        panel.add(passL,layConstraints);
        final JPasswordField passField = new JPasswordField("", 10);
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 1;
        layConstraints.gridy = 4;
        /*fioField.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              fio = fioField.getText();
            }
        });*/
        panel.add(passField, layConstraints);


        JButton loginB = new JButton("Войти");
        loginB.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    id = Integer.parseInt(idField.getText());

                    password = passField.getText();
                    int check = dataBaseController.checkLogin(id, password);
                    dataBaseController.setRole(roleInt);
                    dataBaseController.setFaculty(id);
                    if (check == 1){
                        check = dataBaseController.checkRole(role, id);
                        if (check == 1) {
                            dispose();
                            dataBaseController.openMain(roleInt);
                        }else if (check == 0){
                            ViewMistake viewMistake = new ViewMistake("Пользователь не найден. Пожалуйста проверьте роль, под которой вы хотите зайти.");
                        }
                    }else if (check == 0){
                        ViewMistake viewMistake = new ViewMistake("Пользователь не найден. Пожалуйста проверьте введенные данные.");
                    }
                }catch (SQLException eds){eds.printStackTrace();}
            }
        });
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 4;
        layConstraints.gridy = 0;
        loginB.setBackground(color);
        panel.add(loginB,layConstraints);

        JButton returnB = new JButton("Назад");
        returnB.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                    dispose();
                    FirstPage firstPage = new FirstPage(dataBaseController);
            }
        });
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 2;
        layConstraints.gridy = 5;
        returnB.setBackground(color);
        panel.add(returnB, layConstraints);

        color = new Color (250, 250, 250);
        panel.setBackground(color);
        pack();
        setPreferredSize(new Dimension(WINDOW_WIDTH, WINDOW_HEIGHT));
        setVisible(true);
    }

}
