package org.example.dataBase.view;

import org.example.dataBase.controller.*;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class FirstPage extends JFrame {
    static int WINDOW_WIDTH = 800;
    static int WINDOW_HEIGHT = 500;
    static int PANAL_WIDTH = 750;
    static int PANAL_HEIGHT = 150;
    static int BUTTON_WIDTH = 100;
    static int BUTTON_HEIGHT = 40;

    public FirstPage(final DataBaseController dataBaseController)  {
        Color color = new Color (240, 240, 240);
        Container c = getContentPane();
        c.setLayout(new BorderLayout());
        setTitle("Информационная система вуза. Вход");
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setMinimumSize(new Dimension(WINDOW_WIDTH, WINDOW_HEIGHT));

        JPanel panel = new JPanel();
        panel.setBorder(new EmptyBorder(10, 10, 10, 10));
        final GridBagLayout gridBagLayout = new GridBagLayout();
        gridBagLayout.rowHeights = new int[]{BUTTON_HEIGHT, BUTTON_HEIGHT,BUTTON_HEIGHT,BUTTON_HEIGHT,BUTTON_HEIGHT,BUTTON_HEIGHT};
        gridBagLayout.columnWeights = new double[]{1.0, 1.0, 1.0};
        gridBagLayout.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0};
        panel.setLayout(gridBagLayout);

        GridBagConstraints gbc = new GridBagConstraints();
        gbc.weightx = 1;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.gridwidth = GridBagConstraints.REMAINDER;
        c.add(panel);
        JScrollPane jScrollPane = new JScrollPane(panel);
        jScrollPane.setPreferredSize(new Dimension(WINDOW_WIDTH, WINDOW_HEIGHT));

        jScrollPane.setLayout(new ScrollPaneLayout());
        c.add(jScrollPane);
        jScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);

        GridBagConstraints layConstraints;

        JLabel article = new JLabel("Войти как ");
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 1;
        layConstraints.gridy = 0;
        panel.add(article, layConstraints);

        final String tch = "Преподаватель";
        JButton tchButton = new JButton(tch);
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 0;
        layConstraints.gridy = 2;

        tchButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
                AutorisationPage autorisationPage = new AutorisationPage(dataBaseController, tch, 0);
            }
        });
        tchButton.setBackground(color);
        panel.add(tchButton, layConstraints);

        /*String depWork = "Работник кафедры";
        JButton depButton = new JButton(depWork);
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 2;
        layConstraints.gridy = 2;
        depButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();

            }
        });
        depButton.setBackground(color);
        panel.add(depButton, layConstraints);*/

        final String deanWork = "Работник деканата";
        JButton deanButton = new JButton(deanWork);
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 1;
        layConstraints.gridy = 2;
        deanButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
                AutorisationPage autorisationPage = new AutorisationPage(dataBaseController, deanWork, 1);
            }
        });
        deanButton.setBackground(color);
        panel.add(deanButton, layConstraints);

        final String rectorWork = "Работник ректората";
        JButton rectorButton = new JButton(rectorWork);
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 2;
        layConstraints.gridy = 2;
        rectorButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
                AutorisationPage autorisationPage = new AutorisationPage(dataBaseController, rectorWork, 2);
            }
        });
        rectorButton.setBackground(color);
        panel.add(rectorButton, layConstraints);

        color = new Color (250, 250, 250);
        panel.setBackground(color);
        pack();
        setPreferredSize(new Dimension(WINDOW_WIDTH, WINDOW_HEIGHT));
        setVisible(true);
    }


}
