package org.example.dataBase.view;

import org.example.dataBase.controller.StudentsController;
import org.example.dataBase.model.StudentsEntity;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Date;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;

public class UpdateStudent extends JFrame {
    static int WINDOW_WIDTH = 1350;
    static int WINDOW_HEIGHT = 750;
    static int PANAL_WIDTH = 1100;
    static int PANAL_HEIGHT = 70;
    static int BUTTON_WIDTH = 350;
    static int BUTTON_HEIGHT = 50;
    static int smallBUTTON_WIDTH = 50;
    static int bigBUTTON_WIDTH = 200;

    private String answ = "";
    private StudentsController studentsController;

    public UpdateStudent(StudentsController studentsController1)throws SQLException {
        studentsController = studentsController1;
    }

    public void visibleUS() throws SQLException{
        Color color = new Color (240, 240, 240);
        final StudentsEntity studentsEntity = new StudentsEntity();
        Container c = getContentPane();
        c.setLayout(new BorderLayout());
        setTitle("Изменение  информации о студенте");
        setMinimumSize(new Dimension(WINDOW_WIDTH, WINDOW_HEIGHT));
        setResizable(false);

        JPanel panel = new JPanel();
        panel.setBorder(new EmptyBorder(10, 10, 10, 10));
        GridBagLayout gridBagLayout = new GridBagLayout();
        gridBagLayout.rowHeights = new int[]{BUTTON_HEIGHT, BUTTON_HEIGHT,BUTTON_HEIGHT,BUTTON_HEIGHT,BUTTON_HEIGHT,BUTTON_HEIGHT,BUTTON_HEIGHT };
        gridBagLayout.columnWeights = new double[]{1.0};
        gridBagLayout.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0};
        panel.setLayout(gridBagLayout);

        GridBagConstraints gbc = new GridBagConstraints();
        gbc.weightx = 1;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.gridwidth = GridBagConstraints.REMAINDER;
        c.add(panel);
        JScrollPane jScrollPane = new JScrollPane(panel);
        jScrollPane.setPreferredSize(new Dimension(WINDOW_WIDTH, WINDOW_HEIGHT));

        jScrollPane.setLayout(new ScrollPaneLayout());
        c.add(jScrollPane);
        jScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);

        GridBagConstraints layConstraints;

        final JTextField fioField, bdField, childFiels, ssField;

        JLabel fioLabel = new JLabel("Введите ФИО студента");
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 0;
        layConstraints.gridy = 1;
        panel.add(fioLabel,layConstraints);
        fioField = new JTextField("", 40);
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 1;
        layConstraints.gridy = 1;
        panel.add(fioField, layConstraints);

        JLabel jLabel = new JLabel("Выберите номер новой группы");
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 0;
        layConstraints.gridy = 2;
        panel.add(jLabel,layConstraints);
        ArrayList<Integer> groups = studentsController.getAllGroups();
        final JComboBox groupBox = new JComboBox();
        Iterator<Integer> iter = groups.iterator();
        int k = 0, g = groups.size();
        while(iter.hasNext()){
            if (k == g) break;
            groupBox.addItem(groups.get(k));
            k++;
        }
        groupBox.setAlignmentX(LEFT_ALIGNMENT);
        groupBox.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                answ += "1 ";
                studentsEntity.setNum_gr((Integer) groupBox.getSelectedItem());
            }
        });
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 1;
        layConstraints.gridy = 2;
        panel.add(groupBox, layConstraints);

        JLabel childLabel = new JLabel("Введите количество детей");
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 0;
        layConstraints.gridy = 3;
        panel.add(childLabel,layConstraints);
        childFiels = new JTextField("", 40);
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 1;
        layConstraints.gridy = 3;
        panel.add(childFiels, layConstraints);

        JLabel schLabel = new JLabel("Введите стипендию");
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 0;
        layConstraints.gridy = 4;
        panel.add(schLabel,layConstraints);
        ssField = new JTextField("", 40);
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 1;
        layConstraints.gridy = 4;
        panel.add(ssField, layConstraints);

        JButton addNewB = new JButton("Изменить");
        addNewB.setBounds(50, WINDOW_HEIGHT-BUTTON_HEIGHT, 170, 70 );
        addNewB.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    if (fioField.getText().equals("")){
                        ViewMistake viewMistake = new ViewMistake("Введите ФИО студента!");
                    }else{
                        studentsEntity.setFIO(fioField.getText());
                    }
                    if (!(childFiels.getText().equals(""))){
                        answ += "2 ";
                        studentsEntity.setCheckChild(Integer.parseInt(childFiels.getText()));
                    }else {
                        studentsEntity.setCheckChild(-1);
                    }
                    if (!(ssField.getText().equals(""))){
                        answ += "3 ";
                        studentsEntity.setScholarship(Integer.parseInt(ssField.getText()));
                    }else {
                        studentsEntity.setScholarship(-1);
                    }
                    callUpdate(studentsEntity.getNum_gr(), studentsEntity.getCheckChild(), studentsEntity.getScholarship(), studentsEntity.getFIO());
                    ViewSaveSuccess viewSaveSuccess = new ViewSaveSuccess();
                }catch (SQLException eds){
                    ViewMistake viewMistake = new ViewMistake(eds.getMessage());
                    eds.printStackTrace(); }
            }
        });
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 2;
        layConstraints.gridy = 1;
        addNewB.setBackground(color);
        panel.add(addNewB, layConstraints);

        final JButton resetB = new JButton("Сброс");
        resetB.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                answ = "";
                studentsEntity.setFIO("");
                studentsEntity.setScholarship(-1);
                studentsEntity.setCheckChild(-1);
                studentsEntity.setNum_gr(0);
                fioField.setText("");
                ssField.setText("");
                childFiels.setText("");
            }
        });
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 4;
        layConstraints.gridy = 1;
        resetB.setBackground(color);
        panel.add(resetB,layConstraints);

        JButton returnB = new JButton("Назад");
        returnB.setBounds(WINDOW_WIDTH-smallBUTTON_WIDTH, WINDOW_HEIGHT-BUTTON_HEIGHT, 170, 70 );
        final ArrayList<StudentsEntity> studentsEntities = this.studentsController.getAll();
        returnB.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    dispose();
                    ViewStudents viewStudents = new ViewStudents(studentsEntities, studentsController);
                }catch (SQLException eds){
                    eds.printStackTrace();
                }
            }
        });
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 2;
        layConstraints.gridy = 4;
        returnB.setBackground(color);
        panel.add(returnB, layConstraints);
        color = new Color (240, 240, 240);
        panel.setBackground(color);
        pack();
        setPreferredSize(new Dimension(WINDOW_WIDTH, WINDOW_HEIGHT));
        setVisible(true);
    }

    private void callUpdate(int group, int checkCh, int scholarShip, String fio)throws SQLException{
        if (answ.equals("1 ")){
            studentsController.updateSt(group, -1, -1, fio);
        }else if (answ.equals("2 ")){
            studentsController.updateSt(0, checkCh, -1, fio);
        }else if (answ.equals("3 ")){
            studentsController.updateSt(0, -1, scholarShip, fio);
        }else if (answ.equals("1 2 ") || answ.equals("2 1 ")){
            studentsController.updateSt(group, checkCh, -1, fio);
        }else if (answ.equals("1 3 ") || answ.equals("3 1 ")){
            studentsController.updateSt(group, -1, scholarShip, fio);
        }else if (answ.equals("2 3 ") || answ.equals("3 2 ")){
            studentsController.updateSt(0, checkCh, scholarShip, fio);
        }else if (answ.equals("1 2 3 ") || answ.equals("1 3 2 ") || answ.equals("2 3 1") || answ.equals("2 1 3 ")
                || answ.equals("3 2 1 ") || answ.equals("3 1 2 ")){
            studentsController.updateSt(group, checkCh, scholarShip, fio);
        }else {
            ViewMistake viewMistake = new ViewMistake("По вашему запросу ничего не найдено.");}
    }
}
