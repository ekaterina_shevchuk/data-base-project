package org.example.dataBase.view;

import org.example.dataBase.controller.ScientificDegreeTchController;
import org.example.dataBase.model.ScientificDegreeTchEntity;
import org.example.dataBase.model.ScientificWorkEntity;
import org.example.dataBase.service.ScientificDegreeTchService;
import org.example.dataBase.service.ScientificWorkService;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Date;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;

public class AddScientificDegreeTch extends JFrame {
    static int WINDOW_WIDTH = 1350;
    static int WINDOW_HEIGHT = 750;
    static int PANAL_WIDTH = 1100;
    static int PANAL_HEIGHT = 70;
    static int BUTTON_WIDTH = 350;
    static int BUTTON_HEIGHT = 50;

    final ScientificDegreeTchController scientificDegreeTchController;

    public AddScientificDegreeTch(ScientificDegreeTchController scientificDegreeTchController1)throws SQLException {
        scientificDegreeTchController = scientificDegreeTchController1;
    }

    public void visibleAS() throws SQLException{
        Color color = new Color (240, 240, 240);
        final ScientificDegreeTchEntity scientificDegreeTchEntity = new ScientificDegreeTchEntity();
        Container c = getContentPane();
        c.setLayout(new BorderLayout());
        setTitle("Добавление новой степени");
        setMinimumSize(new Dimension(WINDOW_WIDTH, WINDOW_HEIGHT));
        setResizable(false);

        JPanel panel = new JPanel();
        panel.setBorder(new EmptyBorder(10, 10, 10, 10));
        final GridBagLayout gridBagLayout = new GridBagLayout();
        gridBagLayout.rowHeights = new int[]{BUTTON_HEIGHT, BUTTON_HEIGHT,BUTTON_HEIGHT,BUTTON_HEIGHT,BUTTON_HEIGHT };
        gridBagLayout.columnWeights = new double[]{1.0};
        gridBagLayout.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0};
        panel.setLayout(gridBagLayout);

        GridBagConstraints gbc = new GridBagConstraints();
        gbc.weightx = 1;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.gridwidth = GridBagConstraints.REMAINDER;
        c.add(panel);
        JScrollPane jScrollPane = new JScrollPane(panel);
        jScrollPane.setPreferredSize(new Dimension(WINDOW_WIDTH, WINDOW_HEIGHT));

        jScrollPane.setLayout(new ScrollPaneLayout());
        c.add(jScrollPane);
        jScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);

        GridBagConstraints layConstraints;

        JLabel tchLabel = new JLabel("Выберите преподавателя");
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 0;
        layConstraints.gridy = 1;
        panel.add(tchLabel,layConstraints);
        ArrayList<String> teachersFIO = scientificDegreeTchController.getFIOtch();
        final JComboBox teachersBox = new JComboBox();
        Iterator<String> iter3 = teachersFIO.iterator();
        int k = 0, g = teachersFIO.size();
        while(iter3.hasNext()){
            if (k == g) break;
            teachersBox.addItem(teachersFIO.get(k));
            k++;
        }
        teachersBox.setAlignmentX(LEFT_ALIGNMENT);
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 1;
        layConstraints.gridy = 1;
        panel.add(teachersBox, layConstraints);

        JLabel degLabel = new JLabel("Выберите научную степень");
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 0;
        layConstraints.gridy = 2;
        panel.add(degLabel,layConstraints);
        ArrayList<String> degrees = new ArrayList<String>();
        degrees.add("Доктор наук");
        degrees.add("Кандидат наук");
        final JComboBox degBox = new JComboBox();
        Iterator<String> iter1 = teachersFIO.iterator();
        k = 0;
        g = degrees.size();
        while(iter1.hasNext()){
            if (k == g) break;
            degBox.addItem(degrees.get(k));
            k++;
        }
        degBox.setAlignmentX(LEFT_ALIGNMENT);
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 1;
        layConstraints.gridy = 2;
        panel.add(degBox, layConstraints);


        JButton addNewB = new JButton("Добавить");
        addNewB.setBounds(50, WINDOW_HEIGHT-BUTTON_HEIGHT, 170, 70 );
        addNewB.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    scientificDegreeTchEntity.setFIOtch((String) teachersBox.getSelectedItem());
                    scientificDegreeTchEntity.setTitleDegree((String) degBox.getSelectedItem());
                    scientificDegreeTchController.addScientificWork(scientificDegreeTchEntity);
                    ViewSaveSuccess viewSaveSuccess = new ViewSaveSuccess();
                }catch (SQLException eds){
                    ViewMistake viewMistake = new ViewMistake(eds.getMessage());
                    eds.printStackTrace(); }
            }
        });
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 2;
        layConstraints.gridy = 1;
        addNewB.setBackground(color);
        panel.add(addNewB, layConstraints);

        JButton returnB = new JButton("Назад");
        returnB.setBounds(WINDOW_WIDTH-BUTTON_WIDTH, WINDOW_HEIGHT-BUTTON_HEIGHT, 170, 70 );
        final ArrayList<ScientificDegreeTchEntity> scientificDegreeTchEntities = this.scientificDegreeTchController.getAll();
        returnB.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    dispose();
                    ViewScientificDegreeTch viewScientificDegreeTch = new ViewScientificDegreeTch(scientificDegreeTchEntities, scientificDegreeTchController);
                }catch (SQLException eds){
                    eds.printStackTrace();
                }
            }
        });
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 2;
        layConstraints.gridy = 4;
        returnB.setBackground(color);
        panel.add(returnB, layConstraints);
        color = new Color (240, 240, 240);
        panel.setBackground(color);
        pack();
        setPreferredSize(new Dimension(WINDOW_WIDTH, WINDOW_HEIGHT));
        setVisible(true);
    }
}
