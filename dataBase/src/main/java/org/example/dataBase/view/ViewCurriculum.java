package org.example.dataBase.view;

import org.example.dataBase.controller.CurriculumController;
import org.example.dataBase.model.CurriculumEntity;
import org.example.dataBase.model.TeachersEntity;
import org.example.dataBase.service.CurriculumService;
import org.example.dataBase.service.TeachersService;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;

public class ViewCurriculum extends JFrame {
    static int WINDOW_WIDTH = 1350;
    static int WINDOW_HEIGHT = 750;
    static int PANAL_WIDTH = 1100;
    static int PANAL_HEIGHT = 70;
    static int BUTTON_WIDTH = 350;
    static int BUTTON_HEIGHT = 50;

    CurriculumController curriculumController;
    int colorC = 0;

    public ViewCurriculum(ArrayList<CurriculumEntity> curriculumEntities, CurriculumController curriculumController1) throws SQLException {
        curriculumController = curriculumController1;
        Container c = getContentPane();
        c.setLayout(new BorderLayout());
        setTitle("Просмотр учебного плана");
        setMinimumSize(new Dimension(WINDOW_WIDTH, WINDOW_HEIGHT));
        //setResizable(false);

        JPanel panel = new JPanel();
        panel.setBorder(new EmptyBorder(10, 10, 10, 10));
        GridBagLayout gridBagLayout = new GridBagLayout();
        gridBagLayout.rowHeights = new int[]{BUTTON_HEIGHT, BUTTON_HEIGHT,BUTTON_HEIGHT,BUTTON_HEIGHT,BUTTON_HEIGHT };
        gridBagLayout.columnWeights = new double[]{1.0};
        gridBagLayout.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0};
        panel.setLayout(gridBagLayout);
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.weightx = 1;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.gridwidth = GridBagConstraints.REMAINDER;
        c.add(panel);
        JScrollPane jScrollPane = new JScrollPane(panel);
        jScrollPane.setPreferredSize(new Dimension(WINDOW_WIDTH, WINDOW_HEIGHT));

        jScrollPane.setLayout(new ScrollPaneLayout());
        c.add(jScrollPane);
        jScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
        panel.add(createBut(), gbc);

        panel.add(article(), gbc);

        Iterator<CurriculumEntity> iter = curriculumEntities.iterator();
        while(iter.hasNext()){
            panel.add(createNewJPanel(iter.next()), gbc);
        }
        Color color = new Color (240, 240, 240);
        panel.setBackground(color);
        pack();
        setPreferredSize(new Dimension(WINDOW_WIDTH, WINDOW_HEIGHT));
        setVisible(true);
    }

    private JPanel createNewJPanel(CurriculumEntity model) {
        JPanel pan = new JPanel();
        int otst = 20;
        pan.setPreferredSize(new Dimension(PANAL_WIDTH, PANAL_HEIGHT));
        pan.setLayout(null);

        JLabel idJ = new JLabel(String.valueOf(model.getID_t()));
        idJ.setBounds(otst, PANAL_HEIGHT - BUTTON_HEIGHT - 10, BUTTON_WIDTH / 3, BUTTON_HEIGHT);
        idJ.setFont(new Font("Times New Roman", NORMAL, 15));
        idJ.setBackground(Color.gray);
        idJ.setForeground(Color.DARK_GRAY);
        pan.add(idJ);
        otst += BUTTON_WIDTH / 3;

        JLabel fioJ = new JLabel(model.getTitleSub());
        fioJ.setBounds(otst, PANAL_HEIGHT - BUTTON_HEIGHT - 10, BUTTON_WIDTH, BUTTON_HEIGHT);
        fioJ.setFont(new Font("Times New Roman", NORMAL, 15));
        fioJ.setBackground(Color.gray);
        fioJ.setForeground(Color.DARK_GRAY);
        pan.add(fioJ);
        otst += BUTTON_WIDTH;

        JLabel jobJ = new JLabel(String.valueOf(model.getCourse()));
        jobJ.setBounds(otst, PANAL_HEIGHT - BUTTON_HEIGHT - 10, BUTTON_WIDTH / 3, BUTTON_HEIGHT);
        jobJ.setFont(new Font("Times New Roman", NORMAL, 15));
        jobJ.setBackground(Color.gray);
        jobJ.setForeground(Color.DARK_GRAY);
        pan.add(jobJ);
        otst += BUTTON_WIDTH / 3;
        JLabel catJ = new JLabel(String.valueOf(model.getNum_gr()));
        catJ.setBounds(otst, PANAL_HEIGHT - BUTTON_HEIGHT - 10, BUTTON_WIDTH / 5, BUTTON_HEIGHT);
        catJ.setFont(new Font("Times New Roman", NORMAL, 15));
        catJ.setBackground(Color.gray);
        catJ.setForeground(Color.DARK_GRAY);
        pan.add(catJ);
        otst+=BUTTON_WIDTH / 5;
        JLabel genderJ = new JLabel(model.getName_fa());
        genderJ.setBounds(otst , PANAL_HEIGHT - BUTTON_HEIGHT - 10, BUTTON_WIDTH , BUTTON_HEIGHT);
        genderJ.setFont(new Font("Times New Roman", NORMAL, 15));
        genderJ.setBackground(Color.gray);
        genderJ.setForeground(Color.DARK_GRAY);
        pan.add(genderJ);
        otst += BUTTON_WIDTH ;
        JLabel bdJ = new JLabel(String.valueOf(model.getSemester()));
        bdJ.setBounds(otst, PANAL_HEIGHT - BUTTON_HEIGHT - 10, BUTTON_WIDTH / 3, BUTTON_HEIGHT);
        bdJ.setFont(new Font("Times New Roman", NORMAL, 15));
        bdJ.setBackground(Color.gray);
        bdJ.setForeground(Color.DARK_GRAY);
        pan.add(bdJ);
        otst += BUTTON_WIDTH / 3;
        JLabel childJ = new JLabel(String.valueOf(model.getYear()));
        childJ.setBounds(otst, PANAL_HEIGHT - BUTTON_HEIGHT - 10, BUTTON_WIDTH / 3, BUTTON_HEIGHT);
        childJ.setFont(new Font("Times New Roman", NORMAL, 15));
        childJ.setBackground(Color.gray);
        childJ.setForeground(Color.DARK_GRAY);
        pan.add(childJ);

        colorC++;
        Color col1 = new Color(250, 250, 250);
        Color col2 = new Color(225, 225, 225);
        if(colorC%2 == 0){
            pan.setBackground(col1);
        }else pan.setBackground(col2 );
        return pan;
    }

    private JPanel article(){
        JPanel pan = new JPanel();
        int otst = 20;
        pan.setPreferredSize(new Dimension(PANAL_WIDTH, PANAL_HEIGHT));
        pan.setLayout(null);

        JLabel idJ = new JLabel("Номер");
        idJ.setBounds(otst, PANAL_HEIGHT - BUTTON_HEIGHT - 10, BUTTON_WIDTH / 3, BUTTON_HEIGHT);
        idJ.setFont(new Font("Times New Roman", NORMAL, 15));
        idJ.setBackground(Color.gray);
        idJ.setForeground(Color.DARK_GRAY);
        pan.add(idJ);
        otst += BUTTON_WIDTH / 3;

        JLabel fioJ = new JLabel("Название предмета");
        fioJ.setBounds(otst, PANAL_HEIGHT - BUTTON_HEIGHT - 10, BUTTON_WIDTH, BUTTON_HEIGHT);
        fioJ.setFont(new Font("Times New Roman", NORMAL, 15));
        fioJ.setBackground(Color.gray);
        fioJ.setForeground(Color.DARK_GRAY);
        pan.add(fioJ);
        otst += BUTTON_WIDTH;

        JLabel jobJ = new JLabel("Курс");
        jobJ.setBounds(otst, PANAL_HEIGHT - BUTTON_HEIGHT - 10, BUTTON_WIDTH / 3, BUTTON_HEIGHT);
        jobJ.setFont(new Font("Times New Roman", NORMAL, 15));
        jobJ.setBackground(Color.gray);
        jobJ.setForeground(Color.DARK_GRAY);
        pan.add(jobJ);
        otst += BUTTON_WIDTH / 3;
        JLabel catJ = new JLabel("Группа");
        catJ.setBounds(otst, PANAL_HEIGHT - BUTTON_HEIGHT - 10, BUTTON_WIDTH / 5, BUTTON_HEIGHT);
        catJ.setFont(new Font("Times New Roman", NORMAL, 15));
        catJ.setBackground(Color.gray);
        catJ.setForeground(Color.DARK_GRAY);
        pan.add(catJ);
        otst+=BUTTON_WIDTH / 5;
        JLabel genderJ = new JLabel("Название факультета");
        genderJ.setBounds(otst , PANAL_HEIGHT - BUTTON_HEIGHT - 10, BUTTON_WIDTH , BUTTON_HEIGHT);
        genderJ.setFont(new Font("Times New Roman", NORMAL, 15));
        genderJ.setBackground(Color.gray);
        genderJ.setForeground(Color.DARK_GRAY);
        pan.add(genderJ);
        otst += BUTTON_WIDTH ;
        JLabel bdJ = new JLabel("Семестр");
        bdJ.setBounds(otst, PANAL_HEIGHT - BUTTON_HEIGHT - 10, BUTTON_WIDTH / 3, BUTTON_HEIGHT);
        bdJ.setFont(new Font("Times New Roman", NORMAL, 15));
        bdJ.setBackground(Color.gray);
        bdJ.setForeground(Color.DARK_GRAY);
        pan.add(bdJ);
        otst += BUTTON_WIDTH;
        JLabel childJ = new JLabel("Год");
        childJ.setBounds(otst, PANAL_HEIGHT - BUTTON_HEIGHT - 10, BUTTON_WIDTH / 3, BUTTON_HEIGHT);
        childJ.setFont(new Font("Times New Roman", NORMAL, 15));
        childJ.setBackground(Color.gray);
        childJ.setForeground(Color.DARK_GRAY);
        pan.add(childJ);

        colorC++;
        Color col1 = new Color(250, 250, 250);
        Color col2 = new Color(225, 225, 225);
        if(colorC%2 == 0){
            pan.setBackground(col1);
        }else pan.setBackground(col2 );
        return pan;
    }

    private JPanel createBut() throws SQLException{
        Color color = new Color (240, 240, 240);
        JPanel panel2 = new JPanel();
        panel2.setPreferredSize(new Dimension(PANAL_WIDTH, PANAL_HEIGHT));
        panel2.setLayout(null);
        if (curriculumController.getRole() != 0) {
            JButton addNewB = new JButton("Добавить новый учебный план");
            final AddCurriculum addCurriculum = new AddCurriculum(curriculumController);
            addNewB.setBounds(50, BUTTON_HEIGHT / 2, BUTTON_WIDTH, 30);
            addNewB.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    try {
                        dispose();
                        addCurriculum.visibleAC();
                    } catch (SQLException eds) {
                        eds.printStackTrace();
                    }
                }
            });
            addNewB.setBackground(color);
            panel2.add(addNewB);
        }
        JButton returnB = new JButton("Назад");
        returnB.setBounds(PANAL_WIDTH - BUTTON_WIDTH, BUTTON_HEIGHT/2, 70, 30 );
        returnB.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });
        returnB.setBackground(color);
        panel2.add(returnB);
        return panel2;
    }
}
