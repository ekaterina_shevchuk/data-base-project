package org.example.dataBase.view;

import org.example.dataBase.controller.TeachersController;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;

public class AdditSearchTch2 extends JFrame {
    static int WINDOW_WIDTH = 1500;
    static int WINDOW_HEIGHT = 1000;
    static int PANAL_WIDTH = 1100;
    static int PANAL_HEIGHT = 100;
    static int BUTTON_WIDTH = 350;
    static int BUTTON_HEIGHT = 50;

    private int num_gr, course, semester, year;
    private String occupation, faculty, answ = "";
    private TeachersController teachersController;

    public AdditSearchTch2(final TeachersController teachersController1) throws SQLException {
        teachersController = teachersController1;
        Container c = getContentPane();
        c.setLayout(new BorderLayout());
        setTitle("Расширенный поиск преподавателей");
        setMinimumSize(new Dimension(WINDOW_WIDTH, WINDOW_HEIGHT));
        setResizable(false);

        JPanel panel = new JPanel();
        panel.setBorder(new EmptyBorder(10, 10, 10, 10));
        GridBagLayout gridBagLayout = new GridBagLayout();
        gridBagLayout.rowHeights = new int[]{BUTTON_HEIGHT, BUTTON_HEIGHT, BUTTON_HEIGHT, BUTTON_HEIGHT, BUTTON_HEIGHT, BUTTON_HEIGHT, BUTTON_HEIGHT, BUTTON_HEIGHT, BUTTON_HEIGHT, BUTTON_HEIGHT, BUTTON_HEIGHT};
        gridBagLayout.columnWeights = new double[]{1.0};
        gridBagLayout.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0};
        panel.setLayout(gridBagLayout);
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.weightx = 1;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.gridwidth = GridBagConstraints.REMAINDER;
        c.add(panel);
        JScrollPane jScrollPane = new JScrollPane(panel);
        jScrollPane.setPreferredSize(new Dimension(WINDOW_WIDTH, WINDOW_HEIGHT));

        jScrollPane.setLayout(new ScrollPaneLayout());
        c.add(jScrollPane);
        jScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
        GridBagConstraints layConstraints;

        JLabel facultyLabel = new JLabel("Выберите факультет");
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 0;
        layConstraints.gridy = 1;
        panel.add(facultyLabel,layConstraints);
        ArrayList<String> facultets = teachersController.getFacultets();
        final JComboBox facultyBox = new JComboBox();
        Iterator<String> iter2= facultets.iterator();
        int k = 0, g = facultets.size();
        while(iter2.hasNext()){
            if (k == g) break;
            facultyBox.addItem(facultets.get(k));
            k++;
        }
        facultyBox.setAlignmentX(LEFT_ALIGNMENT);
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 1;
        layConstraints.gridy = 1;
        panel.add(facultyBox, layConstraints);

        JLabel jLabel = new JLabel("Выберите номер группы");
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 0;
        layConstraints.gridy = 3;
        panel.add(jLabel,layConstraints);
        ArrayList<Integer> groups = teachersController.getGroups();
        final JComboBox groupBox = new JComboBox();
        Iterator<Integer> iter = groups.iterator();
        k = 0;
        g = groups.size();
        while(iter.hasNext()){
            if (k == g) break;
            groupBox.addItem(groups.get(k));
            k++;
        }
        groupBox.setAlignmentX(LEFT_ALIGNMENT);
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 1;
        layConstraints.gridy = 3;
        groupBox.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                answ += "!";
            }
        });
        panel.add(groupBox, layConstraints);

        JLabel courseL = new JLabel("Выберите курс");
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 0;
        layConstraints.gridy = 2;
        panel.add(courseL,layConstraints);
        Integer[] courseA = {1,2,3,4,5};
        final JComboBox courseBox = new JComboBox(courseA);
        courseBox.setAlignmentX(LEFT_ALIGNMENT);
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 1;
        layConstraints.gridy = 2;
        panel.add(courseBox, layConstraints);

        JLabel occupationL = new JLabel("Выберите вид занятий");
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 0;
        layConstraints.gridy = 4;
        panel.add(occupationL,layConstraints);
        final ArrayList<String> occupationS = teachersController.getOccupation();
        final JComboBox occupationBox = new JComboBox();
        Iterator<String> iter0 = occupationS.iterator();
        k = 0;
        g = occupationS.size();
        while(iter0.hasNext()){
            if (k == g) break;
            occupationBox.addItem(occupationS.get(k));
            k++;
        }
        occupationBox.setAlignmentX(LEFT_ALIGNMENT);
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 1;
        layConstraints.gridy = 4;
        panel.add(occupationBox, layConstraints);

        JLabel yearTestL = new JLabel("Введите год ");
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 0;
        layConstraints.gridy = 5;
        panel.add(yearTestL,layConstraints);
        final JTextField yearTestField = new JTextField("", 10);
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 1;
        layConstraints.gridy = 5;
        yearTestField.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                year = Integer.parseInt(yearTestField.getText());
                answ += "?";
            }
        });
        panel.add(yearTestField, layConstraints);

        final JLabel semestrL = new JLabel("Выберите семестр");
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 0;
        layConstraints.gridy = 9;
        panel.add(semestrL,layConstraints);
        Integer[] semestrA = {1,2,3,4,5,6,7,8};
        final JComboBox semestrBox = new JComboBox(semestrA);
        semestrBox.setAlignmentX(LEFT_ALIGNMENT);
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 1;
        layConstraints.gridy = 9;
        panel.add(semestrBox, layConstraints);

        JLabel helpLabel = new JLabel("Выберите вид занятий И группу или курс и факультет");
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 0;
        layConstraints.gridy = 0;
        panel.add(helpLabel,layConstraints);

        final JButton searchB = new JButton("Поиск");
        searchB.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                num_gr = (Integer) groupBox.getSelectedItem();
                occupation = (String)occupationBox.getSelectedItem();
                course = (Integer) courseBox.getSelectedItem();
                faculty = (String) facultyBox.getSelectedItem();
                semester = (int) semestrBox.getSelectedItem();
                dispose();
                try{
                    SearchTeachers searchTeachers = new SearchTeachers(teachersController);
                    if ((answ.equals("!?"))||(answ.equals("?!"))){
                        searchTeachers.setAnsw("12 14 15 ");
                        searchTeachers.setQuery6(0, num_gr, "", occupation, 0, year);
                    } else if(answ.equals("!")){
                        searchTeachers.setAnsw("12 14 16 ");
                        searchTeachers.setQuery6(0, num_gr, "", occupation, semester, 0);
                    }
                    else if(answ.equals("?")){
                        searchTeachers.setAnsw("2 13 14 15 ");
                        searchTeachers.setQuery6(course, 0, faculty, occupation, 0, year);
                    }
                    else if(answ.equals("")){
                        searchTeachers.setAnsw("2 13 14 16 ");
                        searchTeachers.setQuery6(course, 0, faculty, occupation, semester, 0);
                    }
                    searchTeachers.callSearch();
                }catch (SQLException eds){
                    ViewMistake viewMistake = new ViewMistake(eds.getMessage());
                    eds.printStackTrace();}
            }
        });
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 4;
        layConstraints.gridy = 0;
        panel.add(searchB,layConstraints);

        Color color = new Color (240, 240, 240);
        JButton returnB = new JButton("Назад");
        final SearchTeachers searchTeachers = new SearchTeachers(teachersController);
        returnB.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    dispose();
                    searchTeachers.visible();
                }catch (SQLException eds){eds.printStackTrace();}
            }
        });
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 4;
        layConstraints.gridy = 1;
        panel.add(returnB, layConstraints);

        panel.setBackground(color);
        pack();
        setPreferredSize(new Dimension(WINDOW_WIDTH, WINDOW_HEIGHT));
        setVisible(true);


    }

}
