package org.example.dataBase.view;

import org.example.dataBase.controller.*;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;

public class ViewMain extends JFrame{
    static int WINDOW_WIDTH = 1350;
    static int WINDOW_HEIGHT = 850;
    static int PANAL_WIDTH = 750;
    static int PANAL_HEIGHT = 150;
    static int BUTTON_WIDTH = 100;
    static int BUTTON_HEIGHT = 40;

    public ViewMain(DataBaseController dataBaseController)  {
        Color color = new Color (240, 240, 240);
        Container c = getContentPane();
        c.setLayout(new BorderLayout());
        setTitle("Информационная система вуза");
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setMinimumSize(new Dimension(WINDOW_WIDTH, WINDOW_HEIGHT));
        //setResizable(false);

        JPanel panel = new JPanel();
        panel.setBorder(new EmptyBorder(10, 10, 10, 10));
        final GridBagLayout gridBagLayout = new GridBagLayout();
        gridBagLayout.rowHeights = new int[]{BUTTON_HEIGHT, BUTTON_HEIGHT,BUTTON_HEIGHT,BUTTON_HEIGHT,BUTTON_HEIGHT,BUTTON_HEIGHT,BUTTON_HEIGHT,BUTTON_HEIGHT,BUTTON_HEIGHT,BUTTON_HEIGHT,BUTTON_HEIGHT,BUTTON_HEIGHT };
        gridBagLayout.columnWeights = new double[]{1.0, 1.0, 1.0};
        gridBagLayout.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0};
        panel.setLayout(gridBagLayout);

        GridBagConstraints gbc = new GridBagConstraints();
        gbc.weightx = 1;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.gridwidth = GridBagConstraints.REMAINDER;
        c.add(panel);
        JScrollPane jScrollPane = new JScrollPane(panel);
        jScrollPane.setPreferredSize(new Dimension(WINDOW_WIDTH, WINDOW_HEIGHT));

        jScrollPane.setLayout(new ScrollPaneLayout());
        c.add(jScrollPane);
        jScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);

        GridBagConstraints layConstraints;
        JButton facultetsButton = new JButton("Факультеты");
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 0;
        layConstraints.gridy = 0;
        final FacultetsController facultetsController = new FacultetsController(dataBaseController.getConn(), dataBaseController.getRole(), dataBaseController.getFaculty());
            facultetsButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    try{
                        //dispose();
                        facultetsController.openView();
                    }catch(SQLException eds){
                        eds.printStackTrace();
                    }
                }
            });
        facultetsButton.setBackground(color);
        panel.add(facultetsButton, layConstraints);
        JButton groupsButton = new JButton("    Группы    ");
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 1;
        layConstraints.gridy = 0;
        final GroupsController groupsController = new GroupsController(dataBaseController.getConn(), dataBaseController.getRole(), dataBaseController.getFaculty());
        groupsButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try{
                    //dispose();
                    groupsController.openView();
                }catch(SQLException eds){
                    eds.printStackTrace();
                }
            }
        });
        groupsButton.setBackground(color);
        panel.add(groupsButton, layConstraints);

        JButton studentsButton = new JButton("     Студенты     ");
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 0;
        layConstraints.gridy = 2;
        final StudentsController studentsController = new StudentsController(dataBaseController.getConn(), dataBaseController.getRole(), dataBaseController.getFaculty());
        studentsButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    studentsController.OpenView();
                }catch (SQLException eds){eds.printStackTrace();}
            }
        });
        studentsButton.setBackground(color);
        panel.add(studentsButton, layConstraints);

        JButton vkrsButton = new JButton("              ВКР             ");
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 0;
        layConstraints.gridy = 6;
        final VkrsController vkrsController = new VkrsController(dataBaseController.getConn(), dataBaseController.getRole(), dataBaseController.getFaculty());
        vkrsButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    vkrsController.openView();
                }catch (SQLException eds){eds.printStackTrace();}
            }
        });
        vkrsButton.setBackground(color);
        panel.add(vkrsButton, layConstraints);

        JButton depB = new JButton("   Кафедры   ");
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 2;
        layConstraints.gridy = 0;
        final DepartmentController departmentController = new DepartmentController(dataBaseController.getConn(), dataBaseController.getRole(), dataBaseController.getFaculty());
        depB.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    departmentController.openView();
                }catch (SQLException eds){eds.printStackTrace();}
            }
        });
        depB.setBackground(color);
        panel.add(depB, layConstraints);

        JButton tchB = new JButton("Преподаватели");
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 1;
        layConstraints.gridy = 2;
        final TeachersController teachersController = new TeachersController(dataBaseController.getConn(), dataBaseController.getRole(), dataBaseController.getFaculty());
        tchB.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    teachersController.OpenView();
                }catch (SQLException eds){eds.printStackTrace();}
            }
        });
        tchB.setBackground(color);
        panel.add(tchB, layConstraints);

        final JButton jobB = new JButton("            Должности            ");
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 0;
        layConstraints.gridy = 4;
        final JobController jobController = new JobController(dataBaseController.getConn(), dataBaseController.getRole(), dataBaseController.getFaculty());
        jobB.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    jobController.openView();
                }catch (SQLException eds){eds.printStackTrace();}
            }
        });
        jobB.setBackground(color);
        panel.add(jobB, layConstraints);

        final JButton swB = new JButton("        Научные работы      ");
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 1;
        layConstraints.gridy = 4;
        final ScientificWorkController scientificWorkController = new ScientificWorkController(dataBaseController.getConn(), dataBaseController.getRole(), dataBaseController.getFaculty());
        swB.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    scientificWorkController.openView();
                }catch (SQLException eds){eds.printStackTrace();}
            }
        });
        swB.setBackground(color);
        panel.add(swB, layConstraints);

        final JButton sdB = new JButton("Степени преподавателей");
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 2;
        layConstraints.gridy = 4;
        final ScientificDegreeTchController scientificDegreeTchController = new ScientificDegreeTchController(dataBaseController.getConn(), dataBaseController.getRole(), dataBaseController.getFaculty());
        sdB.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    scientificDegreeTchController.openView();
                }catch (SQLException eds){
                    ViewMistake viewMistake = new ViewMistake(eds.getMessage());
                    eds.printStackTrace();}
            }
        });
        sdB.setBackground(color);
        panel.add(sdB, layConstraints);

        final JButton subB = new JButton("        Предметы        ");
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 2;
        layConstraints.gridy = 2;
        final SubjectController subjectController = new SubjectController(dataBaseController.getConn(), dataBaseController.getRole(), dataBaseController.getFaculty());
        subB.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    subjectController
                            .openView();
                }catch (SQLException eds){eds.printStackTrace();}
            }
        });
        subB.setBackground(color);
        panel.add(subB, layConstraints);


        final JButton curB = new JButton("     Учебный план     ");
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 1;
        layConstraints.gridy = 6;
        final CurriculumController curriculumController = new CurriculumController(dataBaseController.getConn(), dataBaseController.getRole(), dataBaseController.getFaculty());
        curB.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    curriculumController.OpenView();
                }catch (SQLException eds){eds.printStackTrace();}
            }
        });
        curB.setBackground(color);
        panel.add(curB, layConstraints);

        final JButton trB = new JButton("Учебные поручения");
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 3;
        layConstraints.gridy = 6;
        final TrainingAssignmentController trainingAssignmentController = new TrainingAssignmentController(dataBaseController.getConn(), dataBaseController.getRole(), dataBaseController.getFaculty());
        trB.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    trainingAssignmentController.openView();
                }catch (SQLException eds){eds.printStackTrace();}
            }
        });
        trB.setBackground(color);
        panel.add(trB, layConstraints);

        final JButton testB = new JButton("Экзамены и зачеты");
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 2;
        layConstraints.gridy = 6;
        final TestsController testsController = new TestsController(dataBaseController.getConn(), dataBaseController.getRole(), dataBaseController.getFaculty());
        testB.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    testsController.OpenView();
                }catch (SQLException eds){eds.printStackTrace();}
            }
        });
        testB.setBackground(color);
        panel.add(testB, layConstraints);

        color = new Color (250, 250, 250);
        panel.setBackground(color);
        pack();
        setPreferredSize(new Dimension(WINDOW_WIDTH, WINDOW_HEIGHT));
        setVisible(true);
    }

}
