package org.example.dataBase.view;

import org.example.dataBase.controller.TestsController;
import org.example.dataBase.model.CurriculumEntity;
import org.example.dataBase.model.TestsEntity;
import org.example.dataBase.service.CurriculumService;
import org.example.dataBase.service.TestsService;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;

public class ViewTests extends JFrame {
    static int WINDOW_WIDTH = 1350;
    static int WINDOW_HEIGHT = 750;
    static int PANAL_WIDTH = 1100;
    static int PANAL_HEIGHT = 70;
    static int BUTTON_WIDTH = 350;
    static int BUTTON_HEIGHT = 50;

    TestsController testsController;
    int colorC = 0;

    public ViewTests(ArrayList<TestsEntity> testsEntities, TestsController testsController1) throws SQLException {
        testsController = testsController1;
        Container c = getContentPane();
        c.setLayout(new BorderLayout());
        setTitle("Просмотр экзаменов и зачетов");
        setMinimumSize(new Dimension(WINDOW_WIDTH, WINDOW_HEIGHT));
        setResizable(false);

        JPanel panel = new JPanel();
        panel.setBorder(new EmptyBorder(10, 10, 10, 10));
        GridBagLayout gridBagLayout = new GridBagLayout();
        gridBagLayout.rowHeights = new int[]{BUTTON_HEIGHT, BUTTON_HEIGHT,BUTTON_HEIGHT,BUTTON_HEIGHT,BUTTON_HEIGHT };
        gridBagLayout.columnWeights = new double[]{1.0};
        gridBagLayout.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0};
        panel.setLayout(gridBagLayout);
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.weightx = 1;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.gridwidth = GridBagConstraints.REMAINDER;
        c.add(panel);
        JScrollPane jScrollPane = new JScrollPane(panel);
        jScrollPane.setPreferredSize(new Dimension(WINDOW_WIDTH, WINDOW_HEIGHT));

        jScrollPane.setLayout(new ScrollPaneLayout());
        c.add(jScrollPane);
        jScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
        panel.add(createBut(), gbc);

        panel.add(article(), gbc);

        Iterator<TestsEntity> iter = testsEntities.iterator();
        while(iter.hasNext()){
            panel.add(createNewJPanel(iter.next()), gbc);
        }
        Color color = new Color (240, 240, 240);
        panel.setBackground(color);
        pack();
        setPreferredSize(new Dimension(WINDOW_WIDTH, WINDOW_HEIGHT));
        setVisible(true);
    }

    private JPanel createNewJPanel(TestsEntity model) {
        JPanel pan = new JPanel();
        int otst = 20;
        pan.setPreferredSize(new Dimension(PANAL_WIDTH, PANAL_HEIGHT));
        pan.setLayout(null);

        JLabel fioJ = new JLabel(model.getSubject());
        fioJ.setBounds(otst, PANAL_HEIGHT - BUTTON_HEIGHT - 10, 4*BUTTON_WIDTH/7, BUTTON_HEIGHT);
        fioJ.setFont(new Font("Times New Roman", NORMAL, 15));
        fioJ.setBackground(Color.gray);
        fioJ.setForeground(Color.DARK_GRAY);
        pan.add(fioJ);
        otst += 4*BUTTON_WIDTH/7;

        JLabel kafJ = new JLabel(model.getFIOst());
        kafJ.setBounds(otst, PANAL_HEIGHT - BUTTON_HEIGHT - 10, 5*BUTTON_WIDTH/7 , BUTTON_HEIGHT);
        kafJ.setFont(new Font("Times New Roman", NORMAL, 15));
        kafJ.setBackground(Color.gray);
        kafJ.setForeground(Color.DARK_GRAY);
        pan.add(kafJ);
        otst += 5*BUTTON_WIDTH/7 ;

        /*JLabel catJ = new JLabel(String.valueOf(model.getNum_gr()));
        catJ.setBounds(otst, PANAL_HEIGHT - BUTTON_HEIGHT - 10, BUTTON_WIDTH / 3, BUTTON_HEIGHT);
        catJ.setFont(new Font("Times New Roman", NORMAL, 15));
        catJ.setBackground(Color.gray);
        catJ.setForeground(Color.DARK_GRAY);
        pan.add(catJ);
        otst+=BUTTON_WIDTH / 3;*/

        JLabel genderJ = new JLabel(model.getFIOtch());
        genderJ.setBounds(otst , PANAL_HEIGHT - BUTTON_HEIGHT - 10, 5*BUTTON_WIDTH / 7, BUTTON_HEIGHT);
        genderJ.setFont(new Font("Times New Roman", NORMAL, 15));
        genderJ.setBackground(Color.gray);
        genderJ.setForeground(Color.DARK_GRAY);
        pan.add(genderJ);
        otst += 5*BUTTON_WIDTH / 7;

        JLabel childJ = new JLabel(String.valueOf(model.getMark()));
        childJ.setBounds(otst, PANAL_HEIGHT - BUTTON_HEIGHT - 10, BUTTON_WIDTH / 4, BUTTON_HEIGHT);
        childJ.setFont(new Font("Times New Roman", NORMAL, 15));
        childJ.setBackground(Color.gray);
        childJ.setForeground(Color.DARK_GRAY);
        pan.add(childJ);
        otst += BUTTON_WIDTH / 4;

        JLabel dateJ = new JLabel(String.valueOf(model.getDateTest()));
        dateJ.setBounds(otst, PANAL_HEIGHT - BUTTON_HEIGHT - 10, BUTTON_WIDTH / 4, BUTTON_HEIGHT);
        dateJ.setFont(new Font("Times New Roman", NORMAL, 15));
        dateJ.setBackground(Color.gray);
        dateJ.setForeground(Color.DARK_GRAY);
        pan.add(dateJ);

        colorC++;
        Color col1 = new Color(250, 250, 250);
        Color col2 = new Color(225, 225, 225);
        if(colorC%2 == 0){
            pan.setBackground(col1);
        }else pan.setBackground(col2 );
        return pan;
    }

    private JPanel article(){
        JPanel pan = new JPanel();
        int otst = 20;
        pan.setPreferredSize(new Dimension(PANAL_WIDTH, PANAL_HEIGHT));
        pan.setLayout(null);

        JLabel fioJ = new JLabel("Название предмета");
        fioJ.setBounds(otst, PANAL_HEIGHT - BUTTON_HEIGHT - 10, 4*BUTTON_WIDTH/7, BUTTON_HEIGHT);
        fioJ.setFont(new Font("Times New Roman", NORMAL, 15));
        fioJ.setBackground(Color.gray);
        fioJ.setForeground(Color.DARK_GRAY);
        pan.add(fioJ);
        otst += 4*BUTTON_WIDTH/7;

        JLabel kafJ = new JLabel("ФИО студента");
        kafJ.setBounds(otst, PANAL_HEIGHT - BUTTON_HEIGHT - 10, 5*BUTTON_WIDTH/7 , BUTTON_HEIGHT);
        kafJ.setFont(new Font("Times New Roman", NORMAL, 15));
        kafJ.setBackground(Color.gray);
        kafJ.setForeground(Color.DARK_GRAY);
        pan.add(kafJ);
        otst += 5*BUTTON_WIDTH/7 ;

        /*JLabel catJ = new JLabel("Номер группы");
        catJ.setBounds(otst, PANAL_HEIGHT - BUTTON_HEIGHT - 10, BUTTON_WIDTH / 3, BUTTON_HEIGHT);
        catJ.setFont(new Font("Times New Roman", NORMAL, 15));
        catJ.setBackground(Color.gray);
        catJ.setForeground(Color.DARK_GRAY);
        pan.add(catJ);
        otst+=BUTTON_WIDTH / 3;*/

        JLabel genderJ = new JLabel("ФИО преподавателя");
        genderJ.setBounds(otst , PANAL_HEIGHT - BUTTON_HEIGHT - 10, 5*BUTTON_WIDTH / 7, BUTTON_HEIGHT);
        genderJ.setFont(new Font("Times New Roman", NORMAL, 15));
        genderJ.setBackground(Color.gray);
        genderJ.setForeground(Color.DARK_GRAY);
        pan.add(genderJ);
        otst += 5*BUTTON_WIDTH / 7;

        JLabel childJ = new JLabel("Оценка");
        childJ.setBounds(otst, PANAL_HEIGHT - BUTTON_HEIGHT - 10, BUTTON_WIDTH / 5, BUTTON_HEIGHT);
        childJ.setFont(new Font("Times New Roman", NORMAL, 15));
        childJ.setBackground(Color.gray);
        childJ.setForeground(Color.DARK_GRAY);
        pan.add(childJ);
        otst += BUTTON_WIDTH /4;

        JLabel dateJ = new JLabel("Дата");
        dateJ.setBounds(otst, PANAL_HEIGHT - BUTTON_HEIGHT - 10, BUTTON_WIDTH / 4, BUTTON_HEIGHT);
        dateJ.setFont(new Font("Times New Roman", NORMAL, 15));
        dateJ.setBackground(Color.gray);
        dateJ.setForeground(Color.DARK_GRAY);
        pan.add(dateJ);

        colorC++;
        Color col1 = new Color(250, 250, 250);
        Color col2 = new Color(225, 225, 225);
        if(colorC%2 == 0){
            pan.setBackground(col1);
        }else pan.setBackground(col2 );
        return pan;
    }

    private JPanel createBut() throws SQLException{
        Color color = new Color (240, 240, 240);
        JPanel panel2 = new JPanel();
        panel2.setPreferredSize(new Dimension(PANAL_WIDTH, PANAL_HEIGHT));
        panel2.setLayout(null);
        if (testsController.getRole() != 0) {
            JButton addNewB = new JButton("Добавить новый экзамен/зачет");
            final AddTest addTest = new AddTest(testsController);
            addNewB.setBounds(50, BUTTON_HEIGHT / 2, BUTTON_WIDTH, 30);
            addNewB.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    try {
                        dispose();
                        addTest.visibleAC();
                    } catch (SQLException eds) {
                        eds.printStackTrace();
                    }
                }
            });
            addNewB.setBackground(color);
            panel2.add(addNewB);
        }
        JButton returnB = new JButton("Назад");
        returnB.setBounds(PANAL_WIDTH - BUTTON_WIDTH, BUTTON_HEIGHT/2, 70, 30 );
        returnB.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });
        returnB.setBackground(color);
        panel2.add(returnB);
        return panel2;
    }


}
