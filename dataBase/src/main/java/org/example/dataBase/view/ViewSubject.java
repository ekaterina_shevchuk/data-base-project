package org.example.dataBase.view;

import org.example.dataBase.controller.SubjectController;
import org.example.dataBase.model.ScientificWorkEntity;
import org.example.dataBase.model.SubjectEntity;
import org.example.dataBase.service.ScientificWorkService;
import org.example.dataBase.service.SubjectService;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;

public class ViewSubject extends JFrame {
    final SubjectController subjectController;
    static int WINDOW_WIDTH = 1350;
    static int WINDOW_HEIGHT = 750;
    static int PANAL_WIDTH = 1100;
    static int PANAL_HEIGHT = 70;
    static int BUTTON_WIDTH = 350;
    static int BUTTON_HEIGHT = 50;

    int colorC = 0;

    public ViewSubject(ArrayList<SubjectEntity> subjectEntities, SubjectController subjectController1)throws SQLException {
        subjectController = subjectController1;
        Container c = getContentPane();
        c.setLayout(new BorderLayout());
        setTitle("Просмотр предметов");
        setMinimumSize(new Dimension(WINDOW_WIDTH, WINDOW_HEIGHT));
        setResizable(false);

        JPanel panel = new JPanel();
        panel.setBorder(new EmptyBorder(10, 10, 10, 10));
        GridBagLayout gridBagLayout = new GridBagLayout();
        gridBagLayout.rowHeights = new int[]{BUTTON_HEIGHT, BUTTON_HEIGHT,BUTTON_HEIGHT,BUTTON_HEIGHT,BUTTON_HEIGHT };
        gridBagLayout.columnWeights = new double[]{1.0};
        gridBagLayout.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0};
        panel.setLayout(gridBagLayout);

        GridBagConstraints gbc = new GridBagConstraints();
        gbc.weightx = 1;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.gridwidth = GridBagConstraints.REMAINDER;
        c.add(panel);
        JScrollPane jScrollPane = new JScrollPane(panel);
        jScrollPane.setPreferredSize(new Dimension(WINDOW_WIDTH, WINDOW_HEIGHT));

        jScrollPane.setLayout(new ScrollPaneLayout());
        c.add(jScrollPane);
        jScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
        panel.add(createBut(), gbc);

        panel.add(article(), gbc);
        Iterator<SubjectEntity> iter = subjectEntities.iterator();
        while(iter.hasNext()){
            panel.add(createNewJPanel(iter.next()), gbc);
        }
        Color color = new Color (240, 240, 240);
        panel.setBackground(color);
        pack();
        setPreferredSize(new Dimension(WINDOW_WIDTH, WINDOW_HEIGHT));
        setVisible(true);
    }

    private JPanel createNewJPanel(SubjectEntity model) {
        JPanel pan = new JPanel();
        pan.setPreferredSize(new Dimension(PANAL_WIDTH, PANAL_HEIGHT));
        pan.setLayout(null);
        int otst = 20;

        JLabel themeJ = new JLabel(model.getTitle());
        themeJ.setBounds(otst, PANAL_HEIGHT - BUTTON_HEIGHT - 10, BUTTON_WIDTH , BUTTON_HEIGHT);
        themeJ.setFont(new Font("Times New Roman", NORMAL, 15));
        themeJ.setBackground(Color.gray);
        themeJ.setForeground(Color.DARK_GRAY);
        pan.add(themeJ);

        otst += BUTTON_WIDTH ;
        JLabel formJ = new JLabel(model.getFormOfControl());
        formJ.setBounds(otst, PANAL_HEIGHT - BUTTON_HEIGHT - 10, BUTTON_WIDTH/3 , BUTTON_HEIGHT);
        formJ.setFont(new Font("Times New Roman", NORMAL, 15));
        formJ.setBackground(Color.gray);
        formJ.setForeground(Color.DARK_GRAY);
        pan.add(formJ);

        otst += BUTTON_WIDTH / 3;
        JLabel hourJ = new JLabel(String.valueOf(model.getNumberHours()));
        hourJ.setBounds(otst, PANAL_HEIGHT - BUTTON_HEIGHT - 10, BUTTON_WIDTH/2 , BUTTON_HEIGHT);
        hourJ.setFont(new Font("Times New Roman", NORMAL, 15));
        hourJ.setBackground(Color.gray);
        hourJ.setForeground(Color.DARK_GRAY);
        pan.add(hourJ);

        otst += BUTTON_WIDTH/ 2 ;
        JLabel occupJ = new JLabel(model.getOccupation());
        occupJ.setBounds(otst, PANAL_HEIGHT - BUTTON_HEIGHT - 10, BUTTON_WIDTH/2 , BUTTON_HEIGHT);
        occupJ.setFont(new Font("Times New Roman", NORMAL, 15));
        occupJ.setBackground(Color.gray);
        occupJ.setForeground(Color.DARK_GRAY);
        pan.add(occupJ);

        colorC++;
        Color col1 = new Color(250, 250, 250);
        Color col2 = new Color(225, 225, 225);
        if(colorC%2 == 0){
            pan.setBackground(col1);
        }else pan.setBackground(col2 );
        return pan;
    }

    private JPanel article(){
        JPanel pan = new JPanel();
        pan.setPreferredSize(new Dimension(PANAL_WIDTH, PANAL_HEIGHT));
        pan.setLayout(null);
        int otst = 20;

        JLabel courseJ = new JLabel("Название");
        courseJ.setBounds(otst, PANAL_HEIGHT - BUTTON_HEIGHT - 10, BUTTON_WIDTH , BUTTON_HEIGHT);
        courseJ.setFont(new Font("Times New Roman", NORMAL, 15));
        courseJ.setBackground(Color.gray);
        courseJ.setForeground(Color.DARK_GRAY);
        pan.add(courseJ);

        otst += BUTTON_WIDTH ;
        JLabel formJ = new JLabel("Форма контроля");
        formJ.setBounds(otst, PANAL_HEIGHT - BUTTON_HEIGHT - 10, BUTTON_WIDTH/3 , BUTTON_HEIGHT);
        formJ.setFont(new Font("Times New Roman", NORMAL, 15));
        formJ.setBackground(Color.gray);
        formJ.setForeground(Color.DARK_GRAY);
        pan.add(formJ);

        otst += BUTTON_WIDTH / 3;
        JLabel hourJ = new JLabel("Количество часов");
        hourJ.setBounds(otst, PANAL_HEIGHT - BUTTON_HEIGHT - 10, BUTTON_WIDTH/2 , BUTTON_HEIGHT);
        hourJ.setFont(new Font("Times New Roman", NORMAL, 15));
        hourJ.setBackground(Color.gray);
        hourJ.setForeground(Color.DARK_GRAY);
        pan.add(hourJ);

        otst += BUTTON_WIDTH / 2;
        JLabel occupJ = new JLabel("Вид занятий");
        occupJ.setBounds(otst, PANAL_HEIGHT - BUTTON_HEIGHT - 10, BUTTON_WIDTH/2 , BUTTON_HEIGHT);
        occupJ.setFont(new Font("Times New Roman", NORMAL, 15));
        occupJ.setBackground(Color.gray);
        occupJ.setForeground(Color.DARK_GRAY);
        pan.add(occupJ);

        colorC++;
        Color col2 = new Color(225, 225, 225);
        pan.setBackground(col2);
        return pan;
    }

    private JPanel createBut() throws SQLException{
        Color color = new Color (240, 240, 240);
        JPanel panel2 = new JPanel();
        panel2.setPreferredSize(new Dimension(PANAL_WIDTH, PANAL_HEIGHT));
        panel2.setLayout(null);
        if (subjectController.getRole() != 0) {
            JButton addNewB = new JButton("Добавить предмет");
            final AddSubject addSubject = new AddSubject(subjectController);
            addNewB.setBounds(50, BUTTON_HEIGHT / 2, BUTTON_WIDTH, 30);
            addNewB.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    try {
                        dispose();
                        addSubject.visibleAS();
                    } catch (SQLException eds) {
                        eds.printStackTrace();
                    }
                }
            });
            addNewB.setBackground(color);
            panel2.add(addNewB);
        }
        JButton returnB = new JButton("Назад");
        returnB.setBounds(PANAL_WIDTH - BUTTON_WIDTH, BUTTON_HEIGHT/2, 70, 30 );
        returnB.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });
        returnB.setBackground(color);
        panel2.add(returnB);
        return panel2;
    }
}
