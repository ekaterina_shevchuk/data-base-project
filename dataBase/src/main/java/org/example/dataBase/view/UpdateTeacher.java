package org.example.dataBase.view;

import org.example.dataBase.controller.StudentsController;
import org.example.dataBase.controller.TeachersController;
import org.example.dataBase.model.StudentsEntity;
import org.example.dataBase.model.TeachersEntity;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;

public class UpdateTeacher  extends JFrame {
    static int WINDOW_WIDTH = 1350;
    static int WINDOW_HEIGHT = 750;
    static int PANAL_WIDTH = 1100;
    static int PANAL_HEIGHT = 70;
    static int BUTTON_WIDTH = 350;
    static int BUTTON_HEIGHT = 50;
    static int smallBUTTON_WIDTH = 50;
    static int bigBUTTON_WIDTH = 200;

    private String answ = "";
    private TeachersController teachersController;

    public UpdateTeacher(TeachersController teachersController1)throws SQLException {
        teachersController = teachersController1;
    }

    public void visibleUT() throws SQLException{
        Color color = new Color (240, 240, 240);
        final TeachersEntity teachersEntity = new TeachersEntity();
        Container c = getContentPane();
        c.setLayout(new BorderLayout());
        setTitle("Изменение  информации о преподавателе");
        setMinimumSize(new Dimension(WINDOW_WIDTH, WINDOW_HEIGHT));
        setResizable(false);

        JPanel panel = new JPanel();
        panel.setBorder(new EmptyBorder(10, 10, 10, 10));
        GridBagLayout gridBagLayout = new GridBagLayout();
        gridBagLayout.rowHeights = new int[]{BUTTON_HEIGHT, BUTTON_HEIGHT,BUTTON_HEIGHT,BUTTON_HEIGHT,BUTTON_HEIGHT,BUTTON_HEIGHT,BUTTON_HEIGHT };
        gridBagLayout.columnWeights = new double[]{1.0};
        gridBagLayout.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0};
        panel.setLayout(gridBagLayout);

        GridBagConstraints gbc = new GridBagConstraints();
        gbc.weightx = 1;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.gridwidth = GridBagConstraints.REMAINDER;
        c.add(panel);
        JScrollPane jScrollPane = new JScrollPane(panel);
        jScrollPane.setPreferredSize(new Dimension(WINDOW_WIDTH, WINDOW_HEIGHT));

        jScrollPane.setLayout(new ScrollPaneLayout());
        c.add(jScrollPane);
        jScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);

        GridBagConstraints layConstraints;

        final JTextField fioField, childFiels, ssField;

        JLabel fioLabel = new JLabel("Введите ФИО преподавателя");
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 0;
        layConstraints.gridy = 0;
        panel.add(fioLabel,layConstraints);
        fioField = new JTextField("", 40);
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 1;
        layConstraints.gridy = 0;
        panel.add(fioField, layConstraints);

        JLabel kafLabel = new JLabel("Выберите название кафедры");
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 0;
        layConstraints.gridy = 1;
        panel.add(kafLabel,layConstraints);
        ArrayList<String> departments = teachersController.getDepartments();
        final JComboBox depBox = new JComboBox();
        Iterator<String> iter = departments.iterator();
        int k = 0, g = departments.size();
        while(iter.hasNext()){
            if (k == g) break;
            depBox.addItem(departments.get(k));
            k++;
        }
        depBox.setAlignmentX(LEFT_ALIGNMENT);
        depBox.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                answ += "1 ";
            }
        });
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 1;
        layConstraints.gridy = 1;
        panel.add(depBox, layConstraints);

        JLabel jobLabel = new JLabel("Выберите должность");
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 0;
        layConstraints.gridy = 2;
        panel.add(jobLabel,layConstraints);
        ArrayList<String> jobs = teachersController.getJobs();
        final JComboBox jobBox = new JComboBox();
        iter = departments.iterator();
        k = 0;
        g = departments.size();
        while(iter.hasNext()){
            if (k == g) break;
            jobBox.addItem(jobs.get(k));
            k++;
        }
        jobBox.setAlignmentX(LEFT_ALIGNMENT);
        jobBox.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                answ += "2 ";
                teachersEntity.setName_job((String) jobBox.getSelectedItem());
            }
        });
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 1;
        layConstraints.gridy = 2;
        panel.add(jobBox, layConstraints);

        final JLabel categLabel = new JLabel("Введите категорию преподавателя");
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 0;
        layConstraints.gridy = 3;
        panel.add(categLabel,layConstraints);
        ssField = new JTextField("", 40);
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 1;
        layConstraints.gridy = 3;
        panel.add(ssField, layConstraints);

        JLabel childLabel = new JLabel("Введите количество детей");
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 0;
        layConstraints.gridy = 4;
        panel.add(childLabel,layConstraints);
        childFiels = new JTextField("", 40);
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 1;
        layConstraints.gridy = 4;
        panel.add(childFiels, layConstraints);

        JLabel helpLabel = new JLabel("Вы можете изменить один параметр или все сразу");
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 0;
        layConstraints.gridy = 6;
        panel.add(helpLabel,layConstraints);

        JButton addNewB = new JButton("Изменить");
        addNewB.setBounds(50, WINDOW_HEIGHT-BUTTON_HEIGHT, 170, 70 );
        addNewB.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    if (fioField.getText().equals("")){
                        ViewMistake viewMistake = new ViewMistake("Введите ФИО преподавателя!");
                    }else{
                        teachersEntity.setFIO(fioField.getText());
                    }
                    teachersEntity.setName_job((String) jobBox.getSelectedItem());
                    teachersEntity.setName_kaf((String) depBox.getSelectedItem());
                    if (!(childFiels.getText().equals(""))){
                        answ += "4 ";
                        teachersEntity.setNumChildrens(Integer.parseInt(childFiels.getText()));
                    }else {
                        teachersEntity.setNumChildrens(-1);
                    }
                    if (!(ssField.getText().equals(""))){
                        answ += "3 ";
                        teachersEntity.setCategory((String) ssField.getText());
                    }else {
                        teachersEntity.setCategory("");
                    }
                    callUpdate(teachersEntity.getName_kaf(), teachersEntity.getName_job(), teachersEntity.getNumChildrens(), teachersEntity.getCategory(),  teachersEntity.getFIO());
                    ViewSaveSuccess viewSaveSuccess = new ViewSaveSuccess();
                }catch (SQLException eds){
                    ViewMistake viewMistake = new ViewMistake( "По вашему запросу ничего не найдено. " + eds.getMessage());
                    eds.printStackTrace(); }
            }
        });
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 2;
        layConstraints.gridy = 1;
        addNewB.setBackground(color);
        panel.add(addNewB, layConstraints);

        final JButton resetB = new JButton("Сброс");
        resetB.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                answ = "";
                teachersEntity.setFIO("");
                teachersEntity.setNumChildrens(-1);
                teachersEntity.setName_job("");
                teachersEntity.setName_kaf("");
                teachersEntity.setCategory("");
                fioField.setText("");
                ssField.setText("");
                childFiels.setText("");
            }
        });
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 4;
        layConstraints.gridy = 1;
        resetB.setBackground(color);
        panel.add(resetB,layConstraints);

        JButton returnB = new JButton("Назад");
        returnB.setBounds(WINDOW_WIDTH-smallBUTTON_WIDTH, WINDOW_HEIGHT-BUTTON_HEIGHT, 170, 70 );
        final ArrayList<TeachersEntity> teachersEntities = this.teachersController.getAll();
        returnB.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    dispose();
                    ViewTeachers viewTeachers = new ViewTeachers(teachersEntities, teachersController);
                }catch (SQLException eds){
                    eds.printStackTrace();
                }
            }
        });
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 2;
        layConstraints.gridy = 4;
        returnB.setBackground(color);
        panel.add(returnB, layConstraints);
        color = new Color (240, 240, 240);
        panel.setBackground(color);
        pack();
        setPreferredSize(new Dimension(WINDOW_WIDTH, WINDOW_HEIGHT));
        setVisible(true);
    }

    private void callUpdate(String department, String name_job, int checkCh, String category, String fio)throws SQLException{
        if (answ.equals("1 ")){
            teachersController.updateTch(department, "", -1, "", fio, "1 ");
        }else  if (answ.equals("2 ")){
            teachersController.updateTch("", name_job, -1, "", fio, "2 ");
        }else  if (answ.equals("3 ")){
            teachersController.updateTch("", "", -1, category, fio, "3 ");
        }else  if (answ.equals("4 ")){
            teachersController.updateTch("", "", checkCh, "", fio, "4 ");
        }else {
            teachersController.updateTch(department, name_job, checkCh, category, fio, "1 2 3 4 ");
        }
    }
}
