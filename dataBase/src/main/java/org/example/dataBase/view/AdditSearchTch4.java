package org.example.dataBase.view;

import org.example.dataBase.controller.TeachersController;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;

public class AdditSearchTch4 extends JFrame {
    static int WINDOW_WIDTH = 1350;
    static int WINDOW_HEIGHT = 750;
    static int PANAL_WIDTH = 1100;
    static int PANAL_HEIGHT = 70;
    static int BUTTON_WIDTH = 350;
    static int BUTTON_HEIGHT = 50;

    private String faculty, department, answ = "";
    private TeachersController teachersController;

    public AdditSearchTch4(final TeachersController teachersController1) throws SQLException {
        teachersController = teachersController1;
        Container c = getContentPane();
        c.setLayout(new BorderLayout());
        setTitle("Расширенный поиск преподавателей");
        setMinimumSize(new Dimension(WINDOW_WIDTH, WINDOW_HEIGHT));
        setResizable(false);

        JPanel panel = new JPanel();
        panel.setBorder(new EmptyBorder(10, 10, 10, 10));
        GridBagLayout gridBagLayout = new GridBagLayout();
        gridBagLayout.rowHeights = new int[]{BUTTON_HEIGHT, BUTTON_HEIGHT, BUTTON_HEIGHT, BUTTON_HEIGHT, BUTTON_HEIGHT, BUTTON_HEIGHT, BUTTON_HEIGHT, BUTTON_HEIGHT, BUTTON_HEIGHT, BUTTON_HEIGHT, BUTTON_HEIGHT};
        gridBagLayout.columnWeights = new double[]{1.0};
        gridBagLayout.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0};
        panel.setLayout(gridBagLayout);
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.weightx = 1;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.gridwidth = GridBagConstraints.REMAINDER;
        c.add(panel);
        JScrollPane jScrollPane = new JScrollPane(panel);
        jScrollPane.setPreferredSize(new Dimension(WINDOW_WIDTH, WINDOW_HEIGHT));

        jScrollPane.setLayout(new ScrollPaneLayout());
        c.add(jScrollPane);
        jScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
        GridBagConstraints layConstraints;

        JLabel departmentLabel = new JLabel("Выберите кафедру");
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 0;
        layConstraints.gridy = 0;
        panel.add(departmentLabel,layConstraints);
        ArrayList<String> departments = teachersController.getDepartments();
        final JComboBox departmentBox = new JComboBox();
        Iterator<String> iter1= departments.iterator();
        int k = 0;
        int g = departments.size();
        for (k = 0; k < g; k++){
            //if (k == g) break;
            departmentBox.addItem(departments.get(k));
            //k++;
        }
        departmentBox.setAlignmentX(LEFT_ALIGNMENT);
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 1;
        layConstraints.gridy = 0;
        departmentBox.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                answ += "1";
            }
        });
        panel.add(departmentBox, layConstraints);

        JLabel facultyLabel = new JLabel("Выберите факультет");
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 0;
        layConstraints.gridy = 1;
        panel.add(facultyLabel,layConstraints);
        final ArrayList<String> facultets = teachersController.getFacultets();
        final JComboBox facultyBox = new JComboBox();
        Iterator<String> iter2= facultets.iterator();
        k = 0;
        g = facultets.size();
        while(iter2.hasNext()){
            if (k == g) break;
            facultyBox.addItem(facultets.get(k));
            k++;
        }
        facultyBox.setAlignmentX(LEFT_ALIGNMENT);
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 1;
        layConstraints.gridy = 1;
        panel.add(facultyBox, layConstraints);

        JLabel helpLabel = new JLabel("После окончания ввода нажмите Enter");
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 1;
        layConstraints.gridy = 6;
        panel.add(helpLabel,layConstraints);

        JButton searchB = new JButton("Поиск");
        searchB.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                department = (String)departmentBox.getSelectedItem();
                faculty = (String) facultyBox.getSelectedItem();
                try{
                    SearchTeachers searchTeachers = new SearchTeachers(teachersController);
                    if (answ.equals("1")){
                        searchTeachers.setAnsw("1 ! ");
                        searchTeachers.setQuery12(department, "");
                    } else {
                        searchTeachers.setAnsw("2 ! ");
                        searchTeachers.setQuery12("", faculty);
                    }
                    searchTeachers.callSearch();
                    dispose();
                }catch (SQLException eds){
                    ViewMistake viewMistake = new ViewMistake(eds.getMessage());
                    eds.printStackTrace();}
            }
        });
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 4;
        layConstraints.gridy = 0;
        panel.add(searchB,layConstraints);

        Color color = new Color (240, 240, 240);
        JButton returnB = new JButton("Назад");
        final SearchTeachers searchTeachers = new SearchTeachers(teachersController);
        returnB.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    dispose();
                    searchTeachers.visible();
                }catch (SQLException eds){eds.printStackTrace();}
            }
        });
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 4;
        layConstraints.gridy = 1;
        panel.add(returnB, layConstraints);

        panel.setBackground(color);
        pack();
        setPreferredSize(new Dimension(WINDOW_WIDTH, WINDOW_HEIGHT));
        setVisible(true);

    }
}
