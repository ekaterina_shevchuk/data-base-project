package org.example.dataBase.view;

import org.example.dataBase.controller.SubjectController;
import org.example.dataBase.model.ScientificWorkEntity;
import org.example.dataBase.model.SubjectEntity;
import org.example.dataBase.service.ScientificWorkService;
import org.example.dataBase.service.SubjectService;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Date;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;

public class AddSubject extends JFrame {
    static int WINDOW_WIDTH = 1350;
    static int WINDOW_HEIGHT = 750;
    static int PANAL_WIDTH = 1100;
    static int PANAL_HEIGHT = 70;
    static int BUTTON_WIDTH = 350;
    static int BUTTON_HEIGHT = 50;

    final SubjectController subjectController;

    public AddSubject(SubjectController subjectController1)throws SQLException {
        subjectController = subjectController1;
    }

    public void visibleAS() throws SQLException{
        Color color = new Color (240, 240, 240);
        final SubjectEntity subjectEntity = new SubjectEntity();
        Container c = getContentPane();
        c.setLayout(new BorderLayout());
        setTitle("Добавление нового предмета");
        setMinimumSize(new Dimension(WINDOW_WIDTH, WINDOW_HEIGHT));
        setResizable(false);

        JPanel panel = new JPanel();
        panel.setBorder(new EmptyBorder(10, 10, 10, 10));
        final GridBagLayout gridBagLayout = new GridBagLayout();
        gridBagLayout.rowHeights = new int[]{BUTTON_HEIGHT, BUTTON_HEIGHT,BUTTON_HEIGHT,BUTTON_HEIGHT,BUTTON_HEIGHT };
        gridBagLayout.columnWeights = new double[]{1.0};
        gridBagLayout.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0};
        panel.setLayout(gridBagLayout);

        GridBagConstraints gbc = new GridBagConstraints();
        gbc.weightx = 1;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.gridwidth = GridBagConstraints.REMAINDER;
        c.add(panel);
        JScrollPane jScrollPane = new JScrollPane(panel);
        jScrollPane.setPreferredSize(new Dimension(WINDOW_WIDTH, WINDOW_HEIGHT));

        jScrollPane.setLayout(new ScrollPaneLayout());
        c.add(jScrollPane);
        jScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);

        GridBagConstraints layConstraints;

        final JTextField nameField, hourField;

        JLabel nameLabel = new JLabel("Введите название");
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 0;
        layConstraints.gridy = 0;
        panel.add(nameLabel,layConstraints);
        nameField = new JTextField("", 40);
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 1;
        layConstraints.gridy = 0;
        panel.add(nameField, layConstraints);

        JLabel degLabel = new JLabel("Выберите форму контроля");
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 0;
        layConstraints.gridy = 1;
        panel.add(degLabel,layConstraints);
        ArrayList<String> form = new ArrayList<String>();
        form.add("Диференцированный зачет");
        form.add("Зачет с оценкой");
        form.add("Экзамен");
        final JComboBox formBox = new JComboBox();
        Iterator<String> iter1 = form.iterator();
        int k = 0, g = form.size();
        while(iter1.hasNext()){
            if (k == g) break;
            formBox.addItem(form.get(k));
            k++;
        }
        formBox.setAlignmentX(LEFT_ALIGNMENT);
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 1;
        layConstraints.gridy = 1;
        panel.add(formBox, layConstraints);

        JLabel hourLabel = new JLabel("Введите количество часов");
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 0;
        layConstraints.gridy = 2;
        panel.add(hourLabel,layConstraints);
        hourField = new JTextField("", 40);
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 1;
        layConstraints.gridy = 2;
        panel.add(hourField, layConstraints);

        JLabel ocupLabel = new JLabel("Выберите вид занятий");
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 0;
        layConstraints.gridy = 3;
        panel.add(ocupLabel,layConstraints);
        ArrayList<String> ocupation = new ArrayList<String>();
        ocupation.add("Семинар");
        ocupation.add("Лекция");
        ocupation.add("Лабораторный практикум");
        final JComboBox ocupBox = new JComboBox();
        Iterator<String> iter2 = ocupation.iterator();
        k = 0;
        g = ocupation.size();
        while(iter2.hasNext()){

            if (k == g) break;
            ocupBox.addItem(ocupation.get(k));
            k++;
        }
        ocupBox.setAlignmentX(LEFT_ALIGNMENT);
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 1;
        layConstraints.gridy = 3;
        panel.add(ocupBox, layConstraints);

        JButton addNewB = new JButton("Добавить");
        addNewB.setBounds(50, WINDOW_HEIGHT-BUTTON_HEIGHT, 170, 70 );
        addNewB.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    subjectEntity.setTitle(nameField.getText());
                    subjectEntity.setFormOfControl((String) formBox.getSelectedItem());
                    subjectEntity.setNumberHours(Integer.parseInt(hourField.getText()));
                    subjectEntity.setOccupation((String) ocupBox.getSelectedItem());
                    subjectController.addSubject(subjectEntity);
                    ViewSaveSuccess viewSaveSuccess = new ViewSaveSuccess();
                }catch (SQLException eds){
                    ViewMistake viewMistake = new ViewMistake(eds.getMessage());
                    eds.printStackTrace(); }
            }
        });
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 2;
        layConstraints.gridy = 1;
        addNewB.setBackground(color);
        panel.add(addNewB, layConstraints);

        JButton returnB = new JButton("Назад");
        returnB.setBounds(WINDOW_WIDTH-BUTTON_WIDTH, WINDOW_HEIGHT-BUTTON_HEIGHT, 170, 70 );
        final ArrayList<SubjectEntity> subjectEntities = this.subjectController.getAll();
        returnB.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    dispose();
                    ViewSubject viewSubject = new ViewSubject(subjectEntities, subjectController);
                }catch (SQLException eds){
                    eds.printStackTrace();
                }
            }
        });
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 2;
        layConstraints.gridy = 4;
        returnB.setBackground(color);
        panel.add(returnB, layConstraints);
        color = new Color (240, 240, 240);
        panel.setBackground(color);
        pack();
        setPreferredSize(new Dimension(WINDOW_WIDTH, WINDOW_HEIGHT));
        setVisible(true);
    }
}
