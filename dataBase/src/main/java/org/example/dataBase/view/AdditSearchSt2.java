package org.example.dataBase.view;

import org.example.dataBase.controller.StudentsController;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;

public class AdditSearchSt2 extends JFrame {
    StudentsController studentsController;
    static int WINDOW_WIDTH = 1350;
    static int WINDOW_HEIGHT = 750;
    static int PANAL_WIDTH = 1100;
    static int PANAL_HEIGHT = 70;
    static int BUTTON_WIDTH = 350;
    static int BUTTON_HEIGHT = 50;
    private int num_gr, mark, year, semestr;
    //1 9 10

    public AdditSearchSt2(StudentsController studentsController1) throws SQLException {
        studentsController = studentsController1;

        Container c = getContentPane();
        c.setLayout(new BorderLayout());
        setTitle("Расширенный поиск студентов");
        setMinimumSize(new Dimension(WINDOW_WIDTH, WINDOW_HEIGHT));
        setResizable(false);

        JPanel panel = new JPanel();
        panel.setBorder(new EmptyBorder(10, 10, 10, 10));
        GridBagLayout gridBagLayout = new GridBagLayout();
        gridBagLayout.rowHeights = new int[]{BUTTON_HEIGHT, BUTTON_HEIGHT, BUTTON_HEIGHT, BUTTON_HEIGHT, BUTTON_HEIGHT, BUTTON_HEIGHT, BUTTON_HEIGHT, BUTTON_HEIGHT, BUTTON_HEIGHT, BUTTON_HEIGHT, BUTTON_HEIGHT};
        gridBagLayout.columnWeights = new double[]{1.0};
        gridBagLayout.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0};
        panel.setLayout(gridBagLayout);
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.weightx = 1;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.gridwidth = GridBagConstraints.REMAINDER;
        c.add(panel);
        JScrollPane jScrollPane = new JScrollPane(panel);
        jScrollPane.setPreferredSize(new Dimension(WINDOW_WIDTH, WINDOW_HEIGHT));

        jScrollPane.setLayout(new ScrollPaneLayout());
        c.add(jScrollPane);
        jScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
        GridBagConstraints layConstraints;


        JLabel jLabel = new JLabel("Выберите номер группы");
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 0;
        layConstraints.gridy = 0;
        panel.add(jLabel,layConstraints);
        ArrayList<Integer> groups = studentsController.getByGroups();
        final JComboBox groupBox = new JComboBox();
        Iterator<Integer> iter = groups.iterator();
        int k = 0, g = groups.size();
        while(iter.hasNext()){
            if (k == g) break;
            groupBox.addItem(groups.get(k));
            k++;
        }
        groupBox.setAlignmentX(LEFT_ALIGNMENT);
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 1;
        layConstraints.gridy = 0;
        panel.add(groupBox, layConstraints);

        JLabel markL = new JLabel("Выберите оценку");
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 0;
        layConstraints.gridy = 1;
        panel.add(markL,layConstraints);
        Integer[] markA = {2,3,4,5};
        final JComboBox markBox = new JComboBox(markA);
        markBox.setAlignmentX(LEFT_ALIGNMENT);
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 1;
        layConstraints.gridy = 1;
        panel.add(markBox, layConstraints);

        JLabel yearTestL = new JLabel("Введите год сессии");
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 0;
        layConstraints.gridy = 2;
        panel.add(yearTestL,layConstraints);
        final JTextField yearTestField = new JTextField("", 10);
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 1;
        layConstraints.gridy = 2;
        panel.add(yearTestField, layConstraints);

        JLabel semestrL = new JLabel("Выберите семестр");
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 0;
        layConstraints.gridy = 3;
        panel.add(semestrL,layConstraints);
        Integer[] semestrA = {1,2,3,4,5,6,7,8};
        final JComboBox semestrBox = new JComboBox(semestrA);
        semestrBox.setAlignmentX(LEFT_ALIGNMENT);
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 1;
        layConstraints.gridy = 3;
        panel.add(semestrBox, layConstraints);

        JLabel helpLabel = new JLabel("После окончания ввода нажмите Enter");
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 1;
        layConstraints.gridy = 6;
        panel.add(helpLabel,layConstraints);

        final JButton searchB = new JButton("Поиск");
        searchB.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                num_gr = (Integer) groupBox.getSelectedItem();
                semestr = (Integer) semestrBox.getSelectedItem();
                mark = (Integer) markBox.getSelectedItem();
                year = Integer.parseInt(yearTestField.getText());
                try{
                    SearchStudents searchStudents = new SearchStudents(studentsController);
                    searchStudents.setAnsw("1 9 10 ");
                    searchStudents.setQuery8(num_gr, mark, year,  semestr);
                    searchStudents.callSearch();
                }catch (SQLException eds){eds.printStackTrace();}
            }
        });
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 4;
        layConstraints.gridy = 0;
        panel.add(searchB,layConstraints);

        JButton returnB = new JButton("Назад");
        final SearchStudents searchStudents = new SearchStudents(studentsController);
        returnB.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    dispose();
                    searchStudents.visible();
                }catch (SQLException eds){eds.printStackTrace();}
            }
        });
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 4;
        layConstraints.gridy = 1;
        panel.add(returnB, layConstraints);


        Color color = new Color (240, 240, 240);
        panel.setBackground(color);
        pack();
        setPreferredSize(new Dimension(WINDOW_WIDTH, WINDOW_HEIGHT));
        setVisible(true);

    }
}
