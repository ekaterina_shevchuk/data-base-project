package org.example.dataBase.view;

import org.example.dataBase.controller.DataBaseController;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;

public class ViewDataBaseAutorization extends JFrame {
    static int WINDOW_WIDTH = 800;
    static int WINDOW_HEIGHT = 500;
    static int PANAL_WIDTH = 750;
    static int PANAL_HEIGHT = 150;
    static int BUTTON_WIDTH = 100;
    static int BUTTON_HEIGHT = 40;

    private String user;
    private String password;
    private String dateBase;

    public ViewDataBaseAutorization(){
        Container c = getContentPane();
        c.setLayout(new BorderLayout());
        setTitle("Введите свои логин и пароль для авторизации в установленной на вашем компьютере СУБД");
        setMinimumSize(new Dimension(WINDOW_WIDTH, WINDOW_HEIGHT));
        setResizable(false);
        setDefaultCloseOperation(EXIT_ON_CLOSE);

        JPanel panel = new JPanel();
        panel.setBorder(new EmptyBorder(10, 10, 10, 10));
        GridBagLayout gridBagLayout = new GridBagLayout();
        gridBagLayout.rowHeights = new int[]{BUTTON_HEIGHT, BUTTON_HEIGHT, BUTTON_HEIGHT, BUTTON_HEIGHT, BUTTON_HEIGHT,BUTTON_HEIGHT, BUTTON_HEIGHT, BUTTON_HEIGHT, BUTTON_HEIGHT, BUTTON_HEIGHT, BUTTON_HEIGHT};
        gridBagLayout.columnWeights = new double[]{1.0};
        gridBagLayout.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0};
        panel.setLayout(gridBagLayout);
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.weightx = 1;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.gridwidth = GridBagConstraints.REMAINDER;
        c.add(panel);
        JScrollPane jScrollPane = new JScrollPane(panel);
        jScrollPane.setPreferredSize(new Dimension(WINDOW_WIDTH, WINDOW_HEIGHT));

        jScrollPane.setLayout(new ScrollPaneLayout());
        c.add(jScrollPane);
        jScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
        GridBagConstraints layConstraints;

        Color color = new Color (240, 240, 240);
        final JLabel idL = new JLabel("Введите свой login");
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 0;
        layConstraints.gridy = 3;
        panel.add(idL,layConstraints);

        final JTextField idField = new JTextField("", 10);
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 1;
        layConstraints.gridy = 3;
        panel.add(idField, layConstraints);

        final JLabel passL = new JLabel("Введите свой пароль");
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 0;
        layConstraints.gridy = 4;
        panel.add(passL,layConstraints);
        final JPasswordField passField = new JPasswordField("", 10);
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 1;
        layConstraints.gridy = 4;
        panel.add(passField, layConstraints);

        final JLabel dbL = new JLabel("Введите название базы данных(схемы)");
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 0;
        layConstraints.gridy = 5;
        panel.add(dbL,layConstraints);
        final JTextField dbField = new JTextField("", 10);
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 1;
        layConstraints.gridy = 5;
        panel.add(dbField, layConstraints);


        JButton loginB = new JButton("Войти");
        loginB.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                user = idField.getText();
                password = passField.getText();
                dateBase = dbField.getText();
                try{
                    DataBaseController dataBaseController = new DataBaseController(user, password, dateBase);
                    dispose();
                    dataBaseController.openApp();
                }catch (SQLException eds){
                    ViewMistake viewMistake = new ViewMistake(eds.getMessage());
                    eds.printStackTrace();}
            }
        });
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 4;
        layConstraints.gridy = 0;
        loginB.setBackground(color);
        panel.add(loginB,layConstraints);

        color = new Color (250, 250, 250);
        panel.setBackground(color);
        pack();
        setPreferredSize(new Dimension(WINDOW_WIDTH, WINDOW_HEIGHT));
        setVisible(true);
    }

    public String getUser(){return user;}

    public String getPassword(){return password;}

}
