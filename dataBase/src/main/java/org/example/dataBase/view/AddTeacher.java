package org.example.dataBase.view;

import org.example.dataBase.controller.TeachersController;
import org.example.dataBase.model.StudentsEntity;
import org.example.dataBase.model.TeachersEntity;
import org.example.dataBase.service.StudentsService;
import org.example.dataBase.service.TeachersService;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Date;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;

public class AddTeacher extends JFrame {
    static int WINDOW_WIDTH = 1350;
    static int WINDOW_HEIGHT = 750;
    static int PANAL_WIDTH = 1100;
    static int PANAL_HEIGHT = 70;
    static int BUTTON_WIDTH = 350;
    static int BUTTON_HEIGHT = 50;
    static int smallBUTTON_WIDTH = 50;
    static int bigBUTTON_WIDTH = 200;

    TeachersController teachersController;

    public AddTeacher(TeachersController teachersController1)throws SQLException {
        teachersController = teachersController1;
    }

    public void visibleAT() throws SQLException{
        Color color = new Color (240, 240, 240);
        final TeachersEntity teachersEntity = new TeachersEntity();
        Container c = getContentPane();
        c.setLayout(new BorderLayout());
        setTitle("Добавление нового преподавателя");
        setMinimumSize(new Dimension(WINDOW_WIDTH, WINDOW_HEIGHT));
        setResizable(false);

        JPanel panel = new JPanel();
        panel.setBorder(new EmptyBorder(10, 10, 10, 10));
        GridBagLayout gridBagLayout = new GridBagLayout();
        gridBagLayout.rowHeights = new int[]{BUTTON_HEIGHT, BUTTON_HEIGHT,BUTTON_HEIGHT,BUTTON_HEIGHT,BUTTON_HEIGHT,BUTTON_HEIGHT,BUTTON_HEIGHT };
        gridBagLayout.columnWeights = new double[]{1.0};
        gridBagLayout.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0};
        panel.setLayout(gridBagLayout);

        GridBagConstraints gbc = new GridBagConstraints();
        gbc.weightx = 1;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.gridwidth = GridBagConstraints.REMAINDER;
        c.add(panel);
        JScrollPane jScrollPane = new JScrollPane(panel);
        jScrollPane.setPreferredSize(new Dimension(WINDOW_WIDTH, WINDOW_HEIGHT));

        jScrollPane.setLayout(new ScrollPaneLayout());
        c.add(jScrollPane);
        jScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);

        GridBagConstraints layConstraints;

        final JTextField fioField, genderField, bdField, childFiels, ssField;

        JLabel fioLabel = new JLabel("Введите ФИО преподавателя");
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 0;
        layConstraints.gridy = 0;
        panel.add(fioLabel,layConstraints);
        fioField = new JTextField("", 40);
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 1;
        layConstraints.gridy = 0;
        panel.add(fioField, layConstraints);

        JLabel kafLabel = new JLabel("Выберите название кафедры");
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 0;
        layConstraints.gridy = 1;
        panel.add(kafLabel,layConstraints);
        ArrayList<String> departments = teachersController.getDepartments();
        final JComboBox depBox = new JComboBox();
        Iterator<String> iter = departments.iterator();
        int k = 0, g = departments.size();
        while(iter.hasNext()){
            if (k == g) break;
            depBox.addItem(departments.get(k));
            k++;
        }
        depBox.setAlignmentX(LEFT_ALIGNMENT);
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 1;
        layConstraints.gridy = 1;
        panel.add(depBox, layConstraints);

        JLabel jobLabel = new JLabel("Выберите должность");
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 0;
        layConstraints.gridy = 2;
        panel.add(jobLabel,layConstraints);
        ArrayList<String> jobs = teachersController.getJobs();
        final JComboBox jobBox = new JComboBox();
        iter = departments.iterator();
        k = 0;
        g = departments.size();
        while(iter.hasNext()){
            if (k == g) break;
            jobBox.addItem(jobs.get(k));
            k++;
        }
        jobBox.setAlignmentX(LEFT_ALIGNMENT);
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 1;
        layConstraints.gridy = 2;
        panel.add(jobBox, layConstraints);

        JLabel categLabel = new JLabel("Введите категорию преподавателя");
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 0;
        layConstraints.gridy = 3;
        panel.add(categLabel,layConstraints);
        ssField = new JTextField("", 40);
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 1;
        layConstraints.gridy = 3;
        panel.add(ssField, layConstraints);

        JLabel gendL = new JLabel("Выберите пол");
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 0;
        layConstraints.gridy = 4;
        panel.add(gendL,layConstraints);
        String[] gendS = {"м", "ж"};
        final JComboBox gendBox = new JComboBox(gendS);
        gendBox.setAlignmentX(LEFT_ALIGNMENT);
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 1;
        layConstraints.gridy = 4;
        panel.add(gendBox, layConstraints);

        JLabel bdLabel = new JLabel("Введите дату рождения");
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 0;
        layConstraints.gridy = 5;
        panel.add(bdLabel,layConstraints);
        bdField = new JTextField("", 40);
        bdField.setToolTipText("Формат даты ГГГГ-ММ-ДД");
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 1;
        layConstraints.gridy = 5;
        panel.add(bdField, layConstraints);

        JLabel childLabel = new JLabel("Введите количество детей");
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 0;
        layConstraints.gridy = 6;
        panel.add(childLabel,layConstraints);
        childFiels = new JTextField("", 40);
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 1;
        layConstraints.gridy = 6;
        panel.add(childFiels, layConstraints);

        JButton addNewB = new JButton("Добавить");
        addNewB.setBounds(50, WINDOW_HEIGHT-BUTTON_HEIGHT, 170, 70 );
        addNewB.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    teachersEntity.setFIO(fioField.getText());
                    teachersEntity.setName_kaf((String) depBox.getSelectedItem());
                    teachersEntity.setName_job((String) jobBox.getSelectedItem());
                    teachersEntity.setCategory(ssField.getText());
                    teachersEntity.setGender((String) gendBox.getSelectedItem());
                    teachersEntity.setBirthDate(Date.valueOf(bdField.getText()));
                    teachersEntity.setNumChildrens(Integer.parseInt(childFiels.getText()));
                    teachersController.addTeacher(teachersEntity);
                    ViewSaveSuccess viewSaveSuccess = new ViewSaveSuccess();
                }catch (SQLException eds){
                    ViewMistake viewMistake = new ViewMistake(eds.getMessage());
                    eds.printStackTrace(); }
            }
        });
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 2;
        layConstraints.gridy = 1;
        addNewB.setBackground(color);
        panel.add(addNewB, layConstraints);

        JButton returnB = new JButton("Назад");
        returnB.setBounds(WINDOW_WIDTH-smallBUTTON_WIDTH, WINDOW_HEIGHT-BUTTON_HEIGHT, 170, 70 );
        final ArrayList<TeachersEntity> teachersEntities = this.teachersController.getAll();
        returnB.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    dispose();
                    ViewTeachers viewTeachers = new ViewTeachers(teachersEntities, teachersController);
                }catch (SQLException eds){
                    eds.printStackTrace();
                }
            }
        });
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 2;
        layConstraints.gridy = 4;
        returnB.setBackground(color);
        panel.add(returnB, layConstraints);
        color = new Color (240, 240, 240);
        panel.setBackground(color);
        pack();
        setPreferredSize(new Dimension(WINDOW_WIDTH, WINDOW_HEIGHT));
        setVisible(true);
    }
}
