package org.example.dataBase.view;

import org.example.dataBase.controller.CurriculumController;
import org.example.dataBase.model.CurriculumEntity;
import org.example.dataBase.model.SubjectEntity;
import org.example.dataBase.service.CurriculumService;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;

public class AddCurriculum extends JFrame {
    static int WINDOW_WIDTH = 1350;
    static int WINDOW_HEIGHT = 750;
    static int PANAL_WIDTH = 1100;
    static int PANAL_HEIGHT = 70;
    static int BUTTON_WIDTH = 350;
    static int BUTTON_HEIGHT = 50;
    static int smallBUTTON_WIDTH = 50;
    static int bigBUTTON_WIDTH = 200;
    private int year;
    private ArrayList<String> subjects;
    private CurriculumController curriculumController;

    public AddCurriculum(CurriculumController curriculumController1)throws SQLException {
        curriculumController = curriculumController1;
    }

    public void visibleAC() throws SQLException{
        Color color = new Color (240, 240, 240);
        final CurriculumEntity curriculumEntity = new CurriculumEntity();
        final Container c = getContentPane();
        c.setLayout(new BorderLayout());
        setTitle("Добавление нового учебного плана");
        setMinimumSize(new Dimension(WINDOW_WIDTH, WINDOW_HEIGHT));
        setResizable(false);

        JPanel panel = new JPanel();
        panel.setBorder(new EmptyBorder(10, 10, 10, 10));
        GridBagLayout gridBagLayout = new GridBagLayout();
        gridBagLayout.rowHeights = new int[]{BUTTON_HEIGHT, BUTTON_HEIGHT,BUTTON_HEIGHT,BUTTON_HEIGHT,BUTTON_HEIGHT,BUTTON_HEIGHT,BUTTON_HEIGHT };
        gridBagLayout.columnWeights = new double[]{1.0};
        gridBagLayout.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0};
        panel.setLayout(gridBagLayout);

        GridBagConstraints gbc = new GridBagConstraints();
        gbc.weightx = 1;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.gridwidth = GridBagConstraints.REMAINDER;
        c.add(panel);
        JScrollPane jScrollPane = new JScrollPane(panel);
        jScrollPane.setPreferredSize(new Dimension(WINDOW_WIDTH, WINDOW_HEIGHT));

        jScrollPane.setLayout(new ScrollPaneLayout());
        c.add(jScrollPane);
        jScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);

        GridBagConstraints layConstraints;

        final JTextField  nField;

        JLabel ocupLabel = new JLabel("Выберите вид занятий");
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 0;
        layConstraints.gridy = 0;
        panel.add(ocupLabel,layConstraints);
        ArrayList<String> ocupation = new ArrayList<String>();
        ocupation.add("Семинар");
        ocupation.add("Лекция");
        ocupation.add("Лабораторный практикум");
        final JComboBox ocupBox = new JComboBox();
        Iterator<String> iter2 = ocupation.iterator();
        int k = 0, g = ocupation.size();
        while(iter2.hasNext()){
            if (k == g) break;
            ocupBox.addItem(ocupation.get(k));
            k++;
        }
        ocupBox.setAlignmentX(LEFT_ALIGNMENT);
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 1;
        layConstraints.gridy = 0;
        panel.add(ocupBox, layConstraints);

        JLabel nameL = new JLabel("Выберете предмет");
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 0;
        layConstraints.gridy = 1;
        panel.add(nameL,layConstraints);
        String chooseOcup = (String) ocupBox.getSelectedItem();
        if (chooseOcup.equals("Семинар")){
            subjects = curriculumController.getAllSubjectsSem();
        }
        if (chooseOcup.equals("Лекция")) {
            subjects = curriculumController.getAllSubjectsLect();
        }
        if (chooseOcup.equals("Лабораторный практикум")) {
            subjects = curriculumController.getAllSubjectsLab();
        }
        final JComboBox nameSBox = new JComboBox();
        Iterator<String> iter0 = subjects.iterator();
        k = 0;
        g = subjects.size();
        while(iter0.hasNext()){
            if (k == g) break;
            nameSBox.addItem(subjects.get(k));
            k++;
        }
        nameSBox.setAlignmentX(LEFT_ALIGNMENT);
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 1;
        layConstraints.gridy = 1;
        panel.add(nameSBox, layConstraints);

        JLabel courseL = new JLabel("Выберите курс");
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 0;
        layConstraints.gridy = 2;
        panel.add(courseL,layConstraints);
        Integer[] courseA = {1,2,3,4};
        final JComboBox courseBox = new JComboBox(courseA);
        courseBox.setAlignmentX(LEFT_ALIGNMENT);
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 1;
        layConstraints.gridy = 2;
        panel.add(courseBox, layConstraints);

        JLabel jLabel = new JLabel("Выберите номер группы");
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 0;
        layConstraints.gridy = 3;
        panel.add(jLabel,layConstraints);
        ArrayList<Integer> groups = curriculumController.getAllGroups();
        final JComboBox groupBox = new JComboBox();
        Iterator<Integer> iter = groups.iterator();
        k = 0;
        g = groups.size();
        while(iter.hasNext()){
            if (k == g) break;
            groupBox.addItem(groups.get(k));
            k++;
        }
        groupBox.setAlignmentX(LEFT_ALIGNMENT);
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 1;
        layConstraints.gridy = 3;
        panel.add(groupBox, layConstraints);

        JLabel facLabel = new JLabel("Выберите факультет");
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 0;
        layConstraints.gridy = 4;
        panel.add(facLabel,layConstraints);
        ArrayList<String> facultets = curriculumController.getFaultets();
        final JComboBox facBox = new JComboBox();
        Iterator<String> iter1 = facultets.iterator();
        k = 0;
        g = facultets.size();
        while(iter1.hasNext()){
            if (k == g) break;
            facBox.addItem(facultets.get(k));
            k++;
        }
        facBox.setAlignmentX(LEFT_ALIGNMENT);
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 1;
        layConstraints.gridy = 4;
        panel.add(facBox, layConstraints);

        JLabel semestrL = new JLabel("Выберите семестр");
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 0;
        layConstraints.gridy = 5;
        panel.add(semestrL,layConstraints);
        Integer[] semestrA = {1,2,3,4,5,6,7,8};
        final JComboBox semestrBox = new JComboBox(semestrA);
        semestrBox.setAlignmentX(LEFT_ALIGNMENT);
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 1;
        layConstraints.gridy = 5;
        panel.add(semestrBox, layConstraints);

        JLabel yearL = new JLabel("Введите год");
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 0;
        layConstraints.gridy = 6;
        panel.add(yearL,layConstraints);
        final JTextField yearField = new JTextField("", 10);
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 1;
        layConstraints.gridy = 6;
        yearField.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                year = Integer.parseInt(yearField.getText());
            }
        });
        panel.add(yearField, layConstraints);

        JButton addNewB = new JButton("Добавить");
        addNewB.setBounds(50, WINDOW_HEIGHT-BUTTON_HEIGHT, 170, 70 );
        addNewB.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    curriculumEntity.setTitleSub((String) nameSBox.getSelectedItem());
                    curriculumEntity.setCourse((Integer) courseBox.getSelectedItem());
                    curriculumEntity.setNum_gr((Integer) groupBox.getSelectedItem());
                    curriculumEntity.setName_fa((String) facBox.getSelectedItem());
                    curriculumEntity.setSemester((Integer) semestrBox.getSelectedItem());
                    curriculumEntity.setYear(Integer.parseInt(yearField.getText()));
                    curriculumController.addCurriculum(curriculumEntity);
                    ViewSaveSuccess viewSaveSuccess = new ViewSaveSuccess();
                }catch (SQLException eds){
                    ViewMistake viewMistake = new ViewMistake(eds.getMessage());
                    eds.printStackTrace(); }
            }
        });
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 2;
        layConstraints.gridy = 1;
        addNewB.setBackground(color);
        panel.add(addNewB, layConstraints);

        JButton returnB = new JButton("Назад");
        returnB.setBounds(WINDOW_WIDTH-smallBUTTON_WIDTH, WINDOW_HEIGHT-BUTTON_HEIGHT, 170, 70 );
        final ArrayList<CurriculumEntity> curriculumEntities = this.curriculumController.getAll();
        returnB.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    dispose();
                    ViewCurriculum viewCurriculum = new ViewCurriculum(curriculumEntities, curriculumController);
                }catch (SQLException eds){
                    eds.printStackTrace();
                }
            }
        });
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 2;
        layConstraints.gridy = 4;
        returnB.setBackground(color);
        panel.add(returnB, layConstraints);
        color = new Color (240, 240, 240);
        panel.setBackground(color);
        pack();
        setPreferredSize(new Dimension(WINDOW_WIDTH, WINDOW_HEIGHT));
        setVisible(true);
    }
}
