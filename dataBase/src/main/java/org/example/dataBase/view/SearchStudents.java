package org.example.dataBase.view;

import org.example.dataBase.controller.FacultetsController;
import org.example.dataBase.controller.StudentsController;
import org.example.dataBase.controller.TeachersController;
import org.example.dataBase.service.SubjectService;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;

public class SearchStudents extends JFrame {
    StudentsController studentsController;
    static int WINDOW_WIDTH = 1350;
    static int WINDOW_HEIGHT = 750;
    static int PANAL_WIDTH = 1100;
    static int PANAL_HEIGHT = 70;
    static int BUTTON_WIDTH = 100;
    static int BUTTON_HEIGHT = 75;

    private int num_gr;
    private String gender, FIOTch, subjectT, facultet;
    private int yearBD, course;
    private int age;
    private int checkChild;
    private int scholarShip;
    private int yearTest;
    private int semestr;
    private int mark;

    String answ = "";

    public SearchStudents(StudentsController studentsController1)throws SQLException {
        studentsController = studentsController1;
    }

    public void visible() throws SQLException{
        //ArrayList<StudentsEntity> studentsEntities = studentsService.getAll();
        Container c = getContentPane();
        c.setLayout(new BorderLayout());
        setTitle("Расширенный поиск студентов");
        setMinimumSize(new Dimension(WINDOW_WIDTH, WINDOW_HEIGHT));
        setResizable(false);

        JPanel panel = new JPanel();
        panel.setBorder(new EmptyBorder(10, 10, 10, 10));
        GridBagLayout gridBagLayout = new GridBagLayout();
        gridBagLayout.rowHeights = new int[]{BUTTON_HEIGHT, BUTTON_HEIGHT, BUTTON_HEIGHT, BUTTON_HEIGHT, BUTTON_HEIGHT,BUTTON_HEIGHT, BUTTON_HEIGHT, BUTTON_HEIGHT, BUTTON_HEIGHT, BUTTON_HEIGHT, BUTTON_HEIGHT};
        gridBagLayout.columnWeights = new double[]{1.0, 1.0, 1.0 };
        gridBagLayout.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0};
        panel.setLayout(gridBagLayout);
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.weightx = 1;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.gridwidth = GridBagConstraints.REMAINDER;
        c.add(panel);
        JScrollPane jScrollPane = new JScrollPane(panel);
        jScrollPane.setPreferredSize(new Dimension(WINDOW_WIDTH, WINDOW_HEIGHT));

        jScrollPane.setLayout(new ScrollPaneLayout());
        c.add(jScrollPane);
        jScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
        GridBagConstraints layConstraints;

        JLabel jLabel = new JLabel("Выберите номер группы");
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 0;
        layConstraints.gridy = 0;
        panel.add(jLabel,layConstraints);
        ArrayList<Integer> groups = studentsController.getAllGroups();
        final JComboBox groupBox = new JComboBox();
        Iterator<Integer> iter = groups.iterator();
        int k = 0, g = groups.size();
        while(iter.hasNext()){
            if (k == g) break;
                groupBox.addItem(groups.get(k));
                k++;
        }
        groupBox.setAlignmentX(LEFT_ALIGNMENT);
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 1;
        layConstraints.gridy = 0;
        groupBox.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                answ += "1 ";
            }
        });
        panel.add(groupBox, layConstraints);

        JLabel gendL = new JLabel("Выберите пол");
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 0;
        layConstraints.gridy = 1;
        panel.add(gendL,layConstraints);
        String[] gendS = {"м", "ж"};
        final JComboBox gendBox = new JComboBox(gendS);
        gendBox.setAlignmentX(LEFT_ALIGNMENT);
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 1;
        layConstraints.gridy = 1;
        gendBox.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                answ += "2 ";
            }
        });
        panel.add(gendBox, layConstraints);

        JLabel yearL = new JLabel("Введите год рождения");
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 0;
        layConstraints.gridy = 2;
        panel.add(yearL,layConstraints);
        final JTextField yearBDField = new JTextField("", 10);
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 1;
        layConstraints.gridy = 2;

        yearBDField.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                yearBD = Integer.parseInt(yearBDField.getText());
                answ += "5 ";
            }
        });
        panel.add(yearBDField, layConstraints);

        Color color = new Color (240, 240, 240);
        JLabel ageL = new JLabel("Введите возраст");
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 0;
        layConstraints.gridy = 3;
        panel.add(ageL,layConstraints);
        final JTextField ageField = new JTextField("", 10);
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 1;
        layConstraints.gridy = 3;
        ageField.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                age = Integer.parseInt(ageField.getText());
                answ += "6 ";
            }
        });
        panel.add(ageField, layConstraints);

        JLabel chlL = new JLabel("Введите количество детей");
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 0;
        layConstraints.gridy = 4;
        panel.add(chlL,layConstraints);
        final JTextField chField = new JTextField("", 10);
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 1;
        layConstraints.gridy = 4;
        chField.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                checkChild = Integer.parseInt(chField.getText());
                answ += "7 ";
            }
        });
        panel.add(chField, layConstraints);

        JLabel schL = new JLabel("Введите минимальный размер стипендии");
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 0;
        layConstraints.gridy = 5;
        panel.add(schL,layConstraints);
        final JTextField schField = new JTextField("", 10);
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 1;
        layConstraints.gridy = 5;
        schField.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                scholarShip = Integer.parseInt(schField.getText());
                answ += "8 ";
            }
        });
        panel.add(schField, layConstraints);

        JButton query7B = new JButton("Получить список студентов указанных групп, сдавших зачет либо экзамен по указанной дисциплине с указанной оценкой");
        query7B.setPreferredSize(new Dimension(BUTTON_WIDTH*2, BUTTON_HEIGHT/2));
        query7B.setToolTipText("Получить список студентов указанных групп, сдавших зачет либо экзамен по указанной дисциплине с указанной оценкой");
        query7B.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    dispose();
                    AdditSearchSt1 additSearchSt1 = new AdditSearchSt1(studentsController);
                }catch (SQLException eds){
                    eds.printStackTrace();
                }
            }
        });
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 0;
        layConstraints.gridy = 7;
        panel.add(query7B,layConstraints);

        final JButton query8B = new JButton("Получить список студентов указанных групп, сдавших указанную сессию с указанными оценками");//или указанного курса указанного факультета
        query8B.setPreferredSize(new Dimension(BUTTON_WIDTH*2, BUTTON_HEIGHT/2));
        query8B.setToolTipText("Получить список число студентов указанных групп, сдавших указанную сессию с указанными оценками");
        query8B.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    dispose();
                    AdditSearchSt2 additSearchSt2 = new AdditSearchSt2(studentsController);
                }catch (SQLException eds){
                    eds.printStackTrace();
                }
            }
        });
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 1;
        layConstraints.gridy = 7;
        panel.add(query8B,layConstraints);

        final JButton query9B = new JButton("Получить список студентов указанных групп, которым заданный преподаватель поставил некоторую оценку за экзамен по определенным дисциплинам, в указанных семестрах, за некоторый период");
        query9B.setPreferredSize(new Dimension(BUTTON_WIDTH*2, BUTTON_HEIGHT/2));
        query9B.setToolTipText("Получить список студентов указанных групп, которым заданный преподаватель поставил некоторую оценку за экзамен по определенным дисциплинам, в указанных семестрах, за некоторый период");
        query9B.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    dispose();
                    AdditSearchSt3 additSearchSt3 = new AdditSearchSt3(studentsController);
                }catch (SQLException eds){
                    eds.printStackTrace();
                }
            }
        });
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 0;
        layConstraints.gridy = 8;
        panel.add(query9B,layConstraints);


        JLabel courseL = new JLabel("Выберите курс");
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 0;
        layConstraints.gridy = 6;
        panel.add(courseL,layConstraints);
        Integer[] courseA = {1,2,3,4,5};
        final JComboBox courseBox = new JComboBox(courseA);
        courseBox.setAlignmentX(LEFT_ALIGNMENT);
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 1;
        layConstraints.gridy = 6;
        courseBox.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                answ += "12 ";
            }
        });
        panel.add(courseBox, layConstraints);

        JLabel facultyLabel = new JLabel("Выберите факультет");
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 2;
        layConstraints.gridy = 6;
        panel.add(facultyLabel,layConstraints);
        FacultetsController facultetsController = new FacultetsController(studentsController.getConn(), studentsController.getRole(), studentsController.getFaculty());
        ArrayList<String> facultets = facultetsController.getTitleFacultets();
        final JComboBox facultyBox = new JComboBox();
        Iterator<String> iter5= facultets.iterator();
        k = 0;
        g = facultets.size();
        while(iter5.hasNext()){
            if (k == g) break;
            facultyBox.addItem(facultets.get(k));
            k++;
        }
        facultyBox.setAlignmentX(LEFT_ALIGNMENT);
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 3;
        layConstraints.gridy = 6;
        facultyBox.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                answ += "13 ";
            }
        });
        panel.add(facultyBox, layConstraints);

        JLabel helpLabel = new JLabel("После окончания ввода нажмите Enter");
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 1;
        layConstraints.gridy = 9;
        panel.add(helpLabel,layConstraints);

        final JButton searchB = new JButton("Поиск");
        searchB.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                num_gr = (Integer) groupBox.getSelectedItem();
                gender = (String) gendBox.getSelectedItem();
                //subjectT = (String)subjectBox.getSelectedItem();
                //semestr = (Integer) semestrBox.getSelectedItem();
                //mark = (Integer) markBox.getSelectedItem();
                //FIOTch = (String) teachersBox.getSelectedItem();
                facultet = (String) facultyBox.getSelectedItem();
                course = (Integer) courseBox.getSelectedItem();
                try{
                callSearch();
                }catch (SQLException eds){eds.printStackTrace();}
            }
        });
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 4;
        layConstraints.gridy = 0;
        panel.add(searchB,layConstraints);

        final JButton resetB = new JButton("Сброс");
        resetB.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                resetData();
                schField.setText("");
                yearBDField.setText("");
                ageField.setText("");
                chField.setText("");
                //yearTestField.setText("");
            }
        });
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 4;
        layConstraints.gridy = 1;
        panel.add(resetB,layConstraints);

        panel.setBackground(color);
        pack();
        setPreferredSize(new Dimension(WINDOW_WIDTH, WINDOW_HEIGHT));
        setVisible(true);
    }


    public void resetData(){
        num_gr = 0;
        gender = "";
        FIOTch = "";
        subjectT = "";
        yearBD = 0;
        age = 0;
        checkChild = 0;
        scholarShip = 0;
        yearTest = 0;
        semestr = 0;
        mark = 0;
        return;
    }

    public void callSearch() throws SQLException{
        if(answ.equals("1 ")){
            dispose();
            ViewStudents viewStudents = new ViewStudents(studentsController.chooseStudents(num_gr, "", 0, 0, -1, -1, 0,""), studentsController);
        }else
        if(answ.equals("2 ")){
            dispose();
            ViewStudents viewStudents = new ViewStudents(studentsController.chooseStudents(0, gender, 0, 0, -1, -1, 0,""), studentsController);
        }else
        if(answ.equals("5 ")){
            dispose();
            ViewStudents viewStudents = new ViewStudents(studentsController.chooseStudents(0, "", yearBD, 0, -1, -1, 0,""), studentsController);
        }else
        if(answ.equals("6 ")){
            dispose();
            ViewStudents viewStudents = new ViewStudents(studentsController.chooseStudents(0, "", 0, age, -1, -1, 0, ""), studentsController);
        }else
        if(answ.equals("7 ")){
            dispose();
            ViewStudents viewStudents = new ViewStudents(studentsController.chooseStudents(0, "", 0, 0, checkChild, -1, 0, ""), studentsController);
        }else
        if(answ.equals("8 ")){
            dispose();
            ViewStudents viewStudents = new ViewStudents(studentsController.chooseStudents(0, "", 0, 0, -1, scholarShip, 0, ""), studentsController);
        }else
        if((answ.equals("12 13 ")) || (answ.equals("13 12 "))){
            dispose();
            ViewStudents viewStudents = new ViewStudents(studentsController.chooseStudents(0, "", 0, 0, -1, -1, course, facultet), studentsController);
        }else
        if(answ.equals("1 11 4 ")){
            dispose();
            ViewStudents viewStudents = new ViewStudents(studentsController.getAddition("", subjectT, 0, 0, mark, num_gr), studentsController);
        }else
        if(answ.equals("1 9 10 ")){
            dispose();
            ViewStudents viewStudents = new ViewStudents(studentsController.getAddition("", "", yearTest, semestr, mark, num_gr), studentsController);
        }else
        if(answ.equals("3 4 9 10 11 ") ){
            dispose();
            ViewStudents viewStudents = new ViewStudents(studentsController.getAddition(FIOTch, subjectT, yearTest, semestr, mark, num_gr), studentsController);
        }else{
        ViewMistake viewMistake = new ViewMistake("По вашему запросу ничего не найдено, пожалуйста измените условия");}
        return;
    }

    public void setQuery7(int num_gr1, int mark1, String subject1){
        this.num_gr = num_gr1;
        this.mark = mark1;
        this.subjectT = subject1;
    }

    public void setQuery8(int num_gr1, int mark1, int year1, int semestr1){
        this.num_gr = num_gr1;
        this.mark = mark1;
        this.semestr = semestr1;
        this.yearTest = year1;
    }

    public void setQuery9(int num_gr1, int mark1, int year1, int semestr1, String FIOTch1, String subjectT1){
        this.num_gr = num_gr1;
        this.mark = mark1;
        this.semestr = semestr1;
        this.yearTest = year1;
        this.FIOTch = FIOTch1;
        this.subjectT = subjectT1;
    }

    public void setAnsw(String answ) {
        this.answ = answ;
    }
}
