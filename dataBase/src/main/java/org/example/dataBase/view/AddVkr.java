package org.example.dataBase.view;

import org.example.dataBase.controller.VkrsController;
import org.example.dataBase.model.StudentsEntity;
import org.example.dataBase.model.VkrsEntity;
import org.example.dataBase.service.StudentsService;
import org.example.dataBase.service.TeachersService;
import org.example.dataBase.service.VkrsService;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Date;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;

public class AddVkr extends JFrame {
    static int WINDOW_WIDTH = 1350;
    static int WINDOW_HEIGHT = 750;
    static int PANAL_WIDTH = 1100;
    static int PANAL_HEIGHT = 70;
    static int BUTTON_WIDTH = 350;
    static int BUTTON_HEIGHT = 50;
    static int smallBUTTON_WIDTH = 50;
    static int bigBUTTON_WIDTH = 200;

    String theme;

    VkrsController vkrsController;

    public AddVkr(VkrsController vkrsController1)throws SQLException {
        vkrsController = vkrsController1;
    }

    public void visibleAV() throws SQLException{
        Color color = new Color (240, 240, 240);
        final VkrsEntity vkrsEntity = new VkrsEntity();
        Container c = getContentPane();
        c.setLayout(new BorderLayout());
        setTitle("Добавление новой ВКР");
        setMinimumSize(new Dimension(WINDOW_WIDTH, WINDOW_HEIGHT));
        setResizable(false);

        JPanel panel = new JPanel();
        panel.setBorder(new EmptyBorder(10, 10, 10, 10));
        GridBagLayout gridBagLayout = new GridBagLayout();
        gridBagLayout.rowHeights = new int[]{BUTTON_HEIGHT, BUTTON_HEIGHT,BUTTON_HEIGHT,BUTTON_HEIGHT,BUTTON_HEIGHT,BUTTON_HEIGHT,BUTTON_HEIGHT };
        gridBagLayout.columnWeights = new double[]{1.0};
        gridBagLayout.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0};
        panel.setLayout(gridBagLayout);

        GridBagConstraints gbc = new GridBagConstraints();
        gbc.weightx = 1;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.gridwidth = GridBagConstraints.REMAINDER;
        c.add(panel);
        JScrollPane jScrollPane = new JScrollPane(panel);
        jScrollPane.setPreferredSize(new Dimension(WINDOW_WIDTH, WINDOW_HEIGHT));

        jScrollPane.setLayout(new ScrollPaneLayout());
        c.add(jScrollPane);
        jScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);

        GridBagConstraints layConstraints;

        JLabel stLabel = new JLabel("Выберите студента");
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 0;
        layConstraints.gridy = 1;
        panel.add(stLabel,layConstraints);
        ArrayList<String> stFIO = vkrsController.getFIOst();
        final JComboBox stBox = new JComboBox();
        Iterator<String> iter1 = stFIO.iterator();
        int k = 0;
        int g = stFIO.size();
        while(iter1.hasNext()){
            if (k == g) break;
            stBox.addItem(stFIO.get(k));
            k++;
        }
        stBox.setAlignmentX(LEFT_ALIGNMENT);
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 1;
        layConstraints.gridy = 1;
        panel.add(stBox, layConstraints);

        JLabel themeL = new JLabel("Введите тему");
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 0;
        layConstraints.gridy = 2;
        panel.add(themeL,layConstraints);
        final JTextField themeField = new JTextField("", 40);
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 1;
        layConstraints.gridy = 2;

        themeField.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                theme = themeField.getText();
            }
        });
        panel.add(themeField, layConstraints);

        JLabel tchLabel = new JLabel("Выберите преподавателя");
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 0;
        layConstraints.gridy = 3;
        panel.add(tchLabel,layConstraints);
        ArrayList<String> teachersFIO = vkrsController.getFIOtch();
        final JComboBox teachersBox = new JComboBox();
        Iterator<String> iter3 = teachersFIO.iterator();
        k = 0;
        g = teachersFIO.size();
        while(iter3.hasNext()){
            if (k == g) break;
            teachersBox.addItem(teachersFIO.get(k));
            k++;
        }
        teachersBox.setAlignmentX(LEFT_ALIGNMENT);
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 1;
        layConstraints.gridy = 3;
        panel.add(teachersBox, layConstraints);

        JButton addNewB = new JButton("Добавить");
        addNewB.setBounds(50, WINDOW_HEIGHT-BUTTON_HEIGHT, 170, 70 );
        addNewB.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (theme.equals("")){
                    ViewMistake viewMistake = new ViewMistake("Введите тему.");
                }else {
                    try {
                        vkrsEntity.setFIOStudent((String) stBox.getSelectedItem());
                        vkrsEntity.setTheme(theme);
                        vkrsEntity.setFIOtch((String) teachersBox.getSelectedItem());
                        vkrsController.addVkr(vkrsEntity);
                        ViewSaveSuccess viewSaveSuccess = new ViewSaveSuccess();
                    }catch (SQLException eds){
                        ViewMistake viewMistake = new ViewMistake(eds.getMessage());
                        eds.printStackTrace(); }
                }
            }
        });
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 2;
        layConstraints.gridy = 1;
        addNewB.setBackground(color);
        panel.add(addNewB, layConstraints);

        JButton returnB = new JButton("Назад");
        returnB.setBounds(WINDOW_WIDTH-smallBUTTON_WIDTH, WINDOW_HEIGHT-BUTTON_HEIGHT, 170, 70 );
        final ArrayList<VkrsEntity> vkrsEntities = this.vkrsController.getAll();
        returnB.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    dispose();
                    ViewVkrs viewVkrs = new ViewVkrs(vkrsEntities, vkrsController);
                }catch (SQLException eds){
                    eds.printStackTrace();
                }
            }
        });
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 2;
        layConstraints.gridy = 4;
        returnB.setBackground(color);
        panel.add(returnB, layConstraints);
        color = new Color (240, 240, 240);
        panel.setBackground(color);
        pack();
        setPreferredSize(new Dimension(WINDOW_WIDTH, WINDOW_HEIGHT));
        setVisible(true);
    }
}
