package org.example.dataBase.view;

import org.example.dataBase.controller.DepartmentController;
import org.example.dataBase.model.DepartmentEntity;
import org.example.dataBase.model.FacultetsEntity;
import org.example.dataBase.service.DepartmentService;
import org.example.dataBase.service.FacultetsService;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;

public class ViewDepartment  extends JFrame {
    DepartmentController departmentController;
    static int WINDOW_WIDTH = 1350;
    static int WINDOW_HEIGHT = 750;
    static int PANAL_WIDTH = 1100;
    static int PANAL_HEIGHT = 70;
    static int BUTTON_WIDTH = 350;
    static int BUTTON_HEIGHT = 50;

    int colorC = 0;

    public ViewDepartment(ArrayList<DepartmentEntity> departmentEntities, DepartmentController departmentController1)throws SQLException {
        departmentController = departmentController1;
        Container c = getContentPane();
        c.setLayout(new BorderLayout());
        setTitle("Просмотр кафедр");
        setMinimumSize(new Dimension(WINDOW_WIDTH, WINDOW_HEIGHT));
        setResizable(false);

        JPanel panel = new JPanel();
        panel.setBorder(new EmptyBorder(10, 10, 10, 10));
        GridBagLayout gridBagLayout = new GridBagLayout();
        gridBagLayout.rowHeights = new int[]{BUTTON_HEIGHT, BUTTON_HEIGHT,BUTTON_HEIGHT,BUTTON_HEIGHT,BUTTON_HEIGHT };
        gridBagLayout.columnWeights = new double[]{1.0};
        gridBagLayout.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0};
        panel.setLayout(gridBagLayout);

        GridBagConstraints gbc = new GridBagConstraints();
        gbc.weightx = 1;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.gridwidth = GridBagConstraints.REMAINDER;
        c.add(panel);
        JScrollPane jScrollPane = new JScrollPane(panel);
        jScrollPane.setPreferredSize(new Dimension(WINDOW_WIDTH, WINDOW_HEIGHT));

        jScrollPane.setLayout(new ScrollPaneLayout());
        c.add(jScrollPane);
        jScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
        panel.add(createBut(), gbc);

        panel.add(article(), gbc);
        Iterator<DepartmentEntity> iter = departmentEntities.iterator();
        while(iter.hasNext()){
            panel.add(createNewJPanel(iter.next()), gbc);
        }
        Color color = new Color (240, 240, 240);
        panel.setBackground(color);
        pack();
        setPreferredSize(new Dimension(WINDOW_WIDTH, WINDOW_HEIGHT));
        setVisible(true);
    }

    private JPanel createNewJPanel(DepartmentEntity model) {
        JPanel pan = new JPanel();
        pan.setPreferredSize(new Dimension(PANAL_WIDTH, PANAL_HEIGHT));
        pan.setLayout(null);

        JLabel nameJ = new JLabel(model.getTitle());
        nameJ.setBounds(230, PANAL_HEIGHT - BUTTON_HEIGHT - 10, BUTTON_WIDTH , BUTTON_HEIGHT);
        nameJ.setFont(new Font("Times New Roman", NORMAL, 15));
        nameJ.setBackground(Color.gray);
        nameJ.setForeground(Color.DARK_GRAY);
        pan.add(nameJ);

        JLabel deanJ = new JLabel(model.getName_fa());
        deanJ.setBounds(230 + BUTTON_WIDTH , PANAL_HEIGHT - BUTTON_HEIGHT - 10, BUTTON_WIDTH , BUTTON_HEIGHT);
        deanJ.setFont(new Font("Times New Roman", NORMAL, 15));
        deanJ.setBackground(Color.gray);
        deanJ.setForeground(Color.DARK_GRAY);
        pan.add(deanJ);
        colorC++;

        Color col1 = new Color(250, 250, 250);
        Color col2 = new Color(225, 225, 225);
        if(colorC%2 == 0){
            pan.setBackground(col1);
        }else pan.setBackground(col2 );
        return pan;
    }

    private JPanel article(){
        JPanel pan = new JPanel();
        pan.setPreferredSize(new Dimension(PANAL_WIDTH, PANAL_HEIGHT));
        pan.setLayout(null);

        JLabel nameJ = new JLabel("Название");
        nameJ.setBounds(230, PANAL_HEIGHT - BUTTON_HEIGHT - 10, BUTTON_WIDTH , BUTTON_HEIGHT);
        nameJ.setFont(new Font("Times New Roman", NORMAL, 15));
        nameJ.setBackground(Color.gray);
        nameJ.setForeground(Color.DARK_GRAY);
        pan.add(nameJ);

        JLabel deanJ = new JLabel("Название факультета");
        deanJ.setBounds(230 + BUTTON_WIDTH, PANAL_HEIGHT - BUTTON_HEIGHT - 10, BUTTON_WIDTH, BUTTON_HEIGHT);
        deanJ.setFont(new Font("Times New Roman", NORMAL, 15));
        deanJ.setBackground(Color.gray);
        deanJ.setForeground(Color.DARK_GRAY);
        pan.add(deanJ);
        colorC++;
        Color col2 = new Color(225, 225, 225);
        pan.setBackground(col2 );

        return pan;
    }

    private JPanel createBut() throws SQLException{
        Color color = new Color (240, 240, 240);
        JPanel panel2 = new JPanel();
        panel2.setPreferredSize(new Dimension(PANAL_WIDTH, PANAL_HEIGHT));
        panel2.setLayout(null);
        if (departmentController.getRole() != 0) {
            JButton addNewB = new JButton("Добавить новую кафедру");
            final AddDepartment addDepartment = new AddDepartment(departmentController);
            addNewB.setBounds(50, BUTTON_HEIGHT / 2, 4 * BUTTON_WIDTH / 5, 30);
            addNewB.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    try {
                        dispose();
                        addDepartment.visibleAD();
                    } catch (SQLException eds) {
                        eds.printStackTrace();
                    }
                }
            });
            addNewB.setBackground(color);
            panel2.add(addNewB);
        }
        JButton returnB = new JButton("Назад");
        returnB.setBounds(PANAL_WIDTH - BUTTON_WIDTH/2, BUTTON_HEIGHT/2, 70, 30 );
        returnB.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });
        returnB.setBackground(color);
        panel2.add(returnB);

        JButton filtB = new JButton("Расширенный поиск кафедр");
        final SearchDepartment searchDepartment = new SearchDepartment(departmentController);
        filtB.setBounds(60+BUTTON_WIDTH, BUTTON_HEIGHT/2, 4*BUTTON_WIDTH/5, 30 );
        filtB.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    dispose();
                    searchDepartment.visible();
                }catch (SQLException eds){
                    eds.printStackTrace(); }
            }
        });
        filtB.setBackground(color);
        panel2.add(filtB);
        return panel2;
    }

}
