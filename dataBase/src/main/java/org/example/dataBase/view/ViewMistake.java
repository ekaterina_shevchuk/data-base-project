package org.example.dataBase.view;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;

public class ViewMistake extends JFrame {

    public ViewMistake(String textMistake){
        Container c = getContentPane();
        c.setLayout(new BorderLayout());
        setTitle("Ошибка");
        setMinimumSize(new Dimension(550, 115));
        setResizable(false);

        JPanel panel = new JPanel();
        panel.setBorder(new EmptyBorder(10, 10, 10, 10));
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.weightx = 1;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.gridwidth = GridBagConstraints.REMAINDER;
        c.add(panel);
        JLabel markL = new JLabel(textMistake);
        panel.add(markL);
        setVisible(true);
    }
}
