package org.example.dataBase.view;

import org.example.dataBase.controller.TeachersController;
import org.example.dataBase.model.ScientificWorkEntity;
import org.example.dataBase.model.TeachersEntity;
import org.example.dataBase.model.TrainingAssignmentEntity;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Date;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;

public class SearchTeachersLoad extends JFrame {
    TeachersController teachersController;
    static int WINDOW_WIDTH = 1350;
    static int WINDOW_HEIGHT = 750;
    static int PANAL_WIDTH = 1100;
    static int PANAL_HEIGHT = 70;
    static int BUTTON_WIDTH = 350;
    static int BUTTON_HEIGHT = 50;

    private String fioTch, department;

    private String answ = "";

    public SearchTeachersLoad(TeachersController teachersController1)throws SQLException {
        teachersController = teachersController1;
    }

    public void visible() throws SQLException {
        Color color = new Color (240, 240, 240);
        Container c = getContentPane();
        c.setLayout(new BorderLayout());
        setTitle("Нагрузка преподавателей");
        setMinimumSize(new Dimension(WINDOW_WIDTH, WINDOW_HEIGHT));
        setResizable(false);

        JPanel panel = new JPanel();
        panel.setBorder(new EmptyBorder(10, 10, 10, 10));
        GridBagLayout gridBagLayout = new GridBagLayout();
        gridBagLayout.rowHeights = new int[]{BUTTON_HEIGHT, BUTTON_HEIGHT, BUTTON_HEIGHT, BUTTON_HEIGHT, BUTTON_HEIGHT, BUTTON_HEIGHT, BUTTON_HEIGHT, BUTTON_HEIGHT, BUTTON_HEIGHT, BUTTON_HEIGHT, BUTTON_HEIGHT};
        gridBagLayout.columnWeights = new double[]{1.0};
        gridBagLayout.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0};
        panel.setLayout(gridBagLayout);
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.weightx = 1;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.gridwidth = GridBagConstraints.REMAINDER;
        c.add(panel);
        JScrollPane jScrollPane = new JScrollPane(panel);
        jScrollPane.setPreferredSize(new Dimension(WINDOW_WIDTH, WINDOW_HEIGHT));

        jScrollPane.setLayout(new ScrollPaneLayout());
        c.add(jScrollPane);
        jScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
        GridBagConstraints layConstraints;

        JLabel departmentLabel = new JLabel("Выберите кафедру");
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 0;
        layConstraints.gridy = 0;
        panel.add(departmentLabel, layConstraints);
        ArrayList<String> departments = teachersController.getDepartments();
        final JComboBox departmentBox = new JComboBox();
        Iterator<String> iter1 = departments.iterator();
        int k = 0;
        int g = departments.size();
        for (k = 0; k < g; k++) {
            //if (k == g) break;
            departmentBox.addItem(departments.get(k));
            //k++;
        }
        departmentBox.setAlignmentX(LEFT_ALIGNMENT);
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 1;
        layConstraints.gridy = 0;
        departmentBox.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                answ += "1 ";
            }
        });
        panel.add(departmentBox, layConstraints);

        JLabel facultyLabel = new JLabel("Выберите преподавателя");
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 0;
        layConstraints.gridy = 1;
        panel.add(facultyLabel, layConstraints);
        ArrayList<String> teachers = teachersController.getFIOtch();
        final JComboBox teacherBox = new JComboBox();
        Iterator<String> iter2 = teachers.iterator();
        k = 0;
        g = teachers.size();
        while (iter2.hasNext()) {
            if (k == g) break;
            teacherBox.addItem(teachers.get(k));
            k++;
        }
        teacherBox.setAlignmentX(LEFT_ALIGNMENT);
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 1;
        layConstraints.gridy = 1;
        teacherBox.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                answ += "2 ";
            }
        });
        panel.add(teacherBox, layConstraints);

        final JButton searchB = new JButton("Поиск");
        searchB.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                department = (String) departmentBox.getSelectedItem();
                fioTch = (String) teacherBox.getSelectedItem();
                try{
                    callSearch();
                }catch (SQLException eds){eds.printStackTrace();}
            }
        });
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 4;
        layConstraints.gridy = 0;
        panel.add(searchB,layConstraints);

        final JButton resetB = new JButton("Сброс");
        resetB.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                resetData();
            }
        });
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 4;
        layConstraints.gridy = 1;
        panel.add(resetB,layConstraints);

        JButton returnB = new JButton("Назад");
        final SearchTeachers searchTeachers = new SearchTeachers(teachersController);
        returnB.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    dispose();
                    searchTeachers.visible();
                }catch (SQLException eds){eds.printStackTrace();}
            }
        });
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 4;
        layConstraints.gridy = 3;
        panel.add(returnB,layConstraints);

        panel.setBackground(color);
        pack();
        setPreferredSize(new Dimension(WINDOW_WIDTH, WINDOW_HEIGHT));
        setVisible(true);
    }

    private void resetData(){
        department = "";
        fioTch = "";
        answ = "";
        return;
    }

    private void callSearch() throws SQLException{
        if(answ.equals("1 ")){
            dispose();
            ArrayList<TrainingAssignmentEntity> trainingAssignmentEntities = teachersController.getTeachersLoad(department, "");
            ArrayList<Integer> numHours = teachersController.getTeachersLoadNumHours(department, "");
            int sumNumHours = teachersController.getTeachersLoadSumNumHours(department, "");
            ViewTeachersLoad viewTeachersLoad = new ViewTeachersLoad(trainingAssignmentEntities, numHours, sumNumHours, teachersController);
            return;
        }else
        if(answ.equals("2 ")){
            dispose();
            ArrayList<TrainingAssignmentEntity> trainingAssignmentEntities = teachersController.getTeachersLoad("", fioTch);
            ArrayList<Integer> numHours = teachersController.getTeachersLoadNumHours("", fioTch);
            int sumNumHours = teachersController.getTeachersLoadSumNumHours("", fioTch);
            ViewTeachersLoad viewTeachersLoad = new ViewTeachersLoad(trainingAssignmentEntities, numHours, sumNumHours, teachersController);
            return;
        }else {
            ViewMistake viewMistake = new ViewMistake("По вашему запросу ничего не найдено, пожалуйста измените условия");}
        return;
    }
}
