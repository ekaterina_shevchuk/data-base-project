package org.example.dataBase.view;

import org.example.dataBase.controller.DepartmentController;
import org.example.dataBase.controller.StudentsController;
import org.example.dataBase.service.*;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;

public class SearchDepartment extends JFrame {
    DepartmentController departmentController;
    static int WINDOW_WIDTH = 1350;
    static int WINDOW_HEIGHT = 750;
    static int PANAL_WIDTH = 1100;
    static int PANAL_HEIGHT = 70;
    static int BUTTON_WIDTH = 100;
    static int BUTTON_HEIGHT = 75;
    private int num_gr;
    private String facultet;
    private int course;
    private int yearTest;
    private int semestr;

    String answ = "";

    public SearchDepartment(DepartmentController departmentController1){
        this.departmentController = departmentController1;
    }

    public void visible() throws SQLException {
        Container c = getContentPane();
        c.setLayout(new BorderLayout());
        setTitle("Расширенный поиск кафедр");
        setMinimumSize(new Dimension(WINDOW_WIDTH, WINDOW_HEIGHT));
        setResizable(false);

        JPanel panel = new JPanel();
        panel.setBorder(new EmptyBorder(10, 10, 10, 10));
        GridBagLayout gridBagLayout = new GridBagLayout();
        gridBagLayout.rowHeights = new int[]{BUTTON_HEIGHT, BUTTON_HEIGHT,  BUTTON_HEIGHT, BUTTON_HEIGHT, BUTTON_HEIGHT};
        gridBagLayout.columnWeights = new double[]{1.0};
        gridBagLayout.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0};
        panel.setLayout(gridBagLayout);
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.weightx = 1;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.gridwidth = GridBagConstraints.REMAINDER;
        c.add(panel);
        JScrollPane jScrollPane = new JScrollPane(panel);
        jScrollPane.setPreferredSize(new Dimension(WINDOW_WIDTH, WINDOW_HEIGHT));

        jScrollPane.setLayout(new ScrollPaneLayout());
        c.add(jScrollPane);
        jScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
        GridBagConstraints layConstraints;

        JLabel jLabel = new JLabel("Выберите номер группы");
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 0;
        layConstraints.gridy = 0;
        panel.add(jLabel,layConstraints);

        ArrayList<Integer> groups = departmentController.getByGroups();
        final JComboBox groupBox = new JComboBox();
        Iterator<Integer> iter = groups.iterator();
        int k = 0, g = groups.size();
        while(iter.hasNext()){
            if (k == g) break;
            groupBox.addItem(groups.get(k));
            k++;
        }
        groupBox.setAlignmentX(LEFT_ALIGNMENT);
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 1;
        layConstraints.gridy = 0;
        groupBox.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                answ += "1 ";
            }
        });
        panel.add(groupBox, layConstraints);

        JLabel yearTestL = new JLabel("Введите год");
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 0;
        layConstraints.gridy = 1;
        panel.add(yearTestL,layConstraints);
        final JTextField yearTestField = new JTextField("", 10);
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 1;
        layConstraints.gridy = 1;
        yearTestField.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                yearTest = Integer.parseInt(yearTestField.getText());
                answ += "2 ";
            }
        });
        panel.add(yearTestField, layConstraints);

        JLabel semestrL = new JLabel("Выберите семестр");
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 0;
        layConstraints.gridy = 2;
        panel.add(semestrL,layConstraints);
        Integer[] semestrA = {1,2,3,4,5,6,7,8};
        final JComboBox semestrBox = new JComboBox(semestrA);
        semestrBox.setAlignmentX(LEFT_ALIGNMENT);
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 1;
        layConstraints.gridy = 2;
        semestrBox.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                answ += "3 ";
            }
        });
        panel.add(semestrBox, layConstraints);

        JLabel courseL = new JLabel("Выберите курс");
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 2;
        layConstraints.gridy = 3;
        panel.add(courseL,layConstraints);
        Integer[] courseA = {1,2,3,4,5};
        final JComboBox courseBox = new JComboBox(courseA);
        courseBox.setAlignmentX(LEFT_ALIGNMENT);
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 3;
        layConstraints.gridy = 3;
        courseBox.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                answ += "4 ";
            }
        });
        panel.add(courseBox, layConstraints);

        JLabel facultyLabel = new JLabel("Выберите факультет");
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 0;
        layConstraints.gridy = 3;
        panel.add(facultyLabel,layConstraints);
        FacultetsService facultetsService = new FacultetsService(departmentController.getConn());
        ArrayList<String> facultets = facultetsService.getTitleFacultets();
        final JComboBox facultyBox = new JComboBox();
        Iterator<String> iter5= facultets.iterator();
        k = 0;
        g = facultets.size();
        while(iter5.hasNext()){
            if (k == g) break;
            facultyBox.addItem(facultets.get(k));
            k++;
        }
        facultyBox.setAlignmentX(LEFT_ALIGNMENT);
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 1;
        layConstraints.gridy = 3;
        facultyBox.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                answ += "5 ";
            }
        });
        panel.add(facultyBox, layConstraints);

        JLabel helpLabel = new JLabel("Для получения переченя кафедр выберите 1.группу ИЛИ курс и факультета И 2.семестр ИЛИ год");
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 0;
        layConstraints.gridy = 4;
        panel.add(helpLabel, layConstraints);

        final JButton searchB = new JButton("Поиск");
        searchB.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                num_gr = (Integer) groupBox.getSelectedItem();
                semestr = (Integer) semestrBox.getSelectedItem();
                course = (Integer) courseBox.getSelectedItem();
                facultet = (String) facultyBox.getSelectedItem();
                try{
                    callSearch();
                }catch (SQLException eds){eds.printStackTrace();}
            }
        });
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 3;
        layConstraints.gridy = 0;
        panel.add(searchB,layConstraints);

        final JButton resetB = new JButton("Сброс");
        resetB.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                resetData();
                yearTestField.setText("");
            }
        });
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 3;
        layConstraints.gridy = 1;
        panel.add(resetB,layConstraints);

        Color color = new Color (240, 240, 240);
        panel.setBackground(color);
        pack();
        setPreferredSize(new Dimension(WINDOW_WIDTH, WINDOW_HEIGHT));
        setVisible(true);
    }

    public void resetData(){
        num_gr = 0;
        yearTest = 0;
        semestr = 0;
        facultet = "";
        course = 0;
        return;
    }

    private void callSearch() throws SQLException{
        if(answ.equals("1 3 ") ||(answ.equals("3 1 "))){
            dispose();
            ViewDepartment viewDepartment = new ViewDepartment(departmentController.getDepartmentsByParam(num_gr, "", 0, 0, semestr), departmentController);
        }else
        if(answ.equals("1 2 ") ||(answ.equals("2 1 "))){
            dispose();
            ViewDepartment viewDepartment = new ViewDepartment(departmentController.getDepartmentsByParam(num_gr, "", 0, yearTest, 0), departmentController);
        }else
        if(answ.equals("3 4 5 ") ||(answ.equals("3 5 4 "))||(answ.equals("4 3 5 "))||(answ.equals("4 5 3 "))||(answ.equals("5 3 4 "))||(answ.equals("5 4 3 "))){
            dispose();
            ViewDepartment viewDepartment = new ViewDepartment(departmentController.getDepartmentsByParam(0, facultet, course, 0, semestr), departmentController);
        }else
        if(answ.equals("2 4 5 ") ||(answ.equals("2 5 4 "))||(answ.equals("4 2 5 "))||(answ.equals("4 5 2 "))||(answ.equals("5 2 4 "))||(answ.equals("5 4 2 "))){
            dispose();
            ViewDepartment viewDepartment = new ViewDepartment(departmentController.getDepartmentsByParam(0, facultet, course, yearTest, 0), departmentController);
        }else{
            ViewMistake viewMistake = new ViewMistake("По вашему запросу ничего не найдено, пожалуйста измените условия");
        }
        return;
    }
}
