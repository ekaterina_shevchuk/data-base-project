package org.example.dataBase.view;

import org.example.dataBase.controller.VkrsController;
import org.example.dataBase.model.StudentsEntity;
import org.example.dataBase.service.TeachersService;
import org.example.dataBase.service.VkrsService;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;

public class SearchVkrs extends JFrame {
    VkrsController vkrsController;
    static int WINDOW_WIDTH = 1350;
    static int WINDOW_HEIGHT = 750;
    static int PANAL_WIDTH = 1100;
    static int PANAL_HEIGHT = 70;
    static int BUTTON_WIDTH = 350;
    static int BUTTON_HEIGHT = 50;

    public SearchVkrs(VkrsController vkrsController1){
        this.vkrsController = vkrsController1;
    }

    public void visible() throws SQLException {
        Container c = getContentPane();
        c.setLayout(new BorderLayout());
        setTitle("Расширенный поиск ВКР");
        setMinimumSize(new Dimension(WINDOW_WIDTH, WINDOW_HEIGHT));
        setResizable(false);

        JPanel panel = new JPanel();
        panel.setBorder(new EmptyBorder(10, 10, 10, 10));
        GridBagLayout gridBagLayout = new GridBagLayout();
        gridBagLayout.rowHeights = new int[]{BUTTON_HEIGHT, BUTTON_HEIGHT,  BUTTON_HEIGHT, BUTTON_HEIGHT, BUTTON_HEIGHT};
        gridBagLayout.columnWeights = new double[]{1.0};
        gridBagLayout.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0};
        panel.setLayout(gridBagLayout);
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.weightx = 1;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.gridwidth = GridBagConstraints.REMAINDER;
        c.add(panel);
        JScrollPane jScrollPane = new JScrollPane(panel);
        jScrollPane.setPreferredSize(new Dimension(WINDOW_WIDTH, WINDOW_HEIGHT));

        jScrollPane.setLayout(new ScrollPaneLayout());
        c.add(jScrollPane);
        jScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
        GridBagConstraints layConstraints;

        JLabel tchLabel = new JLabel("Выберите преподавателя");
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 0;
        layConstraints.gridy = 1;
        panel.add(tchLabel,layConstraints);
        ArrayList<String> teachersFIO = vkrsController.getFIOtch();
        final JComboBox teachersBox = new JComboBox();
        Iterator<String> iter3 = teachersFIO.iterator();
        int k = 0;
        int g = teachersFIO.size();
        while(iter3.hasNext()){
            if (k == g) break;
            teachersBox.addItem(teachersFIO.get(k));
            k++;
        }
        teachersBox.setAlignmentX(LEFT_ALIGNMENT);
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 1;
        layConstraints.gridy = 1;
        final ViewSearchVkrs viewVkrs = new ViewSearchVkrs(vkrsController);
        teachersBox.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
                String  FIOTch = (String) teachersBox.getSelectedItem();
                try {
                    viewVkrs.visible(vkrsController.getVKRS(FIOTch, ""));
                }catch (SQLException eds){eds.printStackTrace();}
            }
        });
        panel.add(teachersBox, layConstraints);

        JLabel kafLabel = new JLabel("Выберите кафедру");
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 0;
        layConstraints.gridy = 2;
        panel.add(kafLabel,layConstraints);
        ArrayList<String> departments = vkrsController.getDepartments();
        final JComboBox kafBox = new JComboBox();
        Iterator<String> iter4 = departments.iterator();
        k = 0;
        g = departments.size();
        while(iter4.hasNext()){
            if (k == g) break;
            kafBox.addItem(departments.get(k));
            k++;
        }
        kafBox.setAlignmentX(LEFT_ALIGNMENT);
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 1;
        layConstraints.gridy = 2;
        kafBox.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
                String  kaf = (String) kafBox.getSelectedItem();
                try {
                    viewVkrs.visible(vkrsController.getVKRS("", kaf));
                }catch (SQLException eds){eds.printStackTrace();}
            }
        });
        panel.add(kafBox, layConstraints);

        setVisible(true);
    }
}
