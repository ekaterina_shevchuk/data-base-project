package org.example.dataBase.view;

import org.example.dataBase.controller.VkrsController;
import org.example.dataBase.model.GroupsEntity;
import org.example.dataBase.model.VkrsEntity;
import org.example.dataBase.service.GroupsService;
import org.example.dataBase.service.VkrsService;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;

public class ViewVkrs extends JFrame{
    final VkrsController vkrsController;
    static int WINDOW_WIDTH = 1350;
    static int WINDOW_HEIGHT = 750;
    static int PANAL_WIDTH = 1100;
    static int PANAL_HEIGHT = 70;
    static int BUTTON_WIDTH = 350;
    static int BUTTON_HEIGHT = 50;

    int colorC = 0;

    public ViewVkrs(ArrayList<VkrsEntity> vkrsEntities, VkrsController vkrsController1)throws SQLException {
        Color color = new Color (240, 240, 240);
        vkrsController = vkrsController1;
        Container c = getContentPane();
        c.setLayout(new BorderLayout());
        setTitle("Просмотр ВКР");
        setMinimumSize(new Dimension(WINDOW_WIDTH, WINDOW_HEIGHT));
        setResizable(false);

        JPanel panel = new JPanel();
        panel.setBorder(new EmptyBorder(10, 10, 10, 10));
        GridBagLayout gridBagLayout = new GridBagLayout();
        gridBagLayout.rowHeights = new int[]{BUTTON_HEIGHT, BUTTON_HEIGHT,BUTTON_HEIGHT,BUTTON_HEIGHT,BUTTON_HEIGHT };
        gridBagLayout.columnWeights = new double[]{1.0};
        gridBagLayout.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0};
        panel.setLayout(gridBagLayout);

        GridBagConstraints gbc = new GridBagConstraints();
        gbc.weightx = 1;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.gridwidth = GridBagConstraints.REMAINDER;
        c.add(panel);
        JScrollPane jScrollPane = new JScrollPane(panel);
        jScrollPane.setPreferredSize(new Dimension(WINDOW_WIDTH, WINDOW_HEIGHT));

        jScrollPane.setLayout(new ScrollPaneLayout());
        c.add(jScrollPane);
        jScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
        panel.add(createBut(), gbc);
        //panel.add(numRecords(vkrsEntities.size()), gbc);

        panel.add(article(), gbc);
        Iterator<VkrsEntity> iter = vkrsEntities.iterator();
        while(iter.hasNext()){
            panel.add(createNewJPanel(iter.next()), gbc);
        }
        panel.setBackground(color);
        pack();
        setPreferredSize(new Dimension(WINDOW_WIDTH, WINDOW_HEIGHT));
        setVisible(true);
    }

    private JPanel createNewJPanel(VkrsEntity model) {
        JPanel pan = new JPanel();
        pan.setPreferredSize(new Dimension(PANAL_WIDTH, PANAL_HEIGHT));
        pan.setLayout(null);
        int otst = 20;
        int getIdJ = model.getID_vkr();
        JLabel fioJ = new JLabel(model.getFIOStudent());
        fioJ.setBounds(otst, PANAL_HEIGHT - BUTTON_HEIGHT - 10, 5*BUTTON_WIDTH/7 , BUTTON_HEIGHT);
        fioJ.setFont(new Font("Times New Roman", NORMAL, 15));
        fioJ.setBackground(Color.gray);
        fioJ.setForeground(Color.DARK_GRAY);
        pan.add(fioJ);
        otst += 5*BUTTON_WIDTH/7;
        JLabel themeJ = new JLabel(model.getTheme());
        themeJ.setBounds(otst, PANAL_HEIGHT - BUTTON_HEIGHT - 10, 3*BUTTON_WIDTH/2 , BUTTON_HEIGHT);
        themeJ.setFont(new Font("Times New Roman", NORMAL, 15));
        themeJ.setBackground(Color.gray);
        themeJ.setForeground(Color.DARK_GRAY);
        pan.add(themeJ);
        otst += 3*BUTTON_WIDTH/2;
        JLabel faJ = new JLabel(model.getFIOtch());
        faJ.setBounds(otst, PANAL_HEIGHT - BUTTON_HEIGHT - 10, 5*BUTTON_WIDTH / 7, BUTTON_HEIGHT);
        faJ.setFont(new Font("Times New Roman", NORMAL, 15));
        faJ.setBackground(Color.gray);
        faJ.setForeground(Color.DARK_GRAY);
        pan.add(faJ);
        colorC++;
        Color col1 = new Color(250, 250, 250);
        Color col2 = new Color(225, 225, 225);
        if(colorC%2 == 0){
            pan.setBackground(col1);
        }else pan.setBackground(col2 );
        return pan;
    }

    private JPanel article(){
        JPanel pan = new JPanel();
        pan.setPreferredSize(new Dimension(PANAL_WIDTH, PANAL_HEIGHT));
        pan.setLayout(null);
        int otst = 20;
        JLabel stJ = new JLabel("ФИО студента");
        stJ.setBounds(otst, PANAL_HEIGHT - BUTTON_HEIGHT - 10, 5*BUTTON_WIDTH / 7, BUTTON_HEIGHT);
        stJ.setFont(new Font("Times New Roman", NORMAL, 15));
        stJ.setBackground(Color.gray);
        stJ.setForeground(Color.DARK_GRAY);
        pan.add(stJ);
        otst += 5*BUTTON_WIDTH / 7;
        JLabel themeJ = new JLabel("Тема ВКР");
        themeJ.setBounds(otst, PANAL_HEIGHT - BUTTON_HEIGHT - 10, BUTTON_WIDTH , BUTTON_HEIGHT);
        themeJ.setFont(new Font("Times New Roman", NORMAL, 15));
        themeJ.setBackground(Color.gray);
        themeJ.setForeground(Color.DARK_GRAY);
        pan.add(themeJ);
        otst += BUTTON_WIDTH;
        JLabel faJ = new JLabel("ФИО преподавателя");
        faJ.setBounds(otst, PANAL_HEIGHT - BUTTON_HEIGHT - 10, 5*BUTTON_WIDTH / 7, BUTTON_HEIGHT);
        faJ.setFont(new Font("Times New Roman", NORMAL, 15));
        faJ.setBackground(Color.gray);
        faJ.setForeground(Color.DARK_GRAY);
        pan.add(faJ);
        colorC++;

        Color col2 = new Color(225, 225, 225);
        pan.setBackground(col2);
        return pan;
    }

    private JPanel createBut() throws SQLException{
        Color color = new Color (240, 240, 240);
        JPanel panel2 = new JPanel();
        panel2.setPreferredSize(new Dimension(PANAL_WIDTH, PANAL_HEIGHT));
        panel2.setLayout(null);
        JButton addNewB;
        if (vkrsController.getRole() != 0) {
            addNewB = new JButton("Добавить новую ВКР");
            final AddVkr addVkr = new AddVkr(vkrsController);
            addNewB.setBounds(50, BUTTON_HEIGHT / 2, BUTTON_WIDTH / 2, 30);
            addNewB.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    try {
                        dispose();
                        addVkr.visibleAV();
                    } catch (SQLException eds) {
                        eds.printStackTrace();
                    }
                }
            });
            addNewB.setBackground(color);
            panel2.add(addNewB);
        }
        JButton returnB = new JButton("Назад");
        returnB.setBounds(PANAL_WIDTH - BUTTON_WIDTH, BUTTON_HEIGHT/2, 70, 30 );
        returnB.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });
        returnB.setBackground(color);
        panel2.add(returnB);

        JButton filtB = new JButton("Расширенный поиск ВКР");
        final SearchVkrs searchVkrs = new SearchVkrs(vkrsController);
        filtB.setBounds(60+3*BUTTON_WIDTH/4, BUTTON_HEIGHT/2, 3*BUTTON_WIDTH/4, 30 );
        filtB.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    dispose();
                    searchVkrs.visible();
                }catch (SQLException eds){
                    eds.printStackTrace(); }
            }
        });
        filtB.setBackground(color);
        panel2.add(filtB);
        return panel2;
    }

    private JPanel numRecords(int size1) throws SQLException{
        Color color = new Color (240, 240, 240);
        JPanel panel2 = new JPanel();
        panel2.setPreferredSize(new Dimension(PANAL_WIDTH, PANAL_HEIGHT));
        panel2.setLayout(null);

        JLabel themeJ = new JLabel("Найдено: ");
        //themeJ.setBounds(20, PANAL_HEIGHT - BUTTON_HEIGHT - 10, BUTTON_WIDTH/3 , BUTTON_HEIGHT);
        themeJ.setFont(new Font("Times New Roman", NORMAL, 15));
        themeJ.setBackground(Color.gray);
        themeJ.setForeground(Color.DARK_GRAY);
        panel2.add(themeJ);

        String tmp = String.valueOf(size1);
        JLabel faJ = new JLabel(String.valueOf(size1));
        themeJ.setBounds(20 + BUTTON_WIDTH/3, PANAL_HEIGHT - BUTTON_HEIGHT - 10, BUTTON_WIDTH , BUTTON_HEIGHT);
        faJ.setFont(new Font("Times New Roman", NORMAL, 15));
        faJ.setBackground(Color.gray);
        faJ.setForeground(Color.DARK_GRAY);
        panel2.add(faJ);
        colorC++;

        Color col2 = new Color(225, 225, 225);
        panel2.setBackground(col2);


        return panel2;
    }

}
