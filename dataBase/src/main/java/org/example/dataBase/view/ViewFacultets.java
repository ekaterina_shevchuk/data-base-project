package org.example.dataBase.view;

import org.example.dataBase.controller.FacultetsController;
import org.example.dataBase.model.FacultetsEntity;
import org.example.dataBase.service.FacultetsService;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;

public class ViewFacultets extends JFrame {
    final FacultetsController facultetsController;
    static int WINDOW_WIDTH = 1350;
    static int WINDOW_HEIGHT = 750;
    static int PANAL_WIDTH = 1100;
    static int PANAL_HEIGHT = 70;
    static int BUTTON_WIDTH = 350;
    static int BUTTON_HEIGHT = 50;

    int colorC = 0;

    public ViewFacultets(ArrayList<FacultetsEntity> facultets, FacultetsController facultetsController1)throws SQLException {
        facultetsController = facultetsController1;
        Container c = getContentPane();
        c.setLayout(new BorderLayout());
        setTitle("Просмотр факультетов");
        setMinimumSize(new Dimension(WINDOW_WIDTH, WINDOW_HEIGHT));
        setResizable(false);

        JPanel panel = new JPanel();
        panel.setBorder(new EmptyBorder(10, 10, 10, 10));
        GridBagLayout gridBagLayout = new GridBagLayout();
        gridBagLayout.rowHeights = new int[]{BUTTON_HEIGHT, BUTTON_HEIGHT,BUTTON_HEIGHT,BUTTON_HEIGHT,BUTTON_HEIGHT };
        gridBagLayout.columnWeights = new double[]{1.0};
        gridBagLayout.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0};
        panel.setLayout(gridBagLayout);

        GridBagConstraints gbc = new GridBagConstraints();
        gbc.weightx = 1;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.gridwidth = GridBagConstraints.REMAINDER;
        c.add(panel);
        JScrollPane jScrollPane = new JScrollPane(panel);
        jScrollPane.setPreferredSize(new Dimension(WINDOW_WIDTH, WINDOW_HEIGHT));

        jScrollPane.setLayout(new ScrollPaneLayout());
        c.add(jScrollPane);
        jScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
        panel.add(createBut(), gbc);
        panel.add(article(), gbc);
        Iterator<FacultetsEntity> iter = facultets.iterator();
        while(iter.hasNext()){
            panel.add(createNewJPanel(iter.next()), gbc);
        }
        Color color = new Color (240, 240, 240);
        panel.setBackground(color);
        pack();
        setPreferredSize(new Dimension(WINDOW_WIDTH, WINDOW_HEIGHT));
        setVisible(true);
    }

    private JPanel createNewJPanel(FacultetsEntity model) {
        JPanel pan = new JPanel();
        pan.setPreferredSize(new Dimension(PANAL_WIDTH, PANAL_HEIGHT));
        pan.setLayout(null);

        JLabel nameJ = new JLabel(model.getName());
        nameJ.setBounds(230 + BUTTON_WIDTH / 3, PANAL_HEIGHT - BUTTON_HEIGHT - 10, BUTTON_WIDTH , BUTTON_HEIGHT);
        nameJ.setFont(new Font("Times New Roman", NORMAL, 15));
        nameJ.setBackground(Color.gray);
        nameJ.setForeground(Color.DARK_GRAY);
        pan.add(nameJ);

        JLabel deanJ = new JLabel(model.getFIOdean());
        deanJ.setBounds(230 + 4 * BUTTON_WIDTH / 3, PANAL_HEIGHT - BUTTON_HEIGHT - 10, BUTTON_WIDTH , BUTTON_HEIGHT);
        deanJ.setFont(new Font("Times New Roman", NORMAL, 15));
        deanJ.setBackground(Color.gray);
        deanJ.setForeground(Color.DARK_GRAY);
        pan.add(deanJ);
        colorC++;
        Color col1 = new Color(250, 250, 250);
        Color col2 = new Color(225, 225, 225);
        if(colorC%2 == 0){
            pan.setBackground(col1);
        }else pan.setBackground(col2 );
        return pan;
    }

    private JPanel article(){
        JPanel pan = new JPanel();
        pan.setPreferredSize(new Dimension(PANAL_WIDTH, PANAL_HEIGHT));
        pan.setLayout(null);

        JLabel nameJ = new JLabel("Название");
        nameJ.setBounds(230 + BUTTON_WIDTH / 3, PANAL_HEIGHT - BUTTON_HEIGHT - 10, BUTTON_WIDTH , BUTTON_HEIGHT);
        nameJ.setFont(new Font("Times New Roman", NORMAL, 15));
        nameJ.setBackground(Color.gray);
        nameJ.setForeground(Color.DARK_GRAY);
        pan.add(nameJ);

        JLabel deanJ = new JLabel("ФИО декана");
        deanJ.setBounds(230 + 4 * BUTTON_WIDTH / 3, PANAL_HEIGHT - BUTTON_HEIGHT - 10, BUTTON_WIDTH, BUTTON_HEIGHT);
        deanJ.setFont(new Font("Times New Roman", NORMAL, 15));
        deanJ.setBackground(Color.gray);
        deanJ.setForeground(Color.DARK_GRAY);
        pan.add(deanJ);
        colorC++;

        Color col2 = new Color(225, 225, 225);
        pan.setBackground(col2);

        return pan;
    }

    private JPanel createBut() throws SQLException{
        Color color = new Color (240, 240, 240);
        JPanel panel2 = new JPanel();
        panel2.setPreferredSize(new Dimension(PANAL_WIDTH, PANAL_HEIGHT));
        panel2.setLayout(null);
        if (facultetsController.getRole() == 2) {
            JButton addNewB = new JButton("Добавить новый факультет");
            final AddFaculty addFaculty = new AddFaculty(facultetsController);
            addNewB.setBounds(50, BUTTON_HEIGHT / 2, BUTTON_WIDTH, 30);
            addNewB.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    try {
                        dispose();
                        addFaculty.visibleAF();
                    } catch (SQLException eds) {
                        eds.printStackTrace();
                    }
                }
            });
            addNewB.setBackground(color);
            panel2.add(addNewB);
        }
        JButton returnB = new JButton("Назад");
        returnB.setBounds(PANAL_WIDTH - BUTTON_WIDTH, BUTTON_HEIGHT/2, 70, 30 );
        returnB.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });
        returnB.setBackground(color);
        panel2.add(returnB);
        return panel2;
    }

}
