package org.example.dataBase.view;

import org.example.dataBase.controller.TeachersController;
import org.example.dataBase.model.TeachersEntity;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Date;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;

public class SearchTeachers extends JFrame {
    TeachersController teachersController;
    static int WINDOW_WIDTH = 1350;
    static int WINDOW_HEIGHT = 750;
    static int PANAL_WIDTH = 1100;
    static int PANAL_HEIGHT = 70;
    static int BUTTON_WIDTH = 350;
    static int BUTTON_HEIGHT = 50;

    private int num_gr;
    private String gender, subjectT, facultet, department, category, degree, occupationSub;
    private int yearBD, course;
    private int age;
    private int checkChild;
    private int wages;
    private int yearSub;
    private int semestr;
    private Date workDate1, workDate2;

    private String answ = "";

    public SearchTeachers(TeachersController teachersController1)throws SQLException {
        teachersController = teachersController1;
    }

    public void visible() throws SQLException{
        Container c = getContentPane();
        c.setLayout(new BorderLayout());
        setTitle("Расширенный поиск преподавателей");
        setMinimumSize(new Dimension(WINDOW_WIDTH, WINDOW_HEIGHT));
        setResizable(false);

        JPanel panel = new JPanel();
        panel.setBorder(new EmptyBorder(10, 10, 10, 10));
        GridBagLayout gridBagLayout = new GridBagLayout();
        gridBagLayout.rowHeights = new int[]{BUTTON_HEIGHT, BUTTON_HEIGHT, BUTTON_HEIGHT, BUTTON_HEIGHT, BUTTON_HEIGHT,BUTTON_HEIGHT, BUTTON_HEIGHT, BUTTON_HEIGHT, BUTTON_HEIGHT, BUTTON_HEIGHT, BUTTON_HEIGHT};
        gridBagLayout.columnWeights = new double[]{1.0};
        gridBagLayout.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0};
        panel.setLayout(gridBagLayout);
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.weightx = 1;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.gridwidth = GridBagConstraints.REMAINDER;
        c.add(panel);
        JScrollPane jScrollPane = new JScrollPane(panel);
        jScrollPane.setPreferredSize(new Dimension(WINDOW_WIDTH, WINDOW_HEIGHT));

        jScrollPane.setLayout(new ScrollPaneLayout());
        c.add(jScrollPane);
        jScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
        GridBagConstraints layConstraints;

        JLabel departmentLabel = new JLabel("Выберите кафедру");
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 0;
        layConstraints.gridy = 0;
        panel.add(departmentLabel,layConstraints);
        ArrayList<String> departments = teachersController.getDepartments();
        final JComboBox departmentBox = new JComboBox();
        Iterator<String> iter1= departments.iterator();
        int k = 0;
        int g = departments.size();
        for (k = 0; k < g; k++){
            //if (k == g) break;
            departmentBox.addItem(departments.get(k));
            //k++;
        }
        departmentBox.setAlignmentX(LEFT_ALIGNMENT);
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 1;
        layConstraints.gridy = 0;
        departmentBox.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                answ += "1 ";
            }
        });
        panel.add(departmentBox, layConstraints);


        JLabel facultyLabel = new JLabel("Выберите факультет");
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 0;
        layConstraints.gridy = 1;
        panel.add(facultyLabel,layConstraints);
        ArrayList<String> facultets = teachersController.getFacultets();
        final JComboBox facultyBox = new JComboBox();
        Iterator<String> iter2= facultets.iterator();
        k = 0;
        g = facultets.size();
        while(iter2.hasNext()){
            if (k == g) break;
            facultyBox.addItem(facultets.get(k));
            k++;
        }
        facultyBox.setAlignmentX(LEFT_ALIGNMENT);
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 1;
        layConstraints.gridy = 1;
        facultyBox.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                answ += "2 ";
            }
        });
        panel.add(facultyBox, layConstraints);

        JLabel categoryLabel = new JLabel("Выберите категорию");
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 0;
        layConstraints.gridy = 2;
        panel.add(categoryLabel,layConstraints);
        ArrayList<String> categories = teachersController.getCategories();
        final JComboBox categoryBox = new JComboBox();
        Iterator<String> iter3 = categories.iterator();
        k = 0;
        g = categories.size();
        while(iter3.hasNext()){
            if (k == g) break;
            categoryBox.addItem(categories.get(k));
            k++;
        }
        categoryBox.setAlignmentX(LEFT_ALIGNMENT);
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 1;
        layConstraints.gridy = 2;
        categoryBox.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                answ += "3 ";
            }
        });
        panel.add(categoryBox, layConstraints);

        JLabel gendL = new JLabel("Выберите пол");
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 2;
        layConstraints.gridy = 2;
        panel.add(gendL,layConstraints);
        String[] gendS = {"м", "ж"};
        final JComboBox gendBox = new JComboBox(gendS);
        gendBox.setAlignmentX(LEFT_ALIGNMENT);
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 3;
        layConstraints.gridy = 2;
        gendBox.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                answ += "4 ";
            }
        });
        panel.add(gendBox, layConstraints);

        JLabel yearL = new JLabel("Введите год рождения");
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 0;
        layConstraints.gridy = 3;
        panel.add(yearL,layConstraints);
        final JTextField yearBDField = new JTextField("", 10);
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 1;
        layConstraints.gridy = 3;

        yearBDField.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                yearBD = Integer.parseInt(yearBDField.getText());
                answ += "5 ";
            }
        });
        panel.add(yearBDField, layConstraints);

        Color color = new Color (240, 240, 240);
        JLabel ageL = new JLabel("Введите возраст");
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 2;
        layConstraints.gridy = 3;
        panel.add(ageL,layConstraints);
        final JTextField ageField = new JTextField("", 10);
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 3;
        layConstraints.gridy = 3;
        ageField.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                age = Integer.parseInt(ageField.getText());
                answ += "6 ";
            }
        });
        panel.add(ageField, layConstraints);

        JLabel chlL = new JLabel("Введите количество детей");
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 0;
        layConstraints.gridy = 4;
        panel.add(chlL,layConstraints);
        final JTextField chField = new JTextField("", 10);
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 1;
        layConstraints.gridy = 4;
        chField.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                checkChild = Integer.parseInt(chField.getText());
                answ += "7 ";
            }
        });
        panel.add(chField, layConstraints);

        JLabel schL = new JLabel("Введите минимальный размер зарплаты");
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 0;
        layConstraints.gridy = 5;
        panel.add(schL,layConstraints);
        final JTextField schField = new JTextField("", 10);
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 1;
        layConstraints.gridy = 5;
        schField.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                wages = Integer.parseInt(schField.getText());
                answ += "8 ";
            }
        });
        panel.add(schField, layConstraints);

        JLabel degreeLabel = new JLabel("Выберите научную степень");
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 2;
        layConstraints.gridy = 5;
        panel.add(degreeLabel,layConstraints);
        ArrayList<String> degrees = teachersController.getDegree();
        final JComboBox degreeBox = new JComboBox();
        Iterator<String> iter6 = degrees.iterator();
        k = 0;
        g = degrees.size();
        while(iter6.hasNext()){
            if (k == g) break;
            degreeBox.addItem(degrees.get(k));
            k++;
        }
        degreeBox.setAlignmentX(LEFT_ALIGNMENT);
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 3;
        layConstraints.gridy = 5;
        degreeBox.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                answ += "10 ";
            }
        });
        panel.add(degreeBox, layConstraints);


        JButton query5B = new JButton("Получить список преподавателей, проводивших (проводящих) занятия по указанной дисциплине в указанной группе, либо на указанном курсе указанного факультета");
        query5B.setPreferredSize(new Dimension(2*BUTTON_WIDTH/3, BUTTON_HEIGHT));
        query5B.setToolTipText("Получить список преподавателей, проводивших (проводящих) занятия по указанной дисциплине в указанной группе, либо на указанном курсе указанного факультетай");
        query5B.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                    dispose();
                    try {
                        AdditSearchTch1 additSearchTch1 = new AdditSearchTch1(teachersController);
                    }catch (SQLException eds){eds.printStackTrace();}
            }
        });
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 0;
        layConstraints.gridy = 6;
        panel.add(query5B,layConstraints);

        JButton query6B = new JButton("Получить перечень преподавателей проводивших лекционные, семинарские и другие виды занятий в указанной группе либо на указанном курсе указанного факультета в указанном семестре");
        query6B.setPreferredSize(new Dimension(2*BUTTON_WIDTH/3, BUTTON_HEIGHT));
        query6B.setToolTipText("Получить перечень преподавателей проводивших лекционные, семинарские и другие виды занятий в указанной группе либо на указанном курсе указанного факультета в указанном семестре");
        query6B.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
                try {
                    AdditSearchTch2 additSearchTch2 = new AdditSearchTch2(teachersController);
                }catch (SQLException eds){eds.printStackTrace();}
            }
        });
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 1;
        layConstraints.gridy = 6;
        panel.add(query6B,layConstraints);

        JButton query9B = new JButton("Получить перечень преподавателей, принимающих (принимавших) экзамены в указанных группах, по указанным дисциплинам, в указанном семестре");
        query9B.setPreferredSize(new Dimension(2*BUTTON_WIDTH/3, BUTTON_HEIGHT));
        query9B.setToolTipText("Получить перечень преподавателей, принимающих (принимавших) экзамены в указанных группах, по указанным дисциплинам, в указанном семестре");
        query9B.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
                try {
                    AdditSearchTch3 additSearchTch3 = new AdditSearchTch3(teachersController);
                }catch (SQLException eds){eds.printStackTrace();}
            }
        });
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 2;
        layConstraints.gridy = 6;
        panel.add(query9B,layConstraints);

        JButton query12B = new JButton("Получить список руководителей дипломных работ с указанной кафедры, либо факультета полностью и раздельно по некоторым категориям преподавателей");
        query12B.setPreferredSize(new Dimension(2*BUTTON_WIDTH/3, BUTTON_HEIGHT));
        query12B.setToolTipText("Получить список руководителей дипломных работ с указанной кафедры, либо факультета полностью и раздельно по некоторым категориям преподавателей");
        query12B.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
                try {
                    AdditSearchTch4 additSearchTch4 = new AdditSearchTch4(teachersController);
                }catch (SQLException eds){eds.printStackTrace();}
            }
        });
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 0;
        layConstraints.gridy = 7;
        panel.add(query12B,layConstraints);

        JButton loadB = new JButton("Просмотр нагрузки преподавателя");
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 1;
        layConstraints.gridy = 7;
        loadB.setPreferredSize(new Dimension(2*BUTTON_WIDTH/3, BUTTON_HEIGHT));
        final SearchTeachersLoad searchTeachersLoad = new SearchTeachersLoad(teachersController);
        loadB.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
                try {
                    searchTeachersLoad.visible();
                }catch (SQLException eds){eds.printStackTrace();}
            }
        });
        panel.add(loadB, layConstraints);

        JButton sworkB = new JButton("Просмотр кандидатских и докторских диссертаций");
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 2;
        layConstraints.gridy = 7;
        sworkB.setPreferredSize(new Dimension(2*BUTTON_WIDTH/3, BUTTON_HEIGHT));
        final SWorkSearch sWorkSearch = new SWorkSearch(teachersController);
        sworkB.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    dispose();
                    sWorkSearch.visible();
                }catch (SQLException eds){eds.printStackTrace();}
            }
        });
        panel.add(sworkB, layConstraints);

        JLabel helpLabel1 = new JLabel("Выберите один из параметров");
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 2;
        layConstraints.gridy = 8;
        panel.add(helpLabel1,layConstraints);

        JLabel helpLabel2 = new JLabel("После окончания ввода нажмите Enter");
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 3;
        layConstraints.gridy = 8;
        panel.add(helpLabel2,layConstraints);

        final JButton searchB = new JButton("Поиск");
        searchB.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
                //num_gr = (Integer) groupBox.getSelectedItem();
                gender = (String) gendBox.getSelectedItem();
                //subjectT = (String)subjectBox.getSelectedItem();
                //semestr = (Integer) semestrBox.getSelectedItem();
                department = (String) departmentBox.getSelectedItem();
                category = (String) categoryBox.getSelectedItem();
                degree = (String) degreeBox.getSelectedItem();
                facultet = (String) facultyBox.getSelectedItem();
                //course = (Integer) courseBox.getSelectedItem();
                //occupationSub = (String) occupationBox.getSelectedItem();
                try{
                    callSearch();
                }catch (SQLException eds){eds.printStackTrace();}
            }
        });
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 4;
        layConstraints.gridy = 0;
        panel.add(searchB,layConstraints);

        final JButton resetB = new JButton("Сброс");
        resetB.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                resetData();
                schField.setText("");
                yearBDField.setText("");
                ageField.setText("");
                chField.setText("");
                //yearTestField.setText("");
            }
        });
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 4;
        layConstraints.gridy = 1;
        panel.add(resetB,layConstraints);

        panel.setBackground(color);
        pack();
        setPreferredSize(new Dimension(WINDOW_WIDTH, WINDOW_HEIGHT));
        setVisible(true);
    }

    private void resetData(){
        num_gr = 0;
        gender = "";
        degree = "";
        department = "";
        category = "";
        facultet = "";
        course = 0;
        subjectT = "";
        yearBD = 0;
        age = 0;
        checkChild = 0;
        wages = 0;
        yearSub = 0;
        semestr = 0;
        answ = "";
        return;
    }

    public void callSearch() throws SQLException{
        if(answ.equals("1 ")){
            dispose();
            ArrayList<TeachersEntity> teachersEntities = teachersController.chooseTeachers(department, "", "", "", 0, 0, -1, 0, "", workDate1, workDate2);
            ViewTeachers viewTeachers = new ViewTeachers(teachersEntities, teachersController);
            return;
        }else
        if(answ.equals("2 ")){
            dispose();
            ArrayList<TeachersEntity> teachersEntities = teachersController.chooseTeachers("", facultet, "", "", 0, 0, -1, 0, "",  workDate1, workDate2);
            ViewTeachers viewTeachers = new ViewTeachers(teachersEntities, teachersController);
            return;
        }else
        if(answ.equals("3 ")){
            dispose();
            ArrayList<TeachersEntity> teachersEntities = teachersController.chooseTeachers("", "", category, "", 0, 0, -1, 0, "",  workDate1, workDate2);
            ViewTeachers viewTeachers = new ViewTeachers(teachersEntities, teachersController);
            return;
        }else
        if(answ.equals("4 ")){
            dispose();
            ArrayList<TeachersEntity> teachersEntities = teachersController.chooseTeachers("", "", "", gender, 0, 0, -1, 0, "",  workDate1, workDate2);
            ViewTeachers viewTeachers = new ViewTeachers(teachersEntities, teachersController);
            return;
        }else
        if(answ.equals("5 ")){
            dispose();
            ArrayList<TeachersEntity> teachersEntities = teachersController.chooseTeachers("", "", "", "", yearBD, 0, -1, 0, "",  workDate1, workDate2);
            ViewTeachers viewTeachers = new ViewTeachers(teachersEntities, teachersController);
            return;
        }else
        if(answ.equals("6 ")){
            dispose();
            ArrayList<TeachersEntity> teachersEntities = teachersController.chooseTeachers("", "", "", "", 0, age, -1, 0, "",  workDate1, workDate2);
            ViewTeachers viewTeachers = new ViewTeachers(teachersEntities, teachersController);
            return;
        }else
        if(answ.equals("7 ")){
            dispose();
            ArrayList<TeachersEntity> teachersEntities = teachersController.chooseTeachers("", "", "", "", 0, 0, checkChild, 0, "",  workDate1, workDate2);
            ViewTeachers viewTeachers = new ViewTeachers(teachersEntities, teachersController);
            return;
        }else
        if(answ.equals("8 ")){
            dispose();
            ArrayList<TeachersEntity> teachersEntities = teachersController.chooseTeachers("", "", "", "", 0, 0, -1, wages, "",  workDate1, workDate2);
            ViewTeachers viewTeachers = new ViewTeachers(teachersEntities, teachersController);
            return;
        }else
        if(answ.equals("10 ")){
            dispose();
            ArrayList<TeachersEntity> teachersEntities = teachersController.chooseTeachers("", "", "", "", 0, 0, -1, 0, degree,  workDate1, workDate2);
            ViewTeachers viewTeachers = new ViewTeachers(teachersEntities, teachersController);
            return;
        }else

        if(answ.equals("11 12 ")){
            dispose();
            ArrayList<TeachersEntity> teachersEntities = teachersController.getAddition(subjectT, num_gr, 0, "", 0, "", 0, "");
            ViewTeachers viewTeachers = new ViewTeachers(teachersEntities, teachersController);
            return;
        }else
        if(answ.equals("2 11 13 ")){
            dispose();
            ArrayList<TeachersEntity> teachersEntities = teachersController.getAddition(subjectT, 0, course, facultet, 0, "", 0, "");
            ViewTeachers viewTeachers = new ViewTeachers(teachersEntities, teachersController);
            return;
        }else
        if(answ.equals("12 14 16 ")){
            dispose();
            ArrayList<TeachersEntity> teachersEntities = teachersController.getAddition("", num_gr, 0, "", semestr, occupationSub, 0, "");
            ViewTeachers viewTeachers = new ViewTeachers(teachersEntities, teachersController);
            return;
        }else
        if(answ.equals("12 14 15 ")){
            dispose();
            ArrayList<TeachersEntity> teachersEntities = teachersController.getAddition("", num_gr, 0, "", 0, occupationSub, yearSub, "");
            ViewTeachers viewTeachers = new ViewTeachers(teachersEntities, teachersController);
            return;
        }else
        if(answ.equals("2 13 14 15 ")){
            dispose();
            ArrayList<TeachersEntity> teachersEntities = teachersController.getAddition("", 0, course, facultet, 0, occupationSub, yearSub, "");
            ViewTeachers viewTeachers = new ViewTeachers(teachersEntities, teachersController);
            return;
        }else
        if(answ.equals("2 13 14 16 ")){
            dispose();
            ArrayList<TeachersEntity> teachersEntities = teachersController.getAddition("", 0, course, facultet, semestr, occupationSub, 0, "");
            ViewTeachers viewTeachers = new ViewTeachers(teachersEntities, teachersController);
            return;
        }else //**********************************************
        if(answ.equals("1 ! ")){
            dispose();
            ArrayList<TeachersEntity> teachersEntities = teachersController.getAddition("", 0, 0, "", 0, "", 0, department);
            ViewTeachers viewTeachers = new ViewTeachers(teachersEntities, teachersController);
            return;
        }else
        if(answ.equals("2 ! ")){
            dispose();
            ArrayList<TeachersEntity> teachersEntities = teachersController.getAddition("", 0, 0, facultet, 0, "", 0, "");
            ViewTeachers viewTeachers = new ViewTeachers(teachersEntities, teachersController);
            return;
        }else
        if(answ.equals("11 12 16 ")){
            dispose();
            ArrayList<TeachersEntity> teachersEntities = teachersController.getAddition(subjectT, num_gr, 0, "", semestr, "", 0, "");
            ViewTeachers viewTeachers = new ViewTeachers(teachersEntities, teachersController);
            return;
        }else{
            ViewMistake viewMistake = new ViewMistake("По вашему запросу ничего не найдено, пожалуйста измените условия");}
        return;
    }

    public void setAnsw(String answ1){
        this.answ = answ1;
    }

    public void setQuery5(int course1, int num_gr1, String faculty1, String subject1){
        this.course = course1;
        this.num_gr = num_gr1;
        this.facultet = faculty1;
        this.subjectT = subject1;
    }

    public void setQuery6(int course1, int num_gr1, String faculty1, String ocupattion1, int semestr1, int year1){
        this.course = course1;
        this.num_gr = num_gr1;
        this.facultet = faculty1;
        this.occupationSub = ocupattion1;
        this.semestr = semestr1;
        this.yearSub = year1;
    }

    public void setQuery9(int num_gr1, String subject1, int semestr1){
        this.num_gr = num_gr1;
        this.subjectT = subject1;
        this.semestr = semestr1;
    }

    public void setQuery12(String department1, String faculty1){
        this.department = department1;
        this.facultet = faculty1;
    }

}
