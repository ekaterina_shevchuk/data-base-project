package org.example.dataBase.view;

import org.example.dataBase.controller.TeachersController;
import org.example.dataBase.model.ScientificWorkEntity;
import org.example.dataBase.model.TeachersEntity;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Date;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;

public class SWorkSearch extends JFrame {
    TeachersController teachersController;
    static int WINDOW_WIDTH = 1350;
    static int WINDOW_HEIGHT = 750;
    static int PANAL_WIDTH = 1100;
    static int PANAL_HEIGHT = 70;
    static int BUTTON_WIDTH = 350;
    static int BUTTON_HEIGHT = 50;

    private String facultet, department;
    private Date workDate1, workDate2;

    String answ = "";

    public SWorkSearch(TeachersController teachersController1)throws SQLException {
        teachersController = teachersController1;
    }

    public void visible() throws SQLException {
        Color color = new Color (240, 240, 240);
        Container c = getContentPane();
        c.setLayout(new BorderLayout());
        setTitle("Расширенный поиск преподавателей");
        setMinimumSize(new Dimension(WINDOW_WIDTH, WINDOW_HEIGHT));
        setResizable(false);

        JPanel panel = new JPanel();
        panel.setBorder(new EmptyBorder(10, 10, 10, 10));
        GridBagLayout gridBagLayout = new GridBagLayout();
        gridBagLayout.rowHeights = new int[]{BUTTON_HEIGHT, BUTTON_HEIGHT, BUTTON_HEIGHT, BUTTON_HEIGHT, BUTTON_HEIGHT, BUTTON_HEIGHT, BUTTON_HEIGHT, BUTTON_HEIGHT, BUTTON_HEIGHT, BUTTON_HEIGHT, BUTTON_HEIGHT};
        gridBagLayout.columnWeights = new double[]{1.0};
        gridBagLayout.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0};
        panel.setLayout(gridBagLayout);
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.weightx = 1;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.gridwidth = GridBagConstraints.REMAINDER;
        c.add(panel);
        JScrollPane jScrollPane = new JScrollPane(panel);
        jScrollPane.setPreferredSize(new Dimension(WINDOW_WIDTH, WINDOW_HEIGHT));

        jScrollPane.setLayout(new ScrollPaneLayout());
        c.add(jScrollPane);
        jScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
        GridBagConstraints layConstraints;

        JLabel departmentLabel = new JLabel("Выберите кафедру");
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 0;
        layConstraints.gridy = 2;
        panel.add(departmentLabel, layConstraints);
        ArrayList<String> departments = teachersController.getDepartments();
        final JComboBox departmentBox = new JComboBox();
        Iterator<String> iter1 = departments.iterator();
        int k = 0;
        int g = departments.size();
        for (k = 0; k < g; k++) {
            //if (k == g) break;
            departmentBox.addItem(departments.get(k));
            //k++;
        }
        departmentBox.setAlignmentX(LEFT_ALIGNMENT);
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 1;
        layConstraints.gridy = 2;
        departmentBox.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                answ += "1 ";
            }
        });
        panel.add(departmentBox, layConstraints);

        JLabel facultyLabel = new JLabel("Выберите факультет");
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 0;
        layConstraints.gridy = 3;
        panel.add(facultyLabel, layConstraints);
        ArrayList<String> facultets = teachersController.getFacultets();
        final JComboBox facultyBox = new JComboBox();
        Iterator<String> iter2 = facultets.iterator();
        k = 0;
        g = facultets.size();
        while (iter2.hasNext()) {
            if (k == g) break;
            facultyBox.addItem(facultets.get(k));
            k++;
        }
        facultyBox.setAlignmentX(LEFT_ALIGNMENT);
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 1;
        layConstraints.gridy = 3;
        facultyBox.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                answ += "2 ";
            }
        });
        panel.add(facultyBox, layConstraints);

        JLabel yearSWorkL = new JLabel("Введите даты защиты научной работы");
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 0;
        layConstraints.gridy = 4;
        panel.add(yearSWorkL,layConstraints);
        final JTextField yearSWorkField1 = new JTextField("", 10);
        yearSWorkField1.setToolTipText("Формат даты ГГГГ-ММ-ДД");
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 1;
        layConstraints.gridy = 4;
        yearSWorkField1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                workDate1 = Date.valueOf(yearSWorkField1.getText());
                answ += "9 ";
            }
        });
        panel.add(yearSWorkField1, layConstraints);
        final JTextField yearSWorkField2 = new JTextField("", 10);
        yearSWorkField2.setToolTipText("Формат даты ГГГГ-ММ-ДД");
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 2;
        layConstraints.gridy = 4;
        yearSWorkField2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                workDate2 = Date.valueOf(yearSWorkField2.getText());
            }
        });
        panel.add(yearSWorkField2, layConstraints);

        final JButton searchB = new JButton("Поиск");
        searchB.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                department = (String) departmentBox.getSelectedItem();
                facultet = (String) facultyBox.getSelectedItem();
                try{
                    callSearch();
                }catch (SQLException eds){eds.printStackTrace();}
            }
        });
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 4;
        layConstraints.gridy = 0;
        panel.add(searchB,layConstraints);

        final JButton resetB = new JButton("Сброс");
        resetB.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                resetData();
            }
        });
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 4;
        layConstraints.gridy = 1;
        panel.add(resetB,layConstraints);

        JLabel help1Label = new JLabel("Для просмотра перечня тем докторских и кандидатских,\n выберете кафедру или факультет интересующих сотрудников");
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 0;
        layConstraints.gridy = 0;
        panel.add(help1Label,layConstraints);

        JLabel helpLabel2 = new JLabel("Для просмотра перечня преподавателей защитивших кандидатские,\n докторские диссертации в указанный период - укажите период");
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 0;
        layConstraints.gridy = 1;
        panel.add(helpLabel2,layConstraints);

        final JButton returnB = new JButton("Назад");
        final SearchTeachers searchTeachers = new SearchTeachers(teachersController);
        returnB.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    dispose();
                    searchTeachers.visible();
                }catch (SQLException eds){eds.printStackTrace();}
            }
        });
        layConstraints = new GridBagConstraints();
        layConstraints.fill = GridBagConstraints.BASELINE;
        layConstraints.gridx = 4;
        layConstraints.gridy = 3;
        panel.add(returnB,layConstraints);

        panel.setBackground(color);
        pack();
        setPreferredSize(new Dimension(WINDOW_WIDTH, WINDOW_HEIGHT));
        setVisible(true);
    }

    private void resetData(){
        department = "";
        facultet = "";
        answ = "";
        return;
    }

    private void callSearch() throws SQLException{
        if(answ.equals("1 ")){
            dispose();
            ArrayList<ScientificWorkEntity> scientificWorkEntities = teachersController.getSwork(department, "");
            ArrayList<String> fioTch = teachersController.getSworkTeachers(department, "");
            ViewSWSearch viewSWSearch = new ViewSWSearch(scientificWorkEntities,fioTch,teachersController);
            return;
        }else
        if(answ.equals("2 ")){
            dispose();
            ArrayList<ScientificWorkEntity> scientificWorkEntities = teachersController.getSwork("", facultet);
            ArrayList<String> fioTch = teachersController.getSworkTeachers("", facultet);
            ViewSWSearch viewSWSearch = new ViewSWSearch(scientificWorkEntities,fioTch,teachersController);
            return;
        }else
        if(answ.equals("9 ")){
            dispose();
            ArrayList<TeachersEntity> teachersEntities = teachersController.chooseTeachers("", "", "", "", 0, 0, -1, 0, "", workDate1, workDate2);
            ViewTeachers viewTeachers = new ViewTeachers(teachersEntities, teachersController);
            return;
        }else{
            ViewMistake viewMistake = new ViewMistake("По вашему запросу ничего не найдено, пожалуйста измените условия");}
        return;
    }
}
