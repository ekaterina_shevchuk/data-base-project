package org.example.dataBase.controller;

import org.example.dataBase.model.StudentsEntity;
import org.example.dataBase.service.StudentsService;
import org.example.dataBase.view.ViewStudents;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class StudentsController {
    private int role;
    private Connection conn;
    private StudentsService studentsService;
    private int faculty;

    public StudentsController(Connection connection, int role1, int faculty1){
        faculty = faculty1;
        role = role1;
        conn = connection;
        studentsService = new StudentsService(conn);
        studentsService.setFaculty(faculty);
        studentsService.setRole(role);
    }

    public ArrayList<StudentsEntity> getAll() throws SQLException {
       return studentsService.getAll();
    }

    public ArrayList<Integer> getByGroups() throws SQLException {
        return studentsService.getByGroups();
    }

    public void addStudent(StudentsEntity studentsEntity) throws SQLException{
        studentsService.addStudent(studentsEntity);
    }

    public ArrayList<StudentsEntity> chooseStudents(int num_gr, String gender, int yearBD, int age, int checkChild, int scholarShip, int course, String faculty) throws SQLException{
       return studentsService.chooseStudents(num_gr, gender, yearBD, age, checkChild, scholarShip, course, faculty);
    }

    public ArrayList<StudentsEntity> getAddition(String FIOtch, String subjectTitle, int yearTest, int semester, int mark, int num_gr) throws SQLException{
      return studentsService.getAddition(FIOtch, subjectTitle, yearTest, semester, mark, num_gr);
    }

    public StudentsController returnStudContr(){return this;}

    public void OpenView()throws SQLException {
        ArrayList<StudentsEntity> studentsEntities = this.studentsService.getAll();
        ViewStudents viewStudents = new ViewStudents(studentsEntities, returnStudContr());
    }

    public ArrayList<Integer> getAllGroups() throws SQLException {
        return studentsService.getAllGroups();
    }

    public ArrayList<String> getSubjects() throws SQLException {
       return studentsService.getSubjects();
    }

    public void updateSt(int group, int checkCh, int scholarShip, String fio) throws SQLException{
        studentsService.updateSt(group, checkCh, scholarShip, fio);
    }

    public int getRole(){return role;}

    public Connection getConn(){return conn;}

    public int getFaculty() {
        return faculty;
    }
}
