package org.example.dataBase.controller;

import org.example.dataBase.model.DepartmentEntity;
import org.example.dataBase.model.FacultetsEntity;
import org.example.dataBase.service.DepartmentService;
import org.example.dataBase.service.FacultetsService;
import org.example.dataBase.view.ViewDepartment;
import org.example.dataBase.view.ViewFacultets;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class DepartmentController {
    private final Connection conn;
    private DepartmentService departmentService ;
    private int role;
    private int faculty;

    public DepartmentController(Connection connection, int role1, int faculty1){
        faculty = faculty1;
        role = role1;
        conn = connection;
        departmentService= new DepartmentService(conn);
        departmentService.setFaculty(faculty);
    }

    public DepartmentController ret(){return this;}

    public void openView() throws SQLException {
        ArrayList<DepartmentEntity> departmentEntities = this.departmentService.getAll();
        ViewDepartment viewDepartment = new ViewDepartment(departmentEntities, ret());
    }

    public ArrayList<Integer> getByGroups() throws SQLException {
        return departmentService.getByGroups();
    }
    public ArrayList<DepartmentEntity> getAll() throws SQLException {
        return departmentService.getAll();
    }

    public void addDepartment(DepartmentEntity departmentEntity) throws SQLException{
       departmentService.addDepartment(departmentEntity);
    }

    public ArrayList<DepartmentEntity> getDepartmentsByParam(int num_gr, String facultet, int course, int yearTest, int semestr) throws SQLException{
       return departmentService.getDepartmentsByParam(num_gr, facultet, course, yearTest, semestr);
    }

    public ArrayList<String> getFaultets() throws SQLException {
      return departmentService.getFaultets();
    }

    public Connection getConn(){return conn;}

    public int getFaculty() {
        return faculty;
    }

    public int getRole() {
        return role;
    }

}
