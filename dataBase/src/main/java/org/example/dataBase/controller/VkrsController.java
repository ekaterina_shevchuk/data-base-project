package org.example.dataBase.controller;

import org.example.dataBase.model.GroupsEntity;
import org.example.dataBase.model.VkrsEntity;
import org.example.dataBase.service.GroupsService;
import org.example.dataBase.service.VkrsService;
import org.example.dataBase.view.ViewGroups;
import org.example.dataBase.view.ViewVkrs;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class VkrsController {
    private final Connection conn;
    private VkrsService vkrsService ;
    private int role;
    private int faculty;

    public VkrsController(Connection connection, int role1, int faculty1){
        faculty = faculty1;
        role = role1;
        conn = connection;
        vkrsService = new VkrsService(conn);
        vkrsService.setFaculty(faculty);
    }

    public ArrayList<VkrsEntity> getAll() throws SQLException {
       return vkrsService.getAll();
    }

    public void addVkr(VkrsEntity vkrsEntity) throws SQLException{
        vkrsService.addVkr(vkrsEntity);
    }

    public ArrayList<String> getFIOst() throws SQLException{
       return vkrsService.getFIOst();
    }

    public ArrayList<VkrsEntity> getVKRS(String FIOtch, String kaf) throws SQLException{
      return vkrsService.getVKRS(FIOtch, kaf);
    }

    public VkrsController ret(){return this;}

    public void openView() throws SQLException {
        ArrayList<VkrsEntity> vkrsEntities = this.vkrsService.getAll();
        ViewVkrs viewVkrs = new ViewVkrs(vkrsEntities, ret());
    }

    public ArrayList<String> getFIOtch() throws SQLException {
       return vkrsService.getFIOtch();
    }

    public ArrayList<String> getDepartments() throws SQLException {
       return vkrsService.getDepartments();
    }

    public Connection getConn(){return conn;}

    public int getFaculty() {
        return faculty;
    }

    public int getRole() {
        return role;
    }
}
