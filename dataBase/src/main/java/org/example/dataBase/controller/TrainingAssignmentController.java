package org.example.dataBase.controller;

import org.example.dataBase.model.TrainingAssignmentEntity;
import org.example.dataBase.model.VkrsEntity;
import org.example.dataBase.service.TrainingAssignmentService;
import org.example.dataBase.service.VkrsService;
import org.example.dataBase.view.ViewTrainingAssignment;
import org.example.dataBase.view.ViewVkrs;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class TrainingAssignmentController {
    private final Connection conn;
    private TrainingAssignmentService trainingAssignmentService ;
    private int role;
    private int faculty;

    public TrainingAssignmentController(Connection connection, int role1, int faculty1){
        role = role1;
        conn = connection;
        faculty = faculty1;
        trainingAssignmentService = new TrainingAssignmentService(conn);
        trainingAssignmentService.setFaculty(faculty);
        trainingAssignmentService.setRole(role);
    }

    public ArrayList<TrainingAssignmentEntity> getAll() throws SQLException {
      return trainingAssignmentService.getAll();
    }

    public void addTrainingAssignment(TrainingAssignmentEntity trainingAssignmentEntity) throws SQLException{
        trainingAssignmentService.addTrainingAssignment(trainingAssignmentEntity);
    }

    public TrainingAssignmentController ret(){return this;}

    public void openView() throws SQLException {
        ArrayList<TrainingAssignmentEntity> trainingAssignmentEntities = this.trainingAssignmentService.getAll();
        ViewTrainingAssignment viewTrainingAssignment = new ViewTrainingAssignment(trainingAssignmentEntities, ret());
    }

    public ArrayList<String> getAllSubjects() throws SQLException {
        return trainingAssignmentService.getAllSubjects();
    }

    public ArrayList<Integer> getCurriculumNum() throws SQLException {
        return trainingAssignmentService.getCurriculumNum();
    }

    public ArrayList<String> getFIOtch() throws SQLException {
       return trainingAssignmentService.getFIOtch();
    }

    public int getFaculty() {
        return faculty;
    }

    public int getRole() {
        return role;
    }
}
