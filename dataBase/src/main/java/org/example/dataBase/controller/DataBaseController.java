package org.example.dataBase.controller;


import org.example.dataBase.service.MyService;
import org.example.dataBase.view.FirstPage;
import org.example.dataBase.view.ViewMain;

import java.sql.*;
import java.util.Locale;
import java.util.Properties;
import java.util.TimeZone;

public class DataBaseController {
    private Connection conn;
    private MyService myService;
    private int role;
    private int faculty;
    private String user;
    private String password;
    private String dataBase;

    public DataBaseController(String user1, String password1, String dataBase1) throws SQLException {
        user = user1;
        password = password1;
        dataBase = dataBase1;
        try {
            Class.forName("oracle.jdbc.driver.OracleDriver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        //String url = "http://84.237.50.81:1521";
        //String url = "jdbc:oracle:thin:@84.237.50.81:1521:XE";
        String url = "jdbc:oracle:thin:@localhost:1521:XE";
        Properties props = new Properties();
        props.setProperty("user", user);
        props.setProperty("password", password);

        //props.setProperty("user", "system");
        //props.setProperty("password", "password");
        //props.setProperty("user", "HR");
        //props.setProperty("password", "hr");

        TimeZone timeZone = TimeZone.getTimeZone("GMT+7");
        TimeZone.setDefault(timeZone);
        Locale.setDefault(Locale.ENGLISH);
        conn = DriverManager.getConnection(url, props);
        conn.setSchema(dataBase);
        //conn.setSchema("PROJECT_SEM6_LA");
        conn.setAutoCommit(true);
        myService = new MyService(conn);
    }

    public void setFaculty(int id_tch) throws SQLException{
        myService.setRole(role);
        this.faculty = myService.getFacultet(id_tch);
    }

    public int getFaculty(){
        return this.faculty;
    }

    public void openApp(){
        FirstPage firstPage = new FirstPage(returnMyController());
    }

    public void openMain(int role1){
        ViewMain viewMain = new ViewMain(returnMyController());
        role = role1;
    }

    public void setRole(int role1){
        role = role1;
    }

    public int getRole(){return role;}

    public DataBaseController returnMyController(){return this;}

    public int checkLogin(int id, String pass) throws SQLException{
        String passCheck = myService.checkLogin(id);
        if ((pass.equals(passCheck)||(pass.equals(passCheck + " ")))){
            return 1;
        } else return 0;
    }

    public int checkRole(String role, int id) throws SQLException{
        String roleCheck = myService.checkRole(id);
        if (role.equals("Преподаватель")){
            if ((roleCheck.equals("Семинарист2"))||(roleCheck.equals("Лектор"))||(roleCheck.equals("Семинарист1"))||(roleCheck.equals("Семинарист3"))||(roleCheck.equals("Семинарист4"))){
                return 1;
            }
        }
        /*if (role.equals("Работник кафедры")){
            if (roleCheck.equals("Работник кафедры"));
        }*/
        if (role.equals("Работник деканата")){
            if ((roleCheck.equals("Работник деканата"))||(roleCheck.equals("Декан"))){
                return 1;
            }
        }
        if (role.equals("Работник ректората")){
            if((roleCheck.equals("Работник ректората")) || (roleCheck.equals("Ректор"))){
                return 1;
            }
        }
        return 0;
    }

    public Connection getConn(){return conn;}
}
