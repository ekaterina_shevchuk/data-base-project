package org.example.dataBase.controller;

import org.example.dataBase.model.ScientificWorkEntity;
import org.example.dataBase.model.SubjectEntity;
import org.example.dataBase.service.ScientificWorkService;
import org.example.dataBase.service.SubjectService;
import org.example.dataBase.view.ViewScientificWork;
import org.example.dataBase.view.ViewSubject;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class SubjectController {
    private final Connection conn;
    private SubjectService subjectService ;
    private int role;
    private int faculty;

    public SubjectController(Connection connection, int role1, int faculty1){
        faculty = faculty1;
        role = role1;
        conn = connection;
        subjectService = new SubjectService(conn);
        subjectService.setFaculty(faculty);
        subjectService.setRole(role);
    }

    public ArrayList<SubjectEntity> getAll() throws SQLException {
       return subjectService.getAll();
    }

    public void addSubject(SubjectEntity subjectEntity) throws SQLException{
       subjectService.addSubject(subjectEntity);
    }

    public ArrayList<String> getSubjects() throws SQLException {
       return subjectService.getSubjects();
    }

    public SubjectController ret(){return this;}

    public void openView() throws SQLException {
        ArrayList<SubjectEntity> subjectEntities = this.subjectService.getAll();
        ViewSubject viewSubject = new ViewSubject(subjectEntities, ret());
    }

    public int getRole() {
        return role;
    }

    public int getFaculty() {
        return faculty;
    }
}
