package org.example.dataBase.controller;

import org.example.dataBase.model.JobEntity;
import org.example.dataBase.model.VkrsEntity;
import org.example.dataBase.service.JobService;
import org.example.dataBase.service.VkrsService;
import org.example.dataBase.view.ViewJob;
import org.example.dataBase.view.ViewVkrs;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class JobController {
    private final Connection conn;
    private JobService jobService ;
    private int role;
    private int faculty;

    public JobController(Connection connection, int role1, int faculty1){
        faculty = faculty1;
        role = role1;
        conn = connection;
        jobService = new JobService(conn);
        jobService.setRole(role);
    }

    public ArrayList<JobEntity> getAll() throws SQLException {
        return jobService.getAll();
    }

    public void addJob(JobEntity jobEntity) throws SQLException{
        jobService.addJob(jobEntity);
    }

    public JobController ret(){return this;}

    public void openView() throws SQLException {
        ArrayList<JobEntity> jobEntities = this.jobService.getAll();
        ViewJob viewJob = new ViewJob(jobEntities, ret());
    }

    public int getRole() {
        return role;
    }
}
