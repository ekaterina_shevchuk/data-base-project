package org.example.dataBase.controller;

import org.example.dataBase.model.ScientificWorkEntity;
import org.example.dataBase.model.StudentsEntity;
import org.example.dataBase.model.TeachersEntity;
import org.example.dataBase.model.TrainingAssignmentEntity;
import org.example.dataBase.service.TeachersService;
import org.example.dataBase.view.ViewTeachers;

import java.sql.*;
import java.util.ArrayList;

public class TeachersController {
    private Connection conn;
    private TeachersService teachersService;
    private int role;
    private int faculty;

    public TeachersController(Connection connection, int role1, int faculty1){
        role = role1;
        conn = connection;
        faculty = faculty1;
        teachersService = new TeachersService(conn);
        teachersService.setFaculty(faculty);
        teachersService.setRole(role);
    }

    public TeachersController returnTC(){return this;}

    public void OpenView()throws SQLException {
        ArrayList<TeachersEntity> teachersEntities = this.teachersService.getAll();
        ViewTeachers viewTeachers = new ViewTeachers(teachersEntities, returnTC());
    }

    public void addTeacher(TeachersEntity teachersEntity) throws SQLException{
        teachersService.addTeacher(teachersEntity);
    }

    public ArrayList<String> getJobs() throws SQLException{
       return teachersService.getJobs();
    }

    public ArrayList<TeachersEntity> chooseTeachers(String departmnent, String faculty, String category, String gender, int yearBD, int age, int numChildrens, int wages, String degree, Date dateWork1, Date dateWork2) throws SQLException{
        return teachersService.chooseTeachers(departmnent, faculty, category, gender, yearBD, age, numChildrens, wages, degree, dateWork1, dateWork2);
    }

    public ArrayList<TeachersEntity> getAddition(String titleSub, int num_gr, int course, String faculty, int semester, String occupation, int yearSub, String department) throws SQLException {
        return teachersService.getAddition(titleSub, num_gr, course, faculty, semester, occupation, yearSub, department);
    }

    public ArrayList<TeachersEntity> getAll() throws SQLException{
        return teachersService.getAll();
    }

    public ArrayList<ScientificWorkEntity> getSwork(String department, String faculty) throws SQLException{
        return teachersService.getSwork(department, faculty);
    }

    public ArrayList<String> getSworkTeachers(String department, String faculty) throws SQLException{
        return teachersService.getSworkTeachers(department, faculty);
    }

    public ArrayList<TrainingAssignmentEntity> getTeachersLoad(String department, String fio) throws SQLException{
       return teachersService.getTeachersLoad(department, fio);
    }

    public ArrayList<Integer> getTeachersLoadNumHours(String department, String fio) throws SQLException{
        return teachersService.getTeachersLoadNumHours(department, fio);
    }

    public int getTeachersLoadSumNumHours(String department, String fio) throws SQLException{
       return teachersService.getTeachersLoadSumNumHours(department, fio);
    }

    public ArrayList<String> getFIOtch() throws SQLException {
      return teachersService.getFIOtch();
    }

    public ArrayList<String> getDepartments() throws SQLException{
        return teachersService.getDepartments();
    }

    public ArrayList<String> getFacultets() throws SQLException{
        return teachersService.getFacultets();
    }

    public ArrayList<String> getCategories() throws SQLException{
        return teachersService.getCategories();
    }

    public ArrayList<String> getSubjects() throws SQLException{
        return teachersService.getSubjects();
    }

    public ArrayList<Integer> getGroups() throws SQLException{
       return teachersService.getGroups();
    }

    public ArrayList<String> getDegree() throws SQLException{
       return teachersService.getDegree();
    }

    public ArrayList<String> getOccupation() throws SQLException{
      return teachersService.getOccupation();
    }

    public void updateTch(String department, String name_job, int checkCh, String category, String fio, String answ) throws SQLException{
       teachersService.updateTch(department, name_job, checkCh, category, fio, answ);
    }

    public Connection getConn(){return conn;}

    public int getRole() {
        return role;
    }

    public int getFaculty() {
        return faculty;
    }

    public void setFaculty(int faculty) {
        this.faculty = faculty;
    }
}
