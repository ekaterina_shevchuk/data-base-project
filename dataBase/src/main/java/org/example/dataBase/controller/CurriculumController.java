package org.example.dataBase.controller;

import org.example.dataBase.model.CurriculumEntity;
import org.example.dataBase.model.SubjectEntity;
import org.example.dataBase.model.TeachersEntity;
import org.example.dataBase.service.CurriculumService;
import org.example.dataBase.service.TeachersService;
import org.example.dataBase.view.ViewCurriculum;
import org.example.dataBase.view.ViewTeachers;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class CurriculumController {
    private Connection conn;
    private CurriculumService curriculumService;
    private int role;
    private int faculty;

    public CurriculumController(Connection connection, int role1, int faculty1){
        faculty = faculty1;
        role = role1;
        conn = connection;
        curriculumService = new CurriculumService(conn);
        curriculumService.setFaculty(faculty);
    }

    public ArrayList<CurriculumEntity> getAll() throws SQLException {
       return curriculumService.getAll();
    }

    public void addCurriculum(CurriculumEntity curriculumEntity) throws SQLException{
       curriculumService.addCurriculum(curriculumEntity);
    }

    public CurriculumController ret(){return this;}

    public void OpenView()throws SQLException {
        ArrayList<CurriculumEntity> curriculumEntities = this.curriculumService.getAll();
        ViewCurriculum viewCurriculum = new ViewCurriculum(curriculumEntities, ret());
    }

    public ArrayList<Integer> getAllGroups() throws SQLException {
        return curriculumService.getAllGroups();
    }

    public ArrayList<String> getFaultets() throws SQLException {
       return curriculumService.getFaultets();
    }

    public ArrayList<String> getAllSubjectsLect() throws SQLException {
      return curriculumService.getAllSubjectsLec();
    }

    public ArrayList<String> getAllSubjectsSem() throws SQLException {
        return curriculumService.getAllSubjectsSem();
    }

    public ArrayList<String> getAllSubjectsLab() throws SQLException {
        return curriculumService.getAllSubjectsLab();
    }

    public int getRole() {
        return role;
    }
}
