package org.example.dataBase.controller;

import org.example.dataBase.view.ViewFacultets;
import org.example.dataBase.service.FacultetsService;
import org.example.dataBase.model.FacultetsEntity;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public  class FacultetsController {
    private int role;
    private final Connection conn;
    private FacultetsService facultetsService;
    private int faculty;

    public FacultetsController(Connection connection, int role1, int faculty1){
        faculty = faculty1;
        role = role1;
        conn = connection;
        facultetsService= new FacultetsService(conn);
        facultetsService.setRole(role);
        facultetsService.setFaculty(faculty);
    }

    public ArrayList<FacultetsEntity> getAll() throws SQLException {
       return facultetsService.getAll();
    }

    public void addFaculty(FacultetsEntity facultetsEntity) throws SQLException{
        facultetsService.addFaculty(facultetsEntity);
    }

    public ArrayList<String> getTitleFacultets() throws SQLException {
        return facultetsService.getTitleFacultets();
    }

    public FacultetsController ret(){return this;}

    public void openView() throws SQLException {
        ArrayList<FacultetsEntity> facultetsEntities = this.facultetsService.getAll();
        ViewFacultets viewFacultets = new ViewFacultets(facultetsEntities, ret());
    }

    public ArrayList<String> getDeans() throws SQLException {
       return facultetsService.getDeans();
    }

    public int getFaculty() {
        return faculty;
    }

    public int getRole() {
        return role;
    }
}
