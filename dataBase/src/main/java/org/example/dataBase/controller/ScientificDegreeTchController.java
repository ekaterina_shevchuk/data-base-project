package org.example.dataBase.controller;

import org.example.dataBase.model.ScientificDegreeTchEntity;
import org.example.dataBase.model.ScientificWorkEntity;
import org.example.dataBase.service.ScientificDegreeTchService;
import org.example.dataBase.service.ScientificWorkService;
import org.example.dataBase.view.ViewScientificDegreeTch;
import org.example.dataBase.view.ViewScientificWork;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class ScientificDegreeTchController {
    private final Connection conn;
    private ScientificDegreeTchService scientificDegreeTchService ;
    private int role;
    private int faculty;

    public ScientificDegreeTchController(Connection connection, int role1, int faculty1){
        faculty = faculty1;
        role = role1;
        conn = connection;
        scientificDegreeTchService = new ScientificDegreeTchService(conn);
        scientificDegreeTchService.setFaculty(faculty);
        scientificDegreeTchService.setRole(role);
    }

    public ArrayList<ScientificDegreeTchEntity> getAll() throws SQLException {
        return scientificDegreeTchService.getAll();
    }

    public void addScientificWork(ScientificDegreeTchEntity scientificDegreeTchEntity) throws SQLException{
        scientificDegreeTchService.addScientificWork(scientificDegreeTchEntity);
    }

    public ScientificDegreeTchController ret(){return this;}

    public void openView() throws SQLException {
        ArrayList<ScientificDegreeTchEntity> scientificDegreeTchEntities = this.scientificDegreeTchService.getAll();
        ViewScientificDegreeTch viewScientificDegreeTch = new ViewScientificDegreeTch(scientificDegreeTchEntities, ret());
    }

    public ArrayList<String> getFIOtch() throws SQLException {
      return scientificDegreeTchService.getFIOtch();
    }

    public int getFaculty() {
        return faculty;
    }

    public int getRole() {
        return role;
    }
}
