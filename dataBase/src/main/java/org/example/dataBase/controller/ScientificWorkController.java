package org.example.dataBase.controller;

import org.example.dataBase.model.GroupsEntity;
import org.example.dataBase.model.ScientificWorkEntity;
import org.example.dataBase.service.GroupsService;
import org.example.dataBase.service.ScientificWorkService;
import org.example.dataBase.view.ViewGroups;
import org.example.dataBase.view.ViewScientificWork;
import org.example.dataBase.view.ViewStudents;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class ScientificWorkController {
    private final Connection conn;
    private ScientificWorkService scientificWorkService ;
    private int role;
    private int faculty;

    public ScientificWorkController(Connection connection, int role1, int faculty1){
        faculty = faculty1;
        role = role1;
        conn = connection;
        scientificWorkService = new ScientificWorkService(conn);
        scientificWorkService.setFaculty(faculty);
        scientificWorkService.setRole(role);
    }

    public ArrayList<ScientificWorkEntity> getAll() throws SQLException {
       return scientificWorkService.getAll();
    }

    public void addScientificWork(ScientificWorkEntity scientificWorkEntity) throws SQLException{
      scientificWorkService.addScientificWork(scientificWorkEntity);
    }

    public ScientificWorkController returnSW(){return this;}

    public void openView() throws SQLException {
        ArrayList<ScientificWorkEntity> scientificWorkEntities = this.scientificWorkService.getAll();
        ViewScientificWork viewScientificWork = new ViewScientificWork(scientificWorkEntities, returnSW());
    }

    public ArrayList<String> getFIOtch() throws SQLException {
        return scientificWorkService.getFIOtch();
    }

    public int getFaculty() {
        return faculty;
    }

    public int getRole() {
        return role;
    }
}
