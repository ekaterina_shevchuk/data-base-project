package org.example.dataBase.controller;

import org.example.dataBase.model.FacultetsEntity;
import org.example.dataBase.model.GroupsEntity;
import org.example.dataBase.service.FacultetsService;
import org.example.dataBase.service.GroupsService;
import org.example.dataBase.view.ViewFacultets;
import org.example.dataBase.view.ViewGroups;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class GroupsController {
    private final Connection conn;
    private GroupsService groupsService ;
    private int role;
    private int faculty;

    public GroupsController(Connection connection, int role1, int faculty1){
        faculty = faculty1;
        role = role1;
        conn = connection;
        groupsService= new GroupsService(conn);
        groupsService.setFaculty(faculty);
        groupsService.setRole(role);
    }

    public ArrayList<GroupsEntity> getAll() throws SQLException {
       return groupsService.getAll();
    }

    public void addGroup(GroupsEntity groupsEntity) throws SQLException{
       groupsService.addGroup(groupsEntity);
    }

    public GroupsController ret(){return this;}

    public void openView() throws SQLException {
        ArrayList<GroupsEntity> groupsEntities = this.groupsService.getAll();
        ViewGroups viewGroups = new ViewGroups(groupsEntities, ret());
    }

    public ArrayList<String> getFaultets() throws SQLException {
       return groupsService.getFaultets();
    }

    public int getRole() {
        return role;
    }
}
