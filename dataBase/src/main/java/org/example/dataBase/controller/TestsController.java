package org.example.dataBase.controller;

import org.example.dataBase.model.CurriculumEntity;
import org.example.dataBase.model.TestsEntity;
import org.example.dataBase.service.CurriculumService;
import org.example.dataBase.service.TestsService;
import org.example.dataBase.view.ViewCurriculum;
import org.example.dataBase.view.ViewTests;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class TestsController {
    private Connection conn;
    private TestsService testsService;
    private int role;
    private int faculty;

    public TestsController(Connection connection, int role1, int faculty1){
        role = role1;
        conn = connection;
        faculty = faculty1;
        testsService = new TestsService(conn);
        testsService.setFaculty(faculty);
    }

    public ArrayList<TestsEntity> getAll() throws SQLException {
      return testsService.getAll();
    }

    public void addTests(TestsEntity testsEntity) throws SQLException{
        testsService.addTests(testsEntity);
    }

    public TestsController ret(){return this;}

    public void OpenView()throws SQLException {
        ArrayList<TestsEntity> testsEntities = this.testsService.getAll();
        ViewTests viewTests = new ViewTests(testsEntities, ret());
    }


    public ArrayList<String> getAllSubjectsLect() throws SQLException {
        return testsService.getAllSubjectsLec();
    }

    public ArrayList<String> getAllSubjectsSem() throws SQLException {
        return testsService.getAllSubjectsSem();
    }

    public ArrayList<String> getAllSubjectsLab() throws SQLException {
        return testsService.getAllSubjectsLab();
    }

    public ArrayList<Integer> getAllGroups() throws SQLException {
        return testsService.getAllGroups();
    }

    public ArrayList<String> getFIOst() throws SQLException {
        return testsService.getFIOst();
    }

    public ArrayList<String> getFIOtch() throws SQLException {
        return testsService.getFIOtch();
    }

    public ArrayList<Integer> getCurriculumNum() throws SQLException {
        return testsService.getCurriculumNum();
    }


    public int getFaculty() {
        return faculty;
    }

    public void setFaculty(int faculty) {
        this.faculty = faculty;
    }

    public int getRole() {
        return role;
    }
}
