package org.example.dataBase.service;

import org.example.dataBase.controller.TeachersController;
import org.example.dataBase.model.*;

import java.sql.*;
import java.util.ArrayList;

public class TeachersService {
    private final Connection conn;
    private int faculty_id;
    private int role;

    public void setFaculty(int faculty1){
        faculty_id = faculty1;
    }

    public TeachersService(Connection connection){
        conn = connection;
    }

    public ArrayList<TeachersEntity> getAll() throws SQLException {
        String sql = "SELECT * FROM teachers ";
        if(faculty_id != 0){
            sql += " WHERE ID_KAF IN (SELECT ID_KAF FROM DEPARTMENT WHERE ID_FA = " + String.valueOf(faculty_id) + ")";
        }
        PreparedStatement preStatement = conn.prepareStatement(sql);
        ResultSet result = preStatement.executeQuery();
        ArrayList<TeachersEntity> teachersEntities = new ArrayList<TeachersEntity>();
        while (result.next()) {
            TeachersEntity teachersEntity = new TeachersEntity();
            teachersEntity.setAll(result.getInt(1), result.getString(2), result.getInt(3), result.getInt(4), result.getString(5), result.getString(6), result.getDate(7) ,result.getInt(8));
            String sql1 = "SELECT TITLEJOB FROM JOB WHERE (ID_JOB = " + String.valueOf(teachersEntity.getID_job()) + ")";
            PreparedStatement preStatement1 = conn.prepareStatement(sql1);
            ResultSet result1 = preStatement1.executeQuery();
            if(result1.next()){
                teachersEntity.setName_job(result1.getString(1));
            } else{
                throw new SQLException("(Просмотр преподавателей) Должность не найдена");
            }
            if ((teachersEntity.getName_job().equals("Декан"))||teachersEntity.getName_job().equals("Работник деканата")||teachersEntity.getName_job().equals("Ректор")||teachersEntity.getName_job().equals("Работник ректората")) {
            }else {
                String sql2 = "SELECT TITLE FROM DEPARTMENT WHERE (ID_KAF = " + String.valueOf(teachersEntity.getID_kaf()) + ")";
                PreparedStatement preStatement2 = conn.prepareStatement(sql2);
                ResultSet result2 = preStatement2.executeQuery();
                if (result2.next()) {
                    teachersEntity.setName_kaf(result2.getString(1));
                } else {
                    throw new SQLException("(Просмотр преподавателей) Кафедра не найдена" + String.valueOf(teachersEntity.getID_kaf()));
                }
            }
            teachersEntities.add(teachersEntity);
        }
        return teachersEntities;
    }

    public void addTeacher(TeachersEntity teachersEntity) throws SQLException{
        String sql = "INSERT INTO TEACHERS(FIO, ID_KAF, ID_JOB, CATEGORY,GENDER, BIRTHDATE, NUMCHILDRENS)  VALUES(?,?,?,?,?,?,?)";
        String sql1 = "SELECT ID_KAF FROM DEPARTMENT WHERE (TITLE = '" + teachersEntity.getName_kaf() + "')";
        if (faculty_id != 0){
            sql1 += " AND (ID_FA = " + String.valueOf(faculty_id) + ")";
        }
        PreparedStatement preStatement1 = conn.prepareStatement(sql1);
        ResultSet result1 = preStatement1.executeQuery();
        if(result1.next()){
            teachersEntity.setID_kaf(result1.getInt(1));
        } else{
            throw new SQLException("(Добавление преподавателя) Кафедра не найдена");
        }
        String sql2 = "SELECT ID_JOB FROM JOB WHERE (TITLEJOB = '" + teachersEntity.getName_job() + "')";
        PreparedStatement preStatement2 = conn.prepareStatement(sql2);
        ResultSet result2 = preStatement2.executeQuery();
        if (result2.next()) {
            teachersEntity.setID_job(result2.getInt(1));
        }else{
            throw new SQLException("(Добавление преподавателей) Должность не найдена");
        }
        PreparedStatement preparedStatement = conn.prepareStatement(sql);
        preparedStatement.setString(1, teachersEntity.getFIO());
        preparedStatement.setInt(2, teachersEntity.getID_kaf());
        preparedStatement.setInt(3, teachersEntity.getID_job());
        preparedStatement.setString(4, teachersEntity.getCategory());
        preparedStatement.setString(5, teachersEntity.getGender());
        preparedStatement.setDate(6, teachersEntity.getBirthDate());
        preparedStatement.setInt(7, teachersEntity.getNumChildrens());
        preparedStatement.executeUpdate();
    }

    public ArrayList<TeachersEntity> chooseTeachers(String departmnent, String faculty, String category, String gender, int yearBD, int age, int numChildrens, int wages, String degree, Date dateWork1, Date dateWork2) throws SQLException{
        ArrayList<TeachersEntity> teachersEntities = new ArrayList<TeachersEntity>();
        String sql = "";
        if(!departmnent.equals("")){
            sql = "SELECT * FROM TEACHERS WHERE ID_KAF IN (SELECT ID_KAF FROM DEPARTMENT WHERE TITLE = '" + departmnent + "'";
            if (faculty_id != 0){
                sql += " AND ID_FA = " + String.valueOf(faculty_id);
            }
            sql += ")";
        }
        if(!faculty.equals("")){
            sql = "SELECT * FROM TEACHERS WHERE ID_KAF IN (SELECT ID_KAF FROM DEPARTMENT WHERE ID_FA IN (SELECT ID_FA FROM FACULTETS WHERE NAME = '" + faculty + "'))";
        }
        if(!category.equals("")){
            sql = "SELECT * FROM TEACHERS WHERE CATEGORY = '" + category + "'";
            if (faculty_id != 0){
                sql += " AND ID_KAF IN (SELECT ID_KAF FROM DEPARTMENT WHERE ID_FA = " + String.valueOf(faculty_id) + ")";
            }
        }
        if(!gender.equals("")){
            sql = "SELECT * FROM TEACHERS WHERE GENDER= '" + gender + "'";
            if (faculty_id != 0){
                sql += " AND ID_KAF IN (SELECT ID_KAF FROM DEPARTMENT WHERE ID_FA = " + String.valueOf(faculty_id) + ")";
            }
        }
        if(yearBD != 0){
            sql = "SELECT * FROM TEACHERS WHERE (EXTRACT(year FROM BIRTHDATE) = " + String.valueOf(yearBD) + ")";
            if (faculty_id != 0){
                sql += " AND ID_KAF IN (SELECT ID_KAF FROM DEPARTMENT WHERE ID_FA = " + String.valueOf(faculty_id) + ")";
            }
        }
        if(age != 0){
            sql = "SELECT *  FROM TEACHERS WHERE TRUNC(months_between(sysdate, BIRTHDATE) / 12) = " +  String.valueOf(age);
            if (faculty_id != 0){
                sql += " AND ID_KAF IN (SELECT ID_KAF FROM DEPARTMENT WHERE ID_FA = " + String.valueOf(faculty_id) + ")";
            }
        }
        if(numChildrens != -1){
            sql = "SELECT * FROM TEACHERS WHERE NUMCHILDRENS =" + String.valueOf(numChildrens);
            if (faculty_id != 0){
                sql += " AND ID_KAF IN (SELECT ID_KAF FROM DEPARTMENT WHERE ID_FA = " + String.valueOf(faculty_id) + ")";
            }
        }
        if(wages != 0){
            sql = "SELECT * FROM TEACHERS WHERE ID_JOB IN (SELECT ID_JOB FROM JOB WHERE WAGES >= " + String.valueOf(wages) + ")";
            if (faculty_id != 0){
                sql += " AND ID_KAF IN (SELECT ID_KAF FROM DEPARTMENT WHERE ID_FA = " + String.valueOf(faculty_id) + ")";
            }
        }
        if(!degree.equals("")){
            sql = "SELECT * FROM TEACHERS WHERE ID_TCH IN (SELECT ID_TCH FROM SCIENTIFIC_DEGREE_TCH WHERE TITLEDEGREE = '" + degree + "')";
            if (faculty_id != 0){
                sql += " AND ID_KAF IN (SELECT ID_KAF FROM DEPARTMENT WHERE ID_FA = " + String.valueOf(faculty_id) + ")";
            }
        }
        if(!(String.valueOf(dateWork1).equals(""))&& !(String.valueOf(dateWork2).equals("")) &&(dateWork1 != null)){
            sql = "SELECT * FROM TEACHERS WHERE ID_TCH IN (SELECT ID_TCH FROM SCIENTIFIC_WORK WHERE DATEWORK BETWEEN TO_DATE('"+ dateWork1 +  "', 'YYYY-MM-DD') AND TO_DATE('"+ dateWork2 +  "', 'YYYY-MM-DD'))";
            if (faculty_id != 0){
                sql += " AND ID_KAF IN (SELECT ID_KAF FROM DEPARTMENT WHERE ID_FA = " + String.valueOf(faculty_id) + ")";
            }
        }
        PreparedStatement preStatement = conn.prepareStatement(sql);
        ResultSet result = preStatement.executeQuery();
        while (result.next()) {
            TeachersEntity teachersEntity = new TeachersEntity();
            teachersEntity.setAll(result.getInt(1), result.getString(2), result.getInt(3), result.getInt(4), result.getString(5), result.getString(6), result.getDate(7), result.getInt(8));
            teachersEntities.add(teachersEntity);
        }
        return teachersEntities;
    }

    public ArrayList<TeachersEntity> getAddition(String titleSub, int num_gr, int course, String faculty, int semester, String occupation, int yearSub, String department) throws SQLException{
        ArrayList<TeachersEntity> teachersEntities = new ArrayList<TeachersEntity>();
        String sql = "";
        if((num_gr != 0) && !(titleSub.equals("")) && (semester == 0)){
            sql = "SELECT * FROM TEACHERS WHERE ID_TCH IN (SELECT ID_TCH FROM TRAINING_ASSIGNMENT WHERE ID_T IN (  SELECT ID_T FROM CURRICULUM WHERE (NUM_GR = " + String.valueOf(num_gr) + ")  AND (ID_SUB IN (SELECT ID_SUB FROM SUBJECT WHERE TITLE= '" + titleSub + "') )) )";
        }
        if((course != 0) && !(titleSub.equals(""))&&!(faculty.equals(""))){
            sql = "SELECT * FROM TEACHERS WHERE ID_TCH IN (SELECT ID_TCH FROM TRAINING_ASSIGNMENT WHERE ID_T IN (  SELECT ID_T FROM CURRICULUM WHERE (COURSE= " + String.valueOf(course) + ") AND (ID_FA IN (SELECT ID_FA FROM FACULTETS WHERE NAME = '" + faculty + "' )) AND (ID_SUB IN (SELECT ID_SUB FROM SUBJECT WHERE TITLE= '" + titleSub + "') ) ))";
        }
        if((num_gr != 0) && !(occupation.equals("")) && (semester != 0)){
            sql = "SELECT * FROM TEACHERS WHERE ID_TCH IN (SELECT ID_TCH FROM TRAINING_ASSIGNMENT WHERE ID_T IN (  SELECT ID_T FROM CURRICULUM WHERE (SEMESTER = " + String.valueOf(semester) + ") AND (NUM_GR = " + String.valueOf(num_gr) + ") AND (ID_SUB IN (SELECT ID_SUB FROM SUBJECT WHERE OCCUPATION= '" + occupation + "' ))))";
        }
        if((yearSub != 0) && !(occupation.equals("")) && (num_gr != 0)){
            sql = "SELECT * FROM TEACHERS WHERE ID_TCH IN (SELECT ID_TCH FROM TRAINING_ASSIGNMENT WHERE ID_T IN (  SELECT ID_T FROM CURRICULUM WHERE (YEAR= " + String.valueOf(yearSub) + ") AND (NUM_GR = " + String.valueOf(num_gr) + ") AND (ID_SUB IN (SELECT ID_SUB FROM SUBJECT WHERE OCCUPATION= '" + occupation + "'))))";
        }
        if((yearSub != 0) && !(occupation.equals("")) && (course != 0)&& !(faculty.equals(""))){
            sql = "SELECT * FROM TEACHERS WHERE ID_TCH IN (SELECT ID_TCH FROM TRAINING_ASSIGNMENT WHERE ID_T IN (  SELECT ID_T FROM CURRICULUM WHERE (YEAR = " + String.valueOf(yearSub) + ") AND (COURSE= " + String.valueOf(course) + ") AND (ID_SUB IN (SELECT ID_SUB FROM SUBJECT WHERE OCCUPATION= '" + occupation + "')) AND (ID_FA IN (SELECT ID_FA FROM FACULTETS WHERE NAME = '" + faculty + "'))))";
        }
        if((semester != 0) && !(occupation.equals("")) && (course != 0)&& !(faculty.equals(""))){
            sql = "SELECT * FROM TEACHERS WHERE ID_TCH IN (SELECT ID_TCH FROM TRAINING_ASSIGNMENT WHERE ID_T IN (  SELECT ID_T FROM CURRICULUM WHERE (SEMESTER = " + String.valueOf(semester) + ") AND (COURSE = " + String.valueOf(course) + ") AND (ID_SUB IN (SELECT ID_SUB FROM SUBJECT WHERE OCCUPATION= '" + occupation + "')) AND (ID_FA IN (SELECT ID_FA FROM FACULTETS WHERE NAME = '" + faculty + "'))))";
        }
        //*****************************
        if((num_gr != 0) && (!titleSub.equals("")) && (semester != 0)){
            sql = "SELECT * FROM TEACHERS  WHERE ID_TCH IN (SELECT ID_TCH FROM TESTS WHERE (NUM_GR = " + String.valueOf(num_gr) + ") AND ID_T IN (  SELECT ID_T FROM CURRICULUM WHERE (SEMESTER = " + String.valueOf(semester) + ") AND (ID_SUB IN (SELECT ID_SUB FROM SUBJECT WHERE TITLE= '" + titleSub + "'))))";
        }
        if(!(department.equals(""))){
            sql = "SELECT * FROM TEACHERS WHERE ID_TCH IN (SELECT ID_TCH FROM VKRS WHERE ID_TCH IN ( SELECT ID_TCH FROM TEACHERS OUTER JOIN DEPARTMENT USING (ID_KAF) WHERE DEPARTMENT.TITLE = '" + department + "'))";
        }
        if(!(faculty.equals("")) && (occupation.equals(""))){
            sql = "SELECT * FROM TEACHERS WHERE ID_TCH IN (SELECT ID_TCH FROM VKRS OUTER JOIN TEACHERS USING(ID_TCH) WHERE TEACHERS.ID_KAF IN ( SELECT ID_KAF FROM DEPARTMENT OUTER JOIN FACULTETS USING(ID_FA) WHERE (FACULTETS.NAME= '" + faculty + "')))";
        }
        PreparedStatement preStatement = conn.prepareStatement(sql);
        ResultSet result = preStatement.executeQuery();
        while (result.next()) {
            TeachersEntity teachersEntity = new TeachersEntity();
            teachersEntity.setAll(result.getInt(1), result.getString(2), result.getInt(3), result.getInt(4), result.getString(5), result.getString(6), result.getDate(7), result.getInt(8));
            teachersEntities.add(teachersEntity);
        }
        return teachersEntities;
    }

    public ArrayList<ScientificWorkEntity> getSwork(String department, String faculty) throws SQLException{
        String sql = "";
        if (!department.equals("")) {
            sql = "SELECT THEME, DATEWORK, TEACHERS.FIO FROM SCIENTIFIC_WORK OUTER JOIN TEACHERS USING(ID_TCH) WHERE TEACHERS.ID_KAF IN (SELECT ID_KAF FROM DEPARTMENT WHERE TITLE = '" + department + "')";
        }
        if (!faculty.equals("")) {
            sql = "SELECT THEME, DATEWORK, TEACHERS.FIO FROM SCIENTIFIC_WORK OUTER JOIN TEACHERS USING(ID_TCH) WHERE TEACHERS.ID_KAF IN (SELECT ID_KAF FROM DEPARTMENT OUTER JOIN FACULTETS USING(ID_FA) WHERE FACULTETS.NAME = '" + faculty + "')";
        }
        PreparedStatement preStatement = conn.prepareStatement(sql);
        ResultSet result = preStatement.executeQuery();
        ArrayList<ScientificWorkEntity> scientificWorkEntities = new ArrayList<ScientificWorkEntity>();
        while (result.next()) {
            ScientificWorkEntity scientificWorkEntity = new ScientificWorkEntity();
            scientificWorkEntity.setAll(0, result.getString(1), result.getDate(2), 0);
            scientificWorkEntities.add(scientificWorkEntity);
        }
        return scientificWorkEntities;
    }

    public ArrayList<String> getSworkTeachers(String department, String faculty) throws SQLException{
        String sql = "";
        if (!department.equals("")) {
            sql = "SELECT THEME, DATEWORK, TEACHERS.FIO FROM SCIENTIFIC_WORK OUTER JOIN TEACHERS USING(ID_TCH) WHERE TEACHERS.ID_KAF IN (SELECT ID_KAF FROM DEPARTMENT WHERE TITLE = '" + department + "')";
        }
        if (!faculty.equals("")) {
            sql = "SELECT THEME, DATEWORK, TEACHERS.FIO FROM SCIENTIFIC_WORK OUTER JOIN TEACHERS USING(ID_TCH) WHERE TEACHERS.ID_KAF IN (SELECT ID_KAF FROM DEPARTMENT OUTER JOIN FACULTETS USING(ID_FA) WHERE FACULTETS.NAME = '" + faculty + "')";
        }
        PreparedStatement preStatement = conn.prepareStatement(sql);
        ResultSet result = preStatement.executeQuery();
        ArrayList<String> scientificWorkEntities = new ArrayList<String>();
        while (result.next()) {
            scientificWorkEntities.add(result.getString(3));
        }
        return scientificWorkEntities;
    }

    public ArrayList<TrainingAssignmentEntity> getTeachersLoad(String department, String fio) throws SQLException{
        String sql = "";
        if (!fio.equals("")) {
            sql = "SELECT FIO, TITLE, NUMBERHOURS FROM TEACHERS OUTER JOIN (SELECT ID_TCH, TITLE, NUMBERHOURS FROM SUBJECT OUTER JOIN TRAINING_ASSIGNMENT USING (ID_SUB)) USING (ID_TCH)  WHERE FIO ='" + fio + "'";
        }
        if (!department.equals("")) {
            sql = "SELECT FIO, TITLE, NUMBERHOURS FROM TEACHERS OUTER JOIN (SELECT ID_TCH, TITLE, NUMBERHOURS FROM SUBJECT OUTER JOIN TRAINING_ASSIGNMENT USING (ID_SUB)) USING (ID_TCH) WHERE ID_TCH IN (SELECT ID_TCH FROM TEACHERS WHERE ID_KAF IN (SELECT ID_KAF FROM DEPARTMENT WHERE TITLE = '" + department + "'))";
        }
        PreparedStatement preStatement = conn.prepareStatement(sql);
        ResultSet result = preStatement.executeQuery();
        ArrayList<TrainingAssignmentEntity> trainingAssignmentEntities = new ArrayList<TrainingAssignmentEntity>();
        while (result.next()) {
            TrainingAssignmentEntity trainingAssignmentEntity = new TrainingAssignmentEntity();
            trainingAssignmentEntity.setFIOtch(result.getString(1));
            trainingAssignmentEntity.setTitleSub(result.getString(2));
            trainingAssignmentEntities.add(trainingAssignmentEntity);
        }
        return trainingAssignmentEntities;
    }

    public ArrayList<Integer> getTeachersLoadNumHours(String department, String fio) throws SQLException{
        String sql = "";
        if (!fio.equals("")) {
            sql = "SELECT FIO, TITLE, NUMBERHOURS FROM TEACHERS OUTER JOIN (SELECT ID_TCH, TITLE, NUMBERHOURS FROM SUBJECT OUTER JOIN TRAINING_ASSIGNMENT USING (ID_SUB)) USING (ID_TCH)  WHERE FIO ='" + fio + "'";
        }
        if (!department.equals("")) {
            sql = "SELECT FIO, TITLE, NUMBERHOURS FROM TEACHERS OUTER JOIN (SELECT ID_TCH, TITLE, NUMBERHOURS FROM SUBJECT OUTER JOIN TRAINING_ASSIGNMENT USING (ID_SUB)) USING (ID_TCH) WHERE ID_TCH IN (SELECT ID_TCH FROM TEACHERS WHERE ID_KAF IN (SELECT ID_KAF FROM DEPARTMENT WHERE TITLE = '" + department + "'))";
        }
        PreparedStatement preStatement = conn.prepareStatement(sql);
        ResultSet result = preStatement.executeQuery();
        ArrayList<Integer> hours = new ArrayList<Integer>();
        while (result.next()) {
            hours.add(result.getInt(3));
        }
        return hours;
    }

    public int getTeachersLoadSumNumHours(String department, String fio) throws SQLException{
        String sql = "";
        if (!fio.equals("")) {
            sql = "SELECT SUM(SUBJECT.NUMBERHOURS) FROM TRAINING_ASSIGNMENT OUTER JOIN SUBJECT USING (ID_SUB) WHERE (ID_TCH IN (SELECT ID_TCH FROM TEACHERS WHERE FIO = '" + fio + "')) GROUP BY SUBJECT.NUMBERHOURS";
        }
        if (!department.equals("")) {
            sql = "SELECT SUM(SUBJECT.NUMBERHOURS) FROM TRAINING_ASSIGNMENT OUTER JOIN SUBJECT USING (ID_SUB) WHERE ID_TCH IN (SELECT ID_TCH FROM TEACHERS WHERE ID_KAF IN (SELECT ID_KAF FROM DEPARTMENT WHERE TITLE = '" + department + "')) GROUP BY SUBJECT.NUMBERHOURS";
        }
        PreparedStatement preStatement = conn.prepareStatement(sql);
        ResultSet result = preStatement.executeQuery();
        while (result.next()) {
           return result.getInt(1);
        }
        return 0;
    }

    public ArrayList<String> getFIOtch() throws SQLException {
        String sql = "SELECT FIO FROM teachers ";
        if (faculty_id != 0){
            sql += " WHERE ID_KAF IN (SELECT ID_KAF FROM DEPARTMENT WHERE ID_FA = " + String.valueOf(faculty_id) + ")";
        }
        PreparedStatement preStatement = conn.prepareStatement(sql);
        ResultSet result = preStatement.executeQuery();
        ArrayList<String> teachersFIO = new ArrayList<String>();
        while (result.next()) {
            teachersFIO.add(result.getString(1));
        }
        return teachersFIO;
    }

    public ArrayList<String> getDepartments() throws SQLException{
        String sql = "select TITLE from DEPARTMENT ";
        if (faculty_id != 0){
            sql += " WHERE ID_FA = " + String.valueOf(faculty_id) ;
        }
        PreparedStatement preStatement = conn.prepareStatement(sql);
        ResultSet result = preStatement.executeQuery();
        ArrayList<String> departments = new ArrayList<String>();
        while (result.next()) {
            departments.add(result.getString(1));
        }
        return departments;
    }

    public ArrayList<String> getJobs() throws SQLException{
        String sql = "select TITLEJOB from JOB";
        PreparedStatement preStatement = conn.prepareStatement(sql);
        ResultSet result = preStatement.executeQuery();
        ArrayList<String> jobs = new ArrayList<String>();
        while (result.next()) {
            jobs.add(result.getString(1));
        }
        return jobs;
    }

    public ArrayList<String> getFacultets() throws SQLException{
        String sql = "select NAME from FACULTETS";
        if (faculty_id != 0){
            sql += " WHERE ID_FA = " + String.valueOf(faculty_id) ;
        }
        PreparedStatement preStatement = conn.prepareStatement(sql);
        ResultSet result = preStatement.executeQuery();
        ArrayList<String> facultets = new ArrayList<String>();
        while (result.next()) {
            facultets.add(result.getString(1));
        }
        return facultets;
    }

    public ArrayList<String> getCategories() throws SQLException{
        String sql = "select CATEGORY from TEACHERS WHERE NOT CATEGORY = '-' GROUP BY CATEGORY ";
        //WHERE NOT CATEGORY = '-' ??
        PreparedStatement preStatement = conn.prepareStatement(sql);
        ResultSet result = preStatement.executeQuery();
        ArrayList<String> categories = new ArrayList<String>();
        while (result.next()) {
            categories.add(result.getString(1));
        }
        return categories;
    }

    public ArrayList<String> getSubjects() throws SQLException{
        String sql = "select TITLE from SUBJECT GROUP BY TITLE ";
        PreparedStatement preStatement = conn.prepareStatement(sql);
        ResultSet result = preStatement.executeQuery();
        ArrayList<String> subjects = new ArrayList<String>();
        while (result.next()) {
            subjects.add(result.getString(1));
        }
        return subjects;
    }

    public ArrayList<Integer> getGroups() throws SQLException{
        String sql = "select NUM_GR from GROUPS ";
        if (faculty_id != 0){
            sql += " WHERE ID_FA = " + String.valueOf(faculty_id) ;
        }
        PreparedStatement preStatement = conn.prepareStatement(sql);
        ResultSet result = preStatement.executeQuery();
        ArrayList<Integer> groups = new ArrayList<Integer>();
        while (result.next()) {
            groups.add(result.getInt(1));
        }
        return groups;
    }

    public ArrayList<String> getDegree() throws SQLException{
        String sql = "select TITLEDEGREE from SCIENTIFIC_DEGREE_TCH group by TITLEDEGREE";
        PreparedStatement preStatement = conn.prepareStatement(sql);
        ResultSet result = preStatement.executeQuery();
        ArrayList<String> degrees = new ArrayList<String>();
        while (result.next()) {
            degrees.add(result.getString(1));
        }
        return degrees;
    }

    public ArrayList<String> getOccupation() throws SQLException{
        String sql = "select OCCUPATION from SUBJECT group by OCCUPATION";
        PreparedStatement preStatement = conn.prepareStatement(sql);
        ResultSet result = preStatement.executeQuery();
        ArrayList<String> occupation = new ArrayList<String>();
        while (result.next()) {
            occupation.add(result.getString(1));
        }
        return occupation;
    }

    public void updateTch(String department, String name_job, int checkCh, String category, String fio, String answ) throws SQLException {
        String sql = "";
        if (answ.equals("1 ")) {
            int id_kaf;
            String sql1 = "SELECT ID_KAF FROM DEPARTMENT WHERE (TITLE = '" + department + "')";
            if (faculty_id != 0) {
                sql1 += " AND (ID_FA = " + String.valueOf(faculty_id) + ")";
            }
            PreparedStatement preStatement1 = conn.prepareStatement(sql1);
            ResultSet result1 = preStatement1.executeQuery();
            if (result1.next()) {
                id_kaf = result1.getInt(1);
            } else {
                throw new SQLException("(Изменение информации о преподавателе) Кафедра не найдена");
            }
            sql = "UPDATE TEACHERS SET ID_KAF = " + String.valueOf(id_kaf);
        }
        if (answ.equals("2 ")) {
            int id_job;
            String sql2 = "SELECT ID_JOB FROM JOB WHERE (TITLEJOB = '" + name_job + "')";
            PreparedStatement preStatement2 = conn.prepareStatement(sql2);
            ResultSet result2 = preStatement2.executeQuery();
            if (result2.next()) {
                id_job = result2.getInt(1);
            } else {
                throw new SQLException("(Изменение информации о преподавателе) Должность не найдена");
            }
            sql = "UPDATE TEACHERS SET ID_JOB = " + String.valueOf(id_job);
        }
        if (answ.equals("3 ")) {
            sql = "UPDATE TEACHERS SET CATEGORY = '" + category + "' ";
        }
        if (answ.equals("4 ")) {
            sql = "UPDATE TEACHERS SET NUMCHILDRENS = " + String.valueOf(checkCh);
        }
        if (answ.equals("1 2 3 4 ")) {
            int id_kaf;
            String sql1 = "SELECT ID_KAF FROM DEPARTMENT WHERE (TITLE = '" + department + "')";
            if (faculty_id != 0) {
                sql1 += " AND (ID_FA = " + String.valueOf(faculty_id) + ")";
            }
            PreparedStatement preStatement1 = conn.prepareStatement(sql1);
            ResultSet result1 = preStatement1.executeQuery();
            if (result1.next()) {
                id_kaf = result1.getInt(1);
            } else {
                throw new SQLException("(Изменение информации о преподавателе) Кафедра не найдена");
            }
                int id_job;
                String sql2 = "SELECT ID_JOB FROM JOB WHERE (TITLEJOB = '" + name_job + "')";
                PreparedStatement preStatement2 = conn.prepareStatement(sql2);
                ResultSet result2 = preStatement2.executeQuery();
                if (result2.next()) {
                    id_job = result2.getInt(1);
                } else {
                    throw new SQLException("(Изменение информации о преподавателе) Должность не найдена");
                }
                sql = "UPDATE TEACHERS SET ID_KAF = " + String.valueOf(id_kaf) + " ,ID_JOB = " + String.valueOf(id_job) + " ,CATEGORY = " + category + " , NUMCHILDRENS = " + String.valueOf(checkCh);
            }
            sql += " WHERE FIO = '" + fio + "'";
            PreparedStatement preparedStatement = conn.prepareStatement(sql);
            preparedStatement.executeUpdate();
    }

    public void setRole(int role) {
        this.role = role;
    }
}
