package org.example.dataBase.service;

import com.sun.org.apache.xml.internal.utils.StringToIntTable;
import org.example.dataBase.model.TrainingAssignmentEntity;
import org.example.dataBase.model.VkrsEntity;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class TrainingAssignmentService {
    private final Connection conn;
    private int faculty;
    private int role;

    public void setFaculty(int faculty1){
        faculty = faculty1;
    }

    public TrainingAssignmentService(Connection connection){
        conn = connection;
    }

    public ArrayList<TrainingAssignmentEntity> getAll() throws SQLException {
        String sql = "SELECT * FROM TRAINING_ASSIGNMENT ";
        if (faculty != 0){
            sql += " WHERE ID_T IN (SELECT ID_T FROM CURRICULUM WHERE ID_FA = " + String.valueOf(faculty) + ")";
        }
        PreparedStatement preStatement = conn.prepareStatement(sql);
        ResultSet result = preStatement.executeQuery();
        ArrayList<TrainingAssignmentEntity> trainingAssignmentEntities = new ArrayList<TrainingAssignmentEntity>();
        while (result.next()) {
            TrainingAssignmentEntity trainingAssignmentEntity = new TrainingAssignmentEntity();
            trainingAssignmentEntity.setAll(result.getInt(1), result.getInt(2),  result.getInt(3),result.getInt(3));
            String sql1 = "SELECT TITLE FROM SUBJECT WHERE (ID_SUB = " + String.valueOf(trainingAssignmentEntity.getID_sub()) + ")";
            PreparedStatement preStatement1 = conn.prepareStatement(sql1);
            ResultSet result1 = preStatement1.executeQuery();
            if (result1.next()){
                trainingAssignmentEntity.setTitleSub(result1.getString(1));
            }else{
                throw new SQLException("(Просмотр учебных поручений) Предмет не найден");
            }
            String sql2 = "SELECT FIO FROM TEACHERS WHERE (ID_TCH = " + String.valueOf(trainingAssignmentEntity.getID_tch()) + ")";
            PreparedStatement preStatement2 = conn.prepareStatement(sql2);
            ResultSet result2 = preStatement2.executeQuery();
            if (result2.next()){
                trainingAssignmentEntity.setFIOtch(result2.getString(1));
            }else {
                throw new SQLException("(Просмотр учебных поручений) Преподаватель не найден");
            }
            trainingAssignmentEntities.add(trainingAssignmentEntity);
         }
        return trainingAssignmentEntities;
    }

    public void addTrainingAssignment(TrainingAssignmentEntity trainingAssignmentEntity) throws SQLException{
        String sql = "INSERT INTO TRAINING_ASSIGNMENT(ID_SUB,  ID_TCH, ID_T)  VALUES(?,?,?)";
        String sql1 = "SELECT ID_TCH FROM TEACHERS WHERE (FIO = '" + trainingAssignmentEntity.getFIOtch() + "')";
        PreparedStatement preStatement1 = conn.prepareStatement(sql1);
        ResultSet result1 = preStatement1.executeQuery();
        if(result1.next()){
            trainingAssignmentEntity.setID_tch(result1.getInt(1));
        } else{
            throw new SQLException("(Добавление учебного поручения) Преподаватель не найден");
        }
        String sql2 = "SELECT ID_SUB FROM CURRICULUM WHERE (ID_T =" + trainingAssignmentEntity.getID_t() + ")";
        if (faculty != 0){
            sql += " AND ID_FA = " + String.valueOf(faculty) ;
        }
        PreparedStatement preStatement2 = conn.prepareStatement(sql2);
        ResultSet result2 = preStatement2.executeQuery();
        if(result2.next()){
            trainingAssignmentEntity.setID_sub(result2.getInt(1));
        } else{
            throw new SQLException("(Добавление учебного поручения) Предмет не найден");
        }
        PreparedStatement preparedStatement = conn.prepareStatement(sql);
        preparedStatement.setInt(1, trainingAssignmentEntity.getID_sub());
        preparedStatement.setInt(2, trainingAssignmentEntity.getID_tch());
        preparedStatement.setInt(3, trainingAssignmentEntity.getID_t());
        preparedStatement.executeUpdate();
    }

    public ArrayList<String> getAllSubjects() throws SQLException {
        String sql = "SELECT TITLE FROM SUBJECT ";
        PreparedStatement preStatement = conn.prepareStatement(sql);
        ResultSet result = preStatement.executeQuery();
        ArrayList<String> subjectEntities = new ArrayList<String>();
        while (result.next()) {
            subjectEntities.add(result.getString(1));
        }
        return subjectEntities;
    }

    public ArrayList<Integer> getCurriculumNum() throws SQLException {
        String sql = "SELECT ID_T FROM CURRICULUM ";
        if (faculty != 0){
            sql += " WHERE ID_FA = " + String.valueOf(faculty);
        }
        PreparedStatement preStatement = conn.prepareStatement(sql);
        ResultSet result = preStatement.executeQuery();
        ArrayList<Integer> subjectEntities = new ArrayList<Integer>();
        while (result.next()) {
            subjectEntities.add(result.getInt(1));
        }
        return subjectEntities;
    }

    public ArrayList<String> getFIOtch() throws SQLException {
        String sql = "SELECT FIO FROM teachers ";
        if (faculty != 0){
            sql += " WHERE ID_KAF IN (SELECT ID_KAF FROM DEPARTMENT CURRICULUM WHERE ID_FA = " + String.valueOf(faculty) + ")";
        }
        PreparedStatement preStatement = conn.prepareStatement(sql);
        ResultSet result = preStatement.executeQuery();
        ArrayList<String> teachersFIO = new ArrayList<String>();
        while (result.next()) {
            teachersFIO.add(result.getString(1));
        }
        return teachersFIO;
    }

    public void setRole(int role) {
        this.role = role;
    }
}
