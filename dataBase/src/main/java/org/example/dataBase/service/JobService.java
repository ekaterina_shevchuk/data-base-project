package org.example.dataBase.service;

import org.example.dataBase.model.JobEntity;
import org.example.dataBase.model.VkrsEntity;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class JobService {
    private final Connection conn;
    private int role;

    public JobService(Connection connection){
        conn = connection;
    }

    public ArrayList<JobEntity> getAll() throws SQLException {
        String sql = "SELECT * FROM job";
        PreparedStatement preStatement = conn.prepareStatement(sql);
        ResultSet result = preStatement.executeQuery();
        ArrayList<JobEntity> jobEntities = new ArrayList<JobEntity>();
        while (result.next()) {
            JobEntity jobEntity = new JobEntity();
            jobEntity.setAll(result.getInt(1), result.getString(2), result.getInt(3));
            jobEntities.add(jobEntity);
        }
        return jobEntities;
    }

    public void addJob(JobEntity jobEntity) throws SQLException{
        if (role != 2){
            throw new SQLException("Вы не можете добавить новую должность");
        }
        String sql = "INSERT INTO job(TITLEJOB, WAGES)  VALUES(?,?)";
        PreparedStatement preparedStatement = conn.prepareStatement(sql);
        preparedStatement.setString(1, jobEntity.getTitleJob());
        preparedStatement.setInt(2, jobEntity.getWages());
        preparedStatement.executeUpdate();
    }

    public void setRole(int role) {
        this.role = role;
    }
}
