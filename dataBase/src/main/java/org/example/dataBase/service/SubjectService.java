package org.example.dataBase.service;

import org.example.dataBase.model.ScientificWorkEntity;
import org.example.dataBase.model.SubjectEntity;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class SubjectService {
    private final Connection conn;
    private int faculty;
    private int role;

    public void setFaculty(int faculty1){
        faculty = faculty1;
    }

    public SubjectService(Connection connection){
        conn = connection;
    }

    public ArrayList<SubjectEntity> getAll() throws SQLException {
        String sql = "SELECT * FROM SUBJECT ";
        PreparedStatement preStatement = conn.prepareStatement(sql);
        ResultSet result = preStatement.executeQuery();
        ArrayList<SubjectEntity> subjectEntities = new ArrayList<SubjectEntity>();
        while (result.next()) {
            SubjectEntity subjectEntity = new SubjectEntity();
            subjectEntity.setAll(result.getInt(1), result.getString(2),result.getString(3),result.getInt(4),result.getString(5));
            subjectEntities.add(subjectEntity);
        }
        return subjectEntities;
    }

    public void addSubject(SubjectEntity subjectEntity) throws SQLException{
        String sql = "INSERT INTO SUBJECT(TITLE, FORMOFCONTROL, NUMBERHOURS, OCCUPATION) VALUES(?,?,?,?)";
        PreparedStatement preparedStatement = conn.prepareStatement(sql);
        preparedStatement.setString(1, subjectEntity.getTitle());
        preparedStatement.setString(2,subjectEntity.getFormOfControl());
        preparedStatement.setInt(3, subjectEntity.getNumberHours());
        preparedStatement.setString(4,subjectEntity.getOccupation());
        preparedStatement.executeUpdate();
    }

    public ArrayList<String> getSubjects() throws SQLException {
        String sql = "SELECT TITLE FROM SUBJECT ";
        PreparedStatement preStatement = conn.prepareStatement(sql);
        ResultSet result = preStatement.executeQuery();
        ArrayList<String> subjects = new ArrayList<String>();
        while (result.next()) {
            subjects.add(result.getString(1));
        }
        return subjects;
    }

    public void setRole(int role) {
        this.role = role;
    }
}
