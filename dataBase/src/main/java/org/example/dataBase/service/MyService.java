package org.example.dataBase.service;

import org.example.dataBase.model.TeachersEntity;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class MyService {
    private final Connection conn;
    private int faculty;
    private int role;

    public void setFaculty(int faculty1){
        faculty = faculty1;
    }

    public MyService(Connection connection){
        conn = connection;
    }

    public String checkLogin(int id)throws SQLException {
        String sql = "SELECT PASS FROM TEACHERS WHERE ID_TCH = " + String.valueOf(id);
        PreparedStatement preStatement = conn.prepareStatement(sql);
        ResultSet result = preStatement.executeQuery();

        if (result.next()) {
           return result.getString(1);
        }
        return "";
    }

    public int getFacultet(int id_tch) throws SQLException{
        String sql;
        if (role == 0) {
            sql = "SELECT ID_FA FROM DEPARTMENT WHERE ID_KAF IN (SELECT ID_KAF FROM TEACHERS WHERE ID_TCH = " + String.valueOf(id_tch) + ") ";
        } else {
            sql = "SELECT ID_FA FROM FACULTETS WHERE ID_DEAN = " + String.valueOf(id_tch);
        }
        PreparedStatement preStatement = conn.prepareStatement(sql);
        ResultSet result = preStatement.executeQuery();
        if (result.next()) {
            return result.getInt(1);
        }
        return 0;
    }

    public String checkRole(int id)throws SQLException {
        String sql = "SELECT TITLEJOB FROM JOB outer join TEACHERS USING(ID_JOB) WHERE TEACHERS.ID_TCH = " + String.valueOf(id);
        PreparedStatement preStatement = conn.prepareStatement(sql);
        ResultSet result = preStatement.executeQuery();
        if (result.next()) {
            return result.getString(1);
        }
        return "";
    }

    public void setRole(int role) {
        this.role = role;
    }
}
