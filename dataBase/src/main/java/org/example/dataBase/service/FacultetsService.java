package org.example.dataBase.service;

import org.example.dataBase.model.FacultetsEntity;

import java.sql.*;
import java.util.ArrayList;
import java.util.Locale;
import java.util.Properties;
import java.util.TimeZone;

public class FacultetsService  {
    private final Connection conn;
    private int faculty;
    private int role;

    public void setFaculty(int faculty1){
        faculty = faculty1;
    }

    public FacultetsService(Connection connection){
        conn = connection;
    }

    public ArrayList<FacultetsEntity> getAll() throws SQLException {
        String sql = "SELECT * FROM facultets";
        PreparedStatement preStatement = conn.prepareStatement(sql);
        ResultSet result = preStatement.executeQuery();
        ArrayList<FacultetsEntity> facultetsEntities = new ArrayList<FacultetsEntity>();
        while (result.next()) {
            FacultetsEntity facultetsEntity = new FacultetsEntity();
            facultetsEntity.setAll(result.getInt(1), result.getString(2), result.getInt(3));
            String sql2 = "SELECT FIO FROM TEACHERS WHERE (ID_TCH = " + String.valueOf(facultetsEntity.getID_dean()) + ")";
            PreparedStatement preStatement2 = conn.prepareStatement(sql2);
            ResultSet result2 = preStatement2.executeQuery();
            if(result2.next()){
                facultetsEntity.setFIOdean(result2.getString(1));
            }else {
                throw new SQLException("Декан не найден");
            }
            facultetsEntities.add(facultetsEntity);
        }
        return facultetsEntities;
    }

    public void addFaculty(FacultetsEntity facultetsEntity) throws SQLException{
        if (role != 2){
            throw new SQLException("Вы не можете добавить факультет");
        }
        String sql1 = "SELECT ID_TCH FROM TEACHERS WHERE (FIO = '" + facultetsEntity.getFIOdean() + "')";
        PreparedStatement preStatement1 = conn.prepareStatement(sql1);
        ResultSet result1 = preStatement1.executeQuery();
        if(result1.next()){
            facultetsEntity.setID_dean(result1.getInt(1));
        } else{
            throw new SQLException("Преподаватель не найден");
        }

        String sql = "INSERT INTO FACULTETS (NAME, ID_DEAN) VALUES(?,?)";
        PreparedStatement preparedStatement = conn.prepareStatement(sql);
        preparedStatement.setString(1, facultetsEntity.getName());
        preparedStatement.setInt(2, facultetsEntity.getID_dean());
        preparedStatement.executeUpdate();
        System.out.println(facultetsEntity.getID_fa() + " " + facultetsEntity.getName() + " " + facultetsEntity.getID_dean());
    }

    public ArrayList<String> getTitleFacultets() throws SQLException {
        String sql = "SELECT NAME FROM facultets ";
        if (faculty != 0){
            sql += "WHERE ID_FA =" + String.valueOf(faculty);
        }
        PreparedStatement preStatement = conn.prepareStatement(sql);
        ResultSet result = preStatement.executeQuery();
        ArrayList<String> facultets = new ArrayList<String>();
        while (result.next()) {
            facultets.add(result.getString(1));
        }
        return facultets;
    }

    public ArrayList<String> getDeans() throws SQLException {
        String sql = "SELECT FIO FROM TEACHERS WHERE ID_JOB IN (SELECT ID_JOB FROM JOB WHERE TITLEJOB = 'Декан')";
        PreparedStatement preStatement = conn.prepareStatement(sql);
        ResultSet result = preStatement.executeQuery();
        ArrayList<String> deans = new ArrayList<String>();
        while (result.next()) {
            deans.add(result.getString(1));
        }
        return deans;
    }

    public void setRole(int role) {
        this.role = role;
    }
}
