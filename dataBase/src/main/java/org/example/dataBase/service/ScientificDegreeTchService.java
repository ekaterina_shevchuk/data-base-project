package org.example.dataBase.service;

import org.example.dataBase.model.ScientificDegreeTchEntity;
import org.example.dataBase.model.ScientificWorkEntity;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class ScientificDegreeTchService {
    private final Connection conn;
    private int role;
    private int faculty;

    public ScientificDegreeTchService(Connection connection){
        conn = connection;
    }

    public ArrayList<ScientificDegreeTchEntity> getAll() throws SQLException {
        String sql = "SELECT ID_UCH,ID_TCH,TITLEDEGREE  FROM SCIENTIFIC_DEGREE_TCH ";
        if (faculty != 0){
            sql += "OUTER JOIN TEACHERS USING(ID_TCH) WHERE TEACHERS.ID_KAF IN (SELECT ID_KAF FROM DEPARTMENT WHERE ID_FA = " + String.valueOf(faculty) + ")";
        }
        PreparedStatement preStatement = conn.prepareStatement(sql);
        ResultSet result = preStatement.executeQuery();
        ArrayList<ScientificDegreeTchEntity> scientificDegreeTchEntities = new ArrayList<ScientificDegreeTchEntity>();
        while (result.next()) {
            ScientificDegreeTchEntity scientificDegreeTchEntity = new ScientificDegreeTchEntity();
            scientificDegreeTchEntity.setAll(result.getInt(1), result.getInt(2), result.getString(3));
            String sql1 = "SELECT FIO FROM TEACHERS WHERE (ID_TCH = " + String.valueOf(scientificDegreeTchEntity.getID_tch()) + ")";
            PreparedStatement preStatement1 = conn.prepareStatement(sql1);
            ResultSet result1 = preStatement1.executeQuery();
            if (result1.next()){
                scientificDegreeTchEntity.setFIOtch(result1.getString(1));
            }else {
                throw new SQLException("(Просмотр научных степеней) Преподаватель не найден");
            }
            scientificDegreeTchEntities.add(scientificDegreeTchEntity);
        }
        return scientificDegreeTchEntities;
    }

    public void addScientificWork(ScientificDegreeTchEntity scientificDegreeTchEntity) throws SQLException{
        String sql = "INSERT INTO SCIENTIFIC_DEGREE_TCH( ID_TCH, TITLEDEGREE) VALUES(?,?)";
        String sql1 = "SELECT ID_TCH FROM TEACHERS WHERE (FIO = '" + scientificDegreeTchEntity.getFIOtch() + "')";
        PreparedStatement preStatement1 = conn.prepareStatement(sql1);
        ResultSet result1 = preStatement1.executeQuery();
        if(result1.next()){
            scientificDegreeTchEntity.setID_tch(result1.getInt(1));
        } else{
            throw new SQLException("(Добавление научной степени) Преподаватель не найден");
        }
        PreparedStatement preparedStatement = conn.prepareStatement(sql);
        preparedStatement.setInt(1, scientificDegreeTchEntity.getID_tch());
        preparedStatement.setString(2,scientificDegreeTchEntity.getTitleDegree());
        preparedStatement.executeUpdate();
    }

    public ArrayList<String> getFIOtch() throws SQLException {
        String sql = "SELECT FIO FROM teachers ";
        if (faculty != 0){
            sql += "WHERE ID_KAF IN (SELECT ID_KAF FROM DEPARTMENT WHERE ID_FA = " + String.valueOf(faculty) + ")";
        }
        PreparedStatement preStatement = conn.prepareStatement(sql);
        ResultSet result = preStatement.executeQuery();
        ArrayList<String> teachersFIO = new ArrayList<String>();
        while (result.next()) {
            teachersFIO.add(result.getString(1));
        }
        return teachersFIO;
    }

    public void setRole(int role) {
        this.role = role;
    }

    public void setFaculty(int faculty) {
        this.faculty = faculty;
    }
}
