package org.example.dataBase.service;

import org.example.dataBase.model.GroupsEntity;
import org.example.dataBase.model.VkrsEntity;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class VkrsService {
    private final Connection conn;
    private int faculty;

    public void setFaculty(int faculty1){
        faculty = faculty1;
    }

    public VkrsService(Connection connection){
        conn = connection;
    }

    public ArrayList<VkrsEntity> getAll() throws SQLException {
        String sql = "SELECT * FROM vkrs " ;
        if (faculty != 0){
            sql += "  WHERE ID_TCH IN (SELECT ID_TCH FROM TEACHERS OUTER JOIN DEPARTMENT USING(ID_KAF) WHERE ID_FA = " + String.valueOf(faculty) + ")";
        }
        PreparedStatement preStatement = conn.prepareStatement(sql);
        ResultSet result = preStatement.executeQuery();
        ArrayList<VkrsEntity> vkrsEntities = new ArrayList<VkrsEntity>();
        while (result.next()) {
            VkrsEntity vkrsEntity = new VkrsEntity();
            vkrsEntity.setAll(result.getInt(1), result.getInt(2), result.getString(3), result.getInt(4));
            String sql1 = "SELECT FIO FROM TEACHERS WHERE (ID_TCH = " + String.valueOf(vkrsEntity.getID_tch()) + ")";
            PreparedStatement preStatement1 = conn.prepareStatement(sql1);
            ResultSet result1 = preStatement1.executeQuery();
            if (result1.next()){
                vkrsEntity.setFIOtch(result1.getString(1));
            }else{
                throw new SQLException("(Просмотр ВКР) Преподаватель не найден");
            }
            String sql2 = "SELECT FIO FROM STUDENTS WHERE (ID_ST = " + String.valueOf(vkrsEntity.getID_st()) + ")";
            PreparedStatement preStatement2 = conn.prepareStatement(sql2);
            ResultSet result2 = preStatement2.executeQuery();
            if(result2.next()) {
                vkrsEntity.setFIOStudent(result2.getString(1));
            }else{
                throw new SQLException("(Просмотр ВКР) Студент не найден");
            }
            vkrsEntities.add(vkrsEntity);
        }

        return vkrsEntities;
    }

    public void addVkr(VkrsEntity vkrsEntity) throws SQLException{
        String sql = "INSERT INTO vkrs(ID_ST, THEME, ID_TCH)  VALUES(?,?,?)";
        String sql1 = "SELECT ID_TCH FROM TEACHERS WHERE (FIO = '" + vkrsEntity.getFIOtch() + "')";
        PreparedStatement preStatement1 = conn.prepareStatement(sql1);
        ResultSet result1 = preStatement1.executeQuery();
        if(result1.next()){
            vkrsEntity.setID_tch(result1.getInt(1));
        } else{
            throw new SQLException("Преподаватель не найден");
        }
        String sql2 = "SELECT ID_ST FROM STUDENTS WHERE (FIO = '" + vkrsEntity.getFIOStudent() + "')";
        PreparedStatement preStatement2 = conn.prepareStatement(sql2);
        ResultSet result2 = preStatement2.executeQuery();
        if (result2.next()) {
            vkrsEntity.setID_st(result2.getInt(1));
        }else{
            throw new SQLException("Студент не найден");
        }
        PreparedStatement preparedStatement = conn.prepareStatement(sql);
        preparedStatement.setInt(1, vkrsEntity.getID_st());
        preparedStatement.setString(2, vkrsEntity.getTheme());
        preparedStatement.setInt(3, vkrsEntity.getID_tch());
        preparedStatement.executeUpdate();
    }

    public ArrayList<String> getFIOst() throws SQLException{
        String sql = "SELECT FIO FROM STUDENTS ";
        if (faculty != 0){
            sql += "  WHERE NUM_GR IN (SELECT NUM_GR FROM GROUPS WHERE ID_FA = " + String.valueOf(faculty) + ")";
        }
        PreparedStatement preStatement = conn.prepareStatement(sql);
        ResultSet result = preStatement.executeQuery();
        ArrayList<String> fioST = new ArrayList<String>();
        while (result.next()) {
            fioST.add(result.getString(1));
        }
        return fioST;
    }

    public ArrayList<VkrsEntity> getVKRS(String FIOtch, String kaf) throws SQLException{
        String sql = "";
        if(FIOtch.equals("")){
            sql = "SELECT STUDENTS.FIO, THEME FROM VKRS OUTER JOIN STUDENTS USING (ID_ST) WHERE (ID_TCH IN (SELECT ID_TCH FROM TEACHERS  WHERE (ID_KAF IN (SELECT ID_KAF FROM DEPARTMENT  WHERE TITLE = '" + kaf + "')) ))";
        }
        if(kaf.equals("")) {
            sql = "SELECT STUDENTS.FIO, THEME FROM VKRS OUTER JOIN STUDENTS USING (ID_ST) WHERE (ID_TCH IN (SELECT ID_TCH FROM TEACHERS WHERE (FIO = '" + FIOtch + "') ))";
        }
        PreparedStatement preStatement = conn.prepareStatement(sql);
        ResultSet result = preStatement.executeQuery();
        ArrayList<VkrsEntity> vkrsEntities = new ArrayList<VkrsEntity>();
        while (result.next()) {
            VkrsEntity vkrsEntity = new VkrsEntity();
            vkrsEntity.setAll(0, 0,  result.getString(2), 0);
            vkrsEntity.setFIOStudent(result.getString(1));
            vkrsEntities.add(vkrsEntity);
        }
        return vkrsEntities;
    }

    public ArrayList<String> getFIOtch() throws SQLException {
        String sql = "SELECT FIO FROM teachers ";
        if (faculty != 0){
            sql += "  OUTER JOIN DEPARTMENT USING(ID_KAF) WHERE ID_FA = " + String.valueOf(faculty) ;
        }
        PreparedStatement preStatement = conn.prepareStatement(sql);
        ResultSet result = preStatement.executeQuery();
        ArrayList<String> teachersFIO = new ArrayList<String>();
        while (result.next()) {
            teachersFIO.add(result.getString(1));
        }
        return teachersFIO;
    }

    public ArrayList<String> getDepartments() throws SQLException {
        String sql = "SELECT TITLE FROM DEPARTMENT ";
        if (faculty != 0){
            sql += " WHERE ID_FA = " + String.valueOf(faculty) ;
        }
        PreparedStatement preStatement = conn.prepareStatement(sql);
        ResultSet result = preStatement.executeQuery();
        ArrayList<String> teachersFIO = new ArrayList<String>();
        while (result.next()) {
            teachersFIO.add(result.getString(1));
        }
        return teachersFIO;
    }


}



