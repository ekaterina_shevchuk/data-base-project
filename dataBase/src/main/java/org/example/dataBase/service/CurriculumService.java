package org.example.dataBase.service;

import org.example.dataBase.model.CurriculumEntity;
import org.example.dataBase.model.StudentsEntity;
import org.example.dataBase.model.SubjectEntity;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class CurriculumService {
    private final Connection conn;
    private int faculty;

    public void setFaculty(int faculty1){
        faculty = faculty1;
    }

    public CurriculumService(Connection connection){
        conn = connection;
    }

    public ArrayList<CurriculumEntity> getAll() throws SQLException {
        String sql = "SELECT * FROM CURRICULUM " ;
        if (faculty != 0){
            sql += "WHERE ID_FA =" + String.valueOf(faculty);
        }
        PreparedStatement preStatement = conn.prepareStatement(sql);
        ResultSet result = preStatement.executeQuery();
        ArrayList<CurriculumEntity> curriculumEntities = new ArrayList<CurriculumEntity>();
        while (result.next()) {
            CurriculumEntity curriculumEntity = new CurriculumEntity();
            curriculumEntity.setAll(result.getInt(1), result.getInt(2), result.getInt(3), result.getInt(4), result.getInt(5), result.getInt(6), result.getInt(7));
            String sql1 = "SELECT TITLE FROM SUBJECT WHERE (ID_SUB = " + String.valueOf(curriculumEntity.getID_sub()) + ")";
            PreparedStatement preStatement1 = conn.prepareStatement(sql1);
            ResultSet result1 = preStatement1.executeQuery();
            if (result1.next()){
                curriculumEntity.setTitleSub(result1.getString(1));
            }else{
                throw new SQLException("(Просмотр учебных планов) Предмет не найден");
            }
            String sql2 = "SELECT NAME FROM FACULTETS WHERE (ID_FA = " + String.valueOf(curriculumEntity.getID_fa()) + ")";
            PreparedStatement preStatement2 = conn.prepareStatement(sql2);
            ResultSet result2 = preStatement2.executeQuery();
            if(result2.next()) {
                curriculumEntity.setName_fa(result2.getString(1));
            }else{
                throw new SQLException("(Просмотр учебных планов) Факультет не найден");
            }
            curriculumEntities.add(curriculumEntity);
        }
        return curriculumEntities;
    }

    public void addCurriculum(CurriculumEntity curriculumEntity) throws SQLException{
        if ((curriculumEntity.getID_fa() != faculty)&&(faculty != 0)) {
            throw new SQLException("Вы не можете добавлять учебный план для этого факультета");
        }
        String sql = "INSERT INTO CURRICULUM(ID_SUB, COURSE, NUM_GR, ID_FA, SEMESTER, YEAR)  VALUES(?,?,?,?,?,?)";
        String sql1 = "SELECT ID_SUB FROM SUBJECT WHERE (TITLE = '" + curriculumEntity.getTitleSub() + "')";
        PreparedStatement preStatement1 = conn.prepareStatement(sql1);
        ResultSet result1 = preStatement1.executeQuery();
        if(result1.next()){
            curriculumEntity.setID_sub(result1.getInt(1));
        } else{
            throw new SQLException("(Добавление учебных планов) Предмет не найден");
        }
        String sql2 = "SELECT ID_FA FROM FACULTETS WHERE (NAME = '" + curriculumEntity.getName_fa() + "')";
        PreparedStatement preStatement2 = conn.prepareStatement(sql2);
        ResultSet result2 = preStatement2.executeQuery();
        if (result2.next()) {
            curriculumEntity.setID_fa(result2.getInt(1));
        }else{
            throw new SQLException("(Добавление учебных планов) Факультет не найден");
        }
        PreparedStatement preparedStatement = conn.prepareStatement(sql);
        preparedStatement.setInt(1, curriculumEntity.getID_sub());
        preparedStatement.setInt(2, curriculumEntity.getCourse());
        preparedStatement.setInt(3, curriculumEntity.getNum_gr());
        preparedStatement.setInt(4, curriculumEntity.getID_fa());
        preparedStatement.setInt(5, curriculumEntity.getSemester());
        preparedStatement.setInt(6,curriculumEntity.getYear());
        preparedStatement.executeUpdate();
    }

    public ArrayList<Integer> getAllGroups() throws SQLException {
        String sql = "SELECT NUM_GR FROM GROUPS ";
        if (faculty != 0){
            sql += " WHERE ID_FA = " + String.valueOf(faculty);
        }
        PreparedStatement preStatement = conn.prepareStatement(sql);
        ResultSet result = preStatement.executeQuery();
        ArrayList<Integer> studentsEntities = new ArrayList<Integer>();
        while (result.next()) {
            studentsEntities.add(result.getInt(1));
        }
        return studentsEntities;
    }

    public ArrayList<String> getAllSubjectsLec() throws SQLException {
        String sql = "SELECT TITLE FROM SUBJECT WHERE OCCUPATION = 'Лекция'";
        PreparedStatement preStatement = conn.prepareStatement(sql);
        ResultSet result = preStatement.executeQuery();
        ArrayList<String> subjectEntities = new ArrayList<String>();
        while (result.next()) {
            subjectEntities.add(result.getString(1));
        }
        return subjectEntities;
    }

    public ArrayList<String> getAllSubjectsSem() throws SQLException {
        String sql = "SELECT TITLE FROM SUBJECT WHERE OCCUPATION = 'Семинар'";
        PreparedStatement preStatement = conn.prepareStatement(sql);
        ResultSet result = preStatement.executeQuery();
        ArrayList<String> subjectEntities = new ArrayList<String>();
        while (result.next()) {
            subjectEntities.add(result.getString(1));
        }
        return subjectEntities;
    }

    public ArrayList<String> getAllSubjectsLab() throws SQLException {
        String sql = "SELECT TITLE FROM SUBJECT WHERE OCCUPATION = 'Лабораторный практикум'";
        PreparedStatement preStatement = conn.prepareStatement(sql);
        ResultSet result = preStatement.executeQuery();
        ArrayList<String> subjectEntities = new ArrayList<String>();
        while (result.next()) {
            subjectEntities.add(result.getString(1));
        }
        return subjectEntities;
    }

    public ArrayList<String> getFaultets() throws SQLException {
        String sql = "SELECT name FROM FACULTETS ";
        if (faculty != 0){
            sql += " WHERE ID_FA = " + String.valueOf(faculty);
        }
        PreparedStatement preStatement = conn.prepareStatement(sql);
        ResultSet result = preStatement.executeQuery();
        ArrayList<String> facultets = new ArrayList<String>();
        while (result.next()) {
            facultets.add(result.getString(1));
        }
        return facultets;
    }

}
