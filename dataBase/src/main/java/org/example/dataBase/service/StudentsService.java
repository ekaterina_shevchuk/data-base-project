package org.example.dataBase.service;

import org.example.dataBase.model.FacultetsEntity;
import org.example.dataBase.model.StudentsEntity;

import java.sql.*;
import java.util.ArrayList;

public class StudentsService {
    private final Connection conn;
    private int faculty;
    private int role;

    public void setFaculty(int faculty1){
        faculty = faculty1;
    }

    public StudentsService(Connection connection){
        conn = connection;
    }

    public ArrayList<StudentsEntity> getAll() throws SQLException {
        String sql = "SELECT ID_ST, FIO, NUM_GR, GENDER, BIRTHDAY, CHECKCHILD, SCHOLARSHIP  FROM students ";
        if (faculty != 0){
            sql += "OUTER JOIN GROUPS USING(NUM_GR) WHERE ID_FA = " + String.valueOf(faculty);
        }
        sql += " ORDER BY 1";
        PreparedStatement preStatement = conn.prepareStatement(sql);
        ResultSet result = preStatement.executeQuery();
        ArrayList<StudentsEntity> studentsEntities = new ArrayList<StudentsEntity>();
        while (result.next()) {
            StudentsEntity studentsEntity = new StudentsEntity();
            studentsEntity.setAll(result.getInt(1), result.getString(2), result.getInt(3), result.getString(4), result.getDate(5), result.getInt(6), result.getInt(7));
            studentsEntities.add(studentsEntity);
        }
        return studentsEntities;
    }

    public ArrayList<Integer> getByGroups() throws SQLException {
        String sql = "SELECT NUM_GR FROM students ";
        if (faculty != 0){
            sql += " OUTER JOIN GROUPS USING(NUM_GR) WHERE ID_FA = " + String.valueOf(faculty);
        }
        sql += " GROUP BY NUM_GR";
        PreparedStatement preStatement = conn.prepareStatement(sql);
        ResultSet result = preStatement.executeQuery();
        ArrayList<Integer> studentsEntities = new ArrayList<Integer>();
        while (result.next()) {
            studentsEntities.add(result.getInt(1));
        }
        return studentsEntities;
    }

    public ArrayList<Integer> getAllGroups() throws SQLException {
        String sql = "SELECT NUM_GR FROM GROUPS";
        if (faculty != 0){
            sql += " WHERE ID_FA = " + String.valueOf(faculty);
        }
        PreparedStatement preStatement = conn.prepareStatement(sql);
        ResultSet result = preStatement.executeQuery();
        ArrayList<Integer> studentsEntities = new ArrayList<Integer>();
        while (result.next()) {
            studentsEntities.add(result.getInt(1));
        }
        return studentsEntities;
    }

    public void addStudent(StudentsEntity studentsEntity) throws SQLException{
        String sql = "INSERT INTO STUDENTS (FIO, NUM_GR, GENDER, BIRTHDAY, CHECKCHILD, SCHOLARSHIP)  VALUES(?,?,?,?,?,?)";
        PreparedStatement preparedStatement = conn.prepareStatement(sql);
        preparedStatement.setString(1, studentsEntity.getFIO());
        preparedStatement.setInt(2, studentsEntity.getNum_gr());
        preparedStatement.setString(3, studentsEntity.getGender());
        preparedStatement.setDate(4, studentsEntity.getBirthDay());
        preparedStatement.setInt(5, studentsEntity.getCheckChild());
        preparedStatement.setInt(6, studentsEntity.getScholarship());
        preparedStatement.executeUpdate();
    }

    public ArrayList<StudentsEntity> chooseStudents(int num_gr, String gender, int yearBD, int age, int checkChild, int scholarShip, int course, String faculty) throws SQLException{
        ArrayList<StudentsEntity> studentsEntities = new ArrayList<StudentsEntity>();
        String sql = "";
        if(num_gr != 0){
            sql = "SELECT * FROM STUDENTS WHERE (STUDENTS.NUM_GR =" + String.valueOf(num_gr) +")";
        }
        if(!gender.equals("")){
            sql = "SELECT * FROM STUDENTS WHERE (GENDER  = '" + gender +"')";
        }
        if(yearBD != 0){
            sql = "SELECT * FROM STUDENTS WHERE (EXTRACT(year FROM BIRTHDAY) = " + String.valueOf(yearBD) +")";
        }
        if(age != 0){
            sql = "SELECT * FROM STUDENTS WHERE ID_ST IN (SELECT ID_ST FROM STUDENTS WHERE TRUNC(months_between(sysdate, BIRTHDAY) / 12)= " + String.valueOf(age) +")";
        }
        if(checkChild != -1){
            sql = "SELECT * FROM STUDENTS WHERE (CHECKCHILD =  " + String.valueOf(checkChild) +")";
        }
        if(scholarShip != -1){
            sql = "SELECT * FROM STUDENTS WHERE (SCHOLARSHIP >   " + String.valueOf(scholarShip) +")";
        }
        if((course != 0) && !(faculty.equals(""))){
            sql = "SELECT * FROM STUDENTS INNER JOIN (SELECT NUM_GR FROM GROUPS OUTER JOIN FACULTETS USING(ID_FA) WHERE ((COURSE =" + String.valueOf(course) + " AND(FACULTETS.NAME=' " + faculty + "'))) USING (NUM_GR)  " + String.valueOf(scholarShip) +")";
        }
        PreparedStatement preStatement = conn.prepareStatement(sql);
        ResultSet result = preStatement.executeQuery();
        while (result.next()) {
            StudentsEntity studentsEntity = new StudentsEntity();
            studentsEntity.setAll(result.getInt(1), result.getString(2), result.getInt(3), result.getString(4), result.getDate(5), result.getInt(6), result.getInt(7));
            studentsEntities.add(studentsEntity);
        }
        return studentsEntities;
    }

    public ArrayList<StudentsEntity> getAddition(String FIOtch, String subjectTitle, int yearTest, int semester, int mark, int num_gr) throws SQLException{
        ArrayList<StudentsEntity> studentsEntities = new ArrayList<StudentsEntity>();
        String sql = "";
        if((num_gr != 0) && (mark != 0) && !(subjectTitle.equals(""))&&(yearTest == 0)){
            sql = "SELECT *  FROM STUDENTS INNER JOIN TESTS USING (ID_ST)  WHERE (TESTS.MARK = " + String.valueOf(mark) + ") and (STUDENTS.NUM_GR = " + String.valueOf(num_gr) + ") and (TESTS.ID_T IN (SELECT ID_T FROM CURRICULUM OUTER JOIN SUBJECT USING(ID_SUB) WHERE TITLE= '" + subjectTitle + "'))";
        }
        if(!(FIOtch.equals("")) && (semester != 0) && !(subjectTitle.equals("")) && (yearTest != 0)&&(mark != 0) ){
            sql = "SELECT * FROM STUDENTS WHERE ID_ST IN (SELECT ID_ST  FROM  TESTS OUTER JOIN (SELECT ID_T  FROM CURRICULUM WHERE (NUM_GR = " + String.valueOf(num_gr) + ") AND (SEMESTER = " + String.valueOf(semester) + ") AND (YEAR = " + String.valueOf(yearTest) + ") AND (ID_SUB IN (SELECT ID_SUB FROM SUBJECT WHERE TITLE = '" + subjectTitle + "')) ) USING (ID_T) WHERE (MARK = " + String.valueOf(mark) + ") AND ( ID_TCH IN (SELECT ID_TCH FROM TEACHERS WHERE FIO = '" + FIOtch + "')))\n";
        }
        /*if(!(FIOtch.equals("")) && !(subjectTitle.equals("")) && (num_gr != 0) &&(mark != 0)){
            sql = "SELECT * FROM STUDENTS WHERE ID_ST IN (SELECT ID_ST  FROM  TESTS OUTER JOIN (SELECT ID_T  FROM CURRICULUM WHERE (NUM_GR = " + String.valueOf(num_gr) + ") AND (YEAR = " + String.valueOf(yearTest) + ") AND (ID_SUB IN (SELECT ID_SUB FROM SUBJECT WHERE TITLE = '" + subjectTitle + "')) ) USING (ID_T) WHERE (MARK = " + String.valueOf(mark) + ") AND ( ID_TCH IN (SELECT ID_TCH FROM TEACHERS WHERE FIO = '" + FIOtch + "')))\n";
        }*/
        //*******************
        if((semester != 0) && (num_gr != 0) && (yearTest != 0)&&(mark != 0) && (FIOtch.equals(""))){
            sql = "SELECT * FROM STUDENTS WHERE ID_ST IN (SELECT ID_ST FROM TESTS OUTER JOIN (SELECT ID_T FROM CURRICULUM WHERE (NUM_GR =" + String.valueOf(num_gr) + ") AND (SEMESTER = " + String.valueOf(semester) + ") AND (YEAR =" + String.valueOf(yearTest) + ") ) USING (ID_T) WHERE (MARK >  " + String.valueOf(mark) + ") GROUP BY ID_ST )";
        }
        PreparedStatement preStatement = conn.prepareStatement(sql);
        ResultSet result = preStatement.executeQuery();
        while (result.next()) {
            StudentsEntity studentsEntity = new StudentsEntity();
            studentsEntity.setAll(result.getInt(1), result.getString(2), result.getInt(3), result.getString(4), result.getDate(5), result.getInt(6), result.getInt(7));
            studentsEntities.add(studentsEntity);
        }
        return studentsEntities;
    }

    public ArrayList<String> getSubjects() throws SQLException {
        String sql = "SELECT TITLE FROM SUBJECT GROUP BY TITLE";
        PreparedStatement preStatement = conn.prepareStatement(sql);
        ResultSet result = preStatement.executeQuery();
        ArrayList<String> subjects = new ArrayList<String>();
        while (result.next()) {
            subjects.add(result.getString(1));
        }
        return subjects;
    }

    public void updateSt(int group, int checkCh, int scholarShip, String fio) throws SQLException{
        String sql = "";
        if ((group != 0) && (checkCh == -1) && (scholarShip == -1)){
            sql = "UPDATE STUDENTS SET NUM_GR = " + String.valueOf(group) ;
        }
        if ((checkCh != -1)&& (group == 0) && (scholarShip == -1)){
            sql = "UPDATE STUDENTS SET CHECKCHILD = " + String.valueOf(checkCh) ;
        }
        if ((scholarShip != -1)&& (group == 0) && (checkCh == -1)){
            sql = "UPDATE STUDENTS SET SCHOLARSHIP = " + String.valueOf(scholarShip) ;
        }
        if ((group != 0) && (checkCh != -1) && (scholarShip == -1)){
            sql = "UPDATE STUDENTS SET NUM_GR = " + String.valueOf(group) + " , CHECKCHILD = " + String.valueOf(checkCh);
        }
        if ((group != 0) && (scholarShip != -1) && (checkCh == -1)){
            sql = "UPDATE STUDENTS SET NUM_GR = " + String.valueOf(group) + " , SCHOLARSHIP = " + String.valueOf(scholarShip);
        }
        if ((checkCh != -1) && (scholarShip != -1) && (group == 0)){
            sql = "UPDATE STUDENTS SET CHECKCHILD = " +  String.valueOf(checkCh) + " , SCHOLARSHIP = " + String.valueOf(scholarShip);
        }
        if ((checkCh != -1) && (scholarShip != -1) && (group != 0)){
            sql = "UPDATE STUDENTS SET CHECKCHILD = " +  String.valueOf(checkCh) + " , SCHOLARSHIP = " + String.valueOf(scholarShip) + " , NUM_GR = " + String.valueOf(group) ;
        }
        sql += " WHERE FIO = '" + fio + "'";
        PreparedStatement preparedStatement = conn.prepareStatement(sql);
        preparedStatement.executeUpdate();
    }

    public void setRole(int role) {
        this.role = role;
    }
}
