package org.example.dataBase.service;

import org.example.dataBase.model.DepartmentEntity;
import org.example.dataBase.model.FacultetsEntity;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class DepartmentService {
    private final Connection conn;
    private int faculty;

    public void setFaculty(int faculty1){
        faculty = faculty1;
    }

    public DepartmentService(Connection connection){
        conn = connection;
    }

    public ArrayList<DepartmentEntity> getAll() throws SQLException {
        String sql = "SELECT * FROM department ";
        if (faculty != 0){
            sql += "WHERE ID_FA = " + String.valueOf(faculty);
        }
        PreparedStatement preStatement = conn.prepareStatement(sql);
        ResultSet result = preStatement.executeQuery();
        ArrayList<DepartmentEntity> departmentEntities = new ArrayList<DepartmentEntity>();
        while (result.next()) {
            DepartmentEntity departmentEntity = new DepartmentEntity();
            departmentEntity.setAll(result.getInt(1), result.getString(2), result.getInt(3));
            String sql2 = "SELECT NAME FROM FACULTETS WHERE (ID_FA = " + String.valueOf(departmentEntity.getID_fa()) + ")";
            PreparedStatement preStatement2 = conn.prepareStatement(sql2);
            ResultSet result2 = preStatement2.executeQuery();
            if (result2.next()) {
                departmentEntity.setName_fa(result2.getString(1));
            }else{
                throw new SQLException("(Просмотр кафедр) Факультет не найден.");
            }
            departmentEntities.add(departmentEntity);
        }
        return departmentEntities;
    }

    public void addDepartment(DepartmentEntity departmentEntity) throws SQLException{
        if ((faculty != 0)&&(departmentEntity.getID_fa() != faculty)){
           throw new SQLException("Вы не можете добавлять кафедру для этого факультета");
        }
        String sql = "INSERT INTO Department VALUES(?,?,?)";
        String sql1 = "SELECT ID_FA FROM FACULTETS WHERE (NAME = '" + departmentEntity.getName_fa() + "')";
        PreparedStatement preStatement1 = conn.prepareStatement(sql1);
        ResultSet result1 = preStatement1.executeQuery();
        if(result1.next()){
            departmentEntity.setID_fa(result1.getInt(1));
        } else{
            throw new SQLException("(Добавление кафедры) Факультет не найден.");
        }
        PreparedStatement preparedStatement = conn.prepareStatement(sql);
        preparedStatement.setInt(1, departmentEntity.getID_kaf());
        preparedStatement.setString(2, departmentEntity.getTitle());
        preparedStatement.setInt(3, departmentEntity.getID_fa());
        preparedStatement.executeUpdate();
        System.out.println(departmentEntity.getID_kaf() + " " + departmentEntity.getTitle() + " " + departmentEntity.getID_fa());
    }
    public Connection getConn(){return conn;}

    public ArrayList<Integer> getByGroups() throws SQLException {
        String sql = "SELECT NUM_GR FROM GROUPS ";
        if (faculty != 0){
            sql += " WHERE ID_FA = " + String.valueOf(faculty);
        }
        PreparedStatement preStatement = conn.prepareStatement(sql);
        ResultSet result = preStatement.executeQuery();
        ArrayList<Integer> groups = new ArrayList<Integer>();
        while (result.next()) {
            groups.add(result.getInt(1));
        }
        return groups;
    }

    public ArrayList<String> getFaultets() throws SQLException {
        String sql = "SELECT name FROM FACULTETS ";
        if (faculty != 0){
            sql += " WHERE ID_FA = " + String.valueOf(faculty);
        }
        PreparedStatement preStatement = conn.prepareStatement(sql);
        ResultSet result = preStatement.executeQuery();
        ArrayList<String> facultets = new ArrayList<String>();
        while (result.next()) {
            facultets.add(result.getString(1));
        }
        return facultets;
    }

    public ArrayList<DepartmentEntity> getDepartmentsByParam(int num_gr, String facultet, int course, int yearTest, int semestr) throws SQLException{
        String sql = "";
        if ((num_gr != 0) && (semestr != 0)){
            sql = "SELECT * FROM DEPARTMENT WHERE (ID_FA = 1) AND (ID_KAF IN (SELECT ID_KAF FROM TEACHERS WHERE ID_TCH IN (SELECT ID_TCH FROM TRAINING_ASSIGNMENT WHERE (ID_T IN (SELECT ID_T FROM CURRICULUM WHERE (NUM_GR = "+ String.valueOf(num_gr) + ") AND(SEMESTER = " + String.valueOf(semestr) + "))))))";
        }
        if ((num_gr != 0) && (yearTest != 0)){
            sql = "SELECT * FROM DEPARTMENT WHERE (ID_FA = 1) AND (ID_KAF IN (SELECT ID_KAF FROM TEACHERS WHERE ID_TCH IN (SELECT ID_TCH FROM TRAINING_ASSIGNMENT WHERE (ID_T IN (SELECT ID_T FROM CURRICULUM WHERE (NUM_GR = "+ String.valueOf(num_gr) + ") AND(YEAR = " + String.valueOf(yearTest) + "))))))";
        }
        if ((course != 0) && (semestr != 0)&&(!facultet.equals(""))){
            sql = "SELECT * FROM DEPARTMENT WHERE (ID_FA = 1) AND (ID_KAF IN (SELECT ID_KAF FROM TEACHERS WHERE ID_TCH IN (SELECT ID_TCH FROM TRAINING_ASSIGNMENT WHERE (ID_T IN (SELECT ID_T FROM CURRICULUM WHERE (COURSE= " + String.valueOf(course) + ") AND(SEMESTER= " + String.valueOf(semestr) + ")))))) AND (ID_FA IN (SELECT ID_FA FROM FACULTETS WHERE NAME = '" + facultet + "'))";
        }
        if ((course != 0) &&(!facultet.equals(""))&& (yearTest != 0)){
            sql = "SELECT * FROM DEPARTMENT WHERE (ID_FA = 1) AND (ID_KAF IN (SELECT ID_KAF FROM TEACHERS WHERE ID_TCH IN (SELECT ID_TCH FROM TRAINING_ASSIGNMENT WHERE (ID_T IN (SELECT ID_T FROM CURRICULUM WHERE (COURSE= " + String.valueOf(course) + ") AND(YEAR= " + String.valueOf(yearTest) + ")))))) AND (ID_FA IN (SELECT ID_FA FROM FACULTETS WHERE NAME = '" + facultet + "'))";
        }
        PreparedStatement preStatement = conn.prepareStatement(sql);
        ResultSet result = preStatement.executeQuery();
        ArrayList<DepartmentEntity> departmentEntities = new ArrayList<DepartmentEntity>();
        while (result.next()) {
            DepartmentEntity departmentEntity = new DepartmentEntity();
            departmentEntity.setAll(result.getInt(1), result.getString(2), result.getInt(3));
            departmentEntities.add(departmentEntity);
        }
        return departmentEntities;
    }

}
