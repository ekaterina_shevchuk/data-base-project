package org.example.dataBase.service;

import org.example.dataBase.model.CurriculumEntity;
import org.example.dataBase.model.TestsEntity;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class TestsService {
    private final Connection conn;
    private int faculty;

    public void setFaculty(int faculty1){
        faculty = faculty1;
    }

    public TestsService(Connection connection){
        conn = connection;
    }

    public ArrayList<TestsEntity> getAll() throws SQLException {
        String sql = "SELECT * FROM TESTS ";
        if (faculty != 0){
            sql += " WHERE ID_T IN (SELECT ID_T FROM CURRICULUM WHERE ID_FA = " + String.valueOf(faculty) + ")";
        }
        PreparedStatement preStatement = conn.prepareStatement(sql);
        ResultSet result = preStatement.executeQuery();
        ArrayList<TestsEntity> testsEntities = new ArrayList<TestsEntity>();
        while (result.next()) {
            TestsEntity testsEntity = new TestsEntity();
            testsEntity.setAll(result.getInt(1), result.getInt(2), result.getInt(3), result.getInt(4), result.getDate(5), result.getInt(6));
            String sql1 = "SELECT FIO FROM TEACHERS WHERE (ID_TCH = " + String.valueOf(testsEntity.getID_tch()) + ")";
            PreparedStatement preStatement1 = conn.prepareStatement(sql1);
            ResultSet result1 = preStatement1.executeQuery();
            if (result1.next()){
                testsEntity.setFIOtch(result1.getString(1));
            }else {
                throw new SQLException("(Просмотр экзаменов/зачетов) Преподаватель не найден");
            }
            String sql2 = "SELECT FIO FROM STUDENTS WHERE (ID_ST = " + String.valueOf(testsEntity.getID_st()) + ")";
            PreparedStatement preStatement2 = conn.prepareStatement(sql2);
            ResultSet result2 = preStatement2.executeQuery();
            if (result2.next()){
                testsEntity.setFIOst(result2.getString(1));
            }else {
                throw new SQLException("(Просмотр экзаменов/зачетов) Преподаватель не найден");
            }
            String sql3 = "SELECT TITLE FROM SUBJECT OUTER JOIN CURRICULUM USING(ID_SUB) WHERE ID_T = " + String.valueOf(testsEntity.getID_t()) ;
            PreparedStatement preStatement3 = conn.prepareStatement(sql3);
            ResultSet result3 = preStatement3.executeQuery();
            if (result3.next()){
                testsEntity.setSubject(result3.getString(1));
            }else {
                throw new SQLException("(Просмотр экзаменов/зачетов) Преподаватель не найден");
            }
            testsEntities.add(testsEntity);
        }
        return testsEntities;
    }

    public void addTests(TestsEntity testsEntity) throws SQLException{
        String sql = "INSERT INTO TESTS(ID_ST, ID_TCH, MARK, DATETEST, ID_T) VALUES(?,?,?,?,?)";
        String sql1 = "SELECT ID_TCH FROM TEACHERS WHERE (FIO = '" + testsEntity.getFIOtch() + "')";
        PreparedStatement preStatement1 = conn.prepareStatement(sql1);
        ResultSet result1 = preStatement1.executeQuery();
        if(result1.next()){
            testsEntity.setID_tch(result1.getInt(1));
        } else{
            throw new SQLException("(Добавление экзамена/зачета) Преподаватель не найден");
        }
        String sql2 = "SELECT ID_ST FROM STUDENTS WHERE (FIO = '" + testsEntity.getFIOst() + "')";
        PreparedStatement preStatement2 = conn.prepareStatement(sql2);
        ResultSet result2 = preStatement2.executeQuery();
        if(result2.next()){
            testsEntity.setID_st(result2.getInt(1));
        } else{
            throw new SQLException("(Добавление экзамена/зачета) Студент не найден");
        }
        PreparedStatement preparedStatement = conn.prepareStatement(sql);
        preparedStatement.setInt(1, testsEntity.getID_st());
        preparedStatement.setInt(2, testsEntity.getID_tch());
        preparedStatement.setInt(3, testsEntity.getMark());
        preparedStatement.setDate(4, testsEntity.getDateTest());
        preparedStatement.setInt(5, testsEntity.getID_t());
        preparedStatement.executeUpdate();
    }

    public ArrayList<String> getAllSubjectsLec() throws SQLException {
        String sql = "SELECT TITLE FROM SUBJECT WHERE OCCUPATION = 'Лекция'";
        PreparedStatement preStatement = conn.prepareStatement(sql);
        ResultSet result = preStatement.executeQuery();
        ArrayList<String> subjectEntities = new ArrayList<String>();
        while (result.next()) {
            subjectEntities.add(result.getString(1));
        }
        return subjectEntities;
    }

    public ArrayList<String> getAllSubjectsSem() throws SQLException {
        String sql = "SELECT TITLE FROM SUBJECT WHERE OCCUPATION = 'Семинар'";
        PreparedStatement preStatement = conn.prepareStatement(sql);
        ResultSet result = preStatement.executeQuery();
        ArrayList<String> subjectEntities = new ArrayList<String>();
        while (result.next()) {
            subjectEntities.add(result.getString(1));
        }
        return subjectEntities;
    }

    public ArrayList<String> getAllSubjectsLab() throws SQLException {
        String sql = "SELECT TITLE FROM SUBJECT WHERE OCCUPATION = 'Лабораторный практикум'";
        PreparedStatement preStatement = conn.prepareStatement(sql);
        ResultSet result = preStatement.executeQuery();
        ArrayList<String> subjectEntities = new ArrayList<String>();
        while (result.next()) {
            subjectEntities.add(result.getString(1));
        }
        return subjectEntities;
    }

    public ArrayList<Integer> getAllGroups() throws SQLException {

        String sql = "SELECT NUM_GR FROM GROUPS ";
        if (faculty != 0){
            sql += " WHERE ID_FA = " + String.valueOf(faculty) ;
        }
        PreparedStatement preStatement = conn.prepareStatement(sql);
        ResultSet result = preStatement.executeQuery();
        ArrayList<Integer> studentsEntities = new ArrayList<Integer>();
        while (result.next()) {
            studentsEntities.add(result.getInt(1));
        }
        return studentsEntities;
    }

    public ArrayList<String> getFIOst() throws SQLException {
        String sql = "SELECT FIO FROM STUDENTS ";
        if (faculty != 0){
            sql += " OUTER JOIN GROUPS USING(NUM_GR) WHERE ID_FA = " + String.valueOf(faculty) ;
        }
        //String sql = "SELECT FIO FROM STUDENTS WHERE NUM_GR = " + String.valueOf(group) ;
        PreparedStatement preStatement = conn.prepareStatement(sql);
        ResultSet result = preStatement.executeQuery();
        ArrayList<String> teachersFIO = new ArrayList<String>();
        while (result.next()) {
            teachersFIO.add(result.getString(1));
        }
        return teachersFIO;
    }

    public ArrayList<String> getFIOtch() throws SQLException {
        String sql = "SELECT FIO FROM teachers ";
        if (faculty != 0){
            sql += " WHERE ID_KAF IN (SELECT ID_KAF FROM DEPARTMENT WHERE ID_FA = " + String.valueOf(faculty) + ")";
        }
        //String sql = "SELECT FIO FROM teachers OUTER JOIN TRAINING_ASSIGNMENT USING(ID_TCH) WHERE TRAINING_ASSIGNMENT.ID_T IN (SELECT ID_T FROM CURRICULUM WHERE NUM_GR IN (SELECT NUM_GR FROM GROUPS WHERE ID_FA IN (SELECT ID_FA FROM GROUPS WHERE NUM_GR = " + String.valueOf(group) + "))) GROUP BY FIO";
        PreparedStatement preStatement = conn.prepareStatement(sql);
        ResultSet result = preStatement.executeQuery();
        ArrayList<String> teachersFIO = new ArrayList<String>();
        while (result.next()) {
            teachersFIO.add(result.getString(1));
        }
        return teachersFIO;
    }

    public ArrayList<Integer> getCurriculumNum() throws SQLException {
        String sql = "SELECT ID_T FROM CURRICULUM ";
        if (faculty != 0){
            sql += " WHERE ID_FA = " + String.valueOf(faculty);
        }
        PreparedStatement preStatement = conn.prepareStatement(sql);
        ResultSet result = preStatement.executeQuery();
        ArrayList<Integer> subjectEntities = new ArrayList<Integer>();
        while (result.next()) {
            subjectEntities.add(result.getInt(1));
        }
        return subjectEntities;
    }


}
