package org.example.dataBase.service;

import org.example.dataBase.model.FacultetsEntity;
import org.example.dataBase.model.GroupsEntity;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class GroupsService {
    private final Connection conn;
    private int role;
    private int faculty;

    public void setFaculty(int faculty1){
        faculty = faculty1;
    }

    public GroupsService(Connection connection){
        conn = connection;
    }

    public ArrayList<GroupsEntity> getAll() throws SQLException {
        String sql = "SELECT * FROM groups ";
        if (faculty != 0){
            sql += "WHERE ID_FA =" + String.valueOf(faculty);
        }
        PreparedStatement preStatement = conn.prepareStatement(sql);
        ResultSet result = preStatement.executeQuery();
        ArrayList<GroupsEntity> groupsEntities = new ArrayList<GroupsEntity>();
        while (result.next()) {
            GroupsEntity groupsEntity = new GroupsEntity();
            groupsEntity.setAll(result.getInt(1), result.getInt(2), result.getInt(3));
            String sql2 = "SELECT NAME FROM FACULTETS WHERE (ID_FA = " + String.valueOf(groupsEntity.getID_fa()) + ")";
            PreparedStatement preStatement2 = conn.prepareStatement(sql2);
            ResultSet result2 = preStatement2.executeQuery();
            result2.next();
            groupsEntity.setName_fa(result2.getString(1));
            groupsEntities.add(groupsEntity);
        }
        return groupsEntities;
    }

    public void addGroup(GroupsEntity groupsEntity) throws SQLException{
        String sql = "INSERT INTO GROUPS VALUES(?,?,?)";
        String sql1 = "SELECT ID_FA FROM FACULTETS WHERE (NAME = '" + groupsEntity.getName_fa() + "')";
        PreparedStatement preStatement1 = conn.prepareStatement(sql1);
        ResultSet result1 = preStatement1.executeQuery();
        if(result1.next()){
            groupsEntity.setID_fa(result1.getInt(1));
        } else{
            throw new SQLException("Факультет не найден");
        }
        if ((faculty != 0)&&(groupsEntity.getID_fa() != faculty)){
            throw new SQLException("Вы не можете добавить группу (проверьте факультет)");
        }
        PreparedStatement preparedStatement = conn.prepareStatement(sql);
        preparedStatement.setInt(1, groupsEntity.getNum_gr());
        preparedStatement.setInt(2, groupsEntity.getCourse());
        preparedStatement.setInt(3, groupsEntity.getID_fa());
        preparedStatement.executeUpdate();
    }

    public ArrayList<String> getFaultets() throws SQLException {
        String sql = "SELECT name FROM FACULTETS ";
        if (faculty != 0){
            sql += " WHERE ID_FA =" + String.valueOf(faculty);
        }
        PreparedStatement preStatement = conn.prepareStatement(sql);
        ResultSet result = preStatement.executeQuery();
        ArrayList<String> facultets = new ArrayList<String>();
        while (result.next()) {
            facultets.add(result.getString(1));
        }
        return facultets;
    }

    public void setRole(int role) {
        this.role = role;
    }
}
