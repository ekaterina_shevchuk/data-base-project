package org.example.dataBase.service;

import org.example.dataBase.model.GroupsEntity;
import org.example.dataBase.model.ScientificWorkEntity;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class ScientificWorkService {
    private final Connection conn;
    private int faculty;
    private int role;

    public void setFaculty(int faculty1){
        faculty = faculty1;
    }

    public ScientificWorkService(Connection connection){
        conn = connection;
    }

    public ArrayList<ScientificWorkEntity> getAll() throws SQLException {
        String sql = "SELECT ID_SJOB, THEME, DATEWORK, ID_TCH FROM SCIENTIFIC_WORK ";
        if (faculty != 0){
            sql += "OUTER JOIN TEACHERS USING(ID_TCH) WHERE TEACHERS.ID_KAF IN (SELECT ID_KAF FROM DEPARTMENT WHERE ID_FA = " + String.valueOf(faculty) + ")";
        }
        PreparedStatement preStatement = conn.prepareStatement(sql);
        ResultSet result = preStatement.executeQuery();
        ArrayList<ScientificWorkEntity> scientificWorkEntities = new ArrayList<ScientificWorkEntity>();
        while (result.next()) {
            ScientificWorkEntity scientificWorkEntity = new ScientificWorkEntity();
            scientificWorkEntity.setAll(result.getInt(1), result.getString(2), result.getDate(3), result.getInt(4));
            String sql1 = "SELECT FIO FROM TEACHERS WHERE (ID_TCH = " + String.valueOf(scientificWorkEntity.getID_tch()) + ")";
            PreparedStatement preStatement1 = conn.prepareStatement(sql1);
            ResultSet result1 = preStatement1.executeQuery();
            if (result1.next()){
                scientificWorkEntity.setFIOtch(result1.getString(1));
            }else {
                throw new SQLException("(Просмотр научных работ) Преподаватель не найден");
            }
            scientificWorkEntities.add(scientificWorkEntity);
        }
        return scientificWorkEntities;
    }

    public void addScientificWork(ScientificWorkEntity scientificWorkEntity) throws SQLException{
        String sql = "INSERT INTO SCIENTIFIC_WORK(THEME, DATEWORK, ID_TCH)  VALUES(?,?,?)";
        String sql1 = "SELECT ID_TCH FROM TEACHERS WHERE (FIO = '" + scientificWorkEntity.getFIOtch() + "')";
        PreparedStatement preStatement1 = conn.prepareStatement(sql1);
        ResultSet result1 = preStatement1.executeQuery();
        if(result1.next()){
            scientificWorkEntity.setID_tch(result1.getInt(1));
        } else{
            throw new SQLException("(Добавление научной работы) Преподаватель не найден");
        }
        PreparedStatement preparedStatement = conn.prepareStatement(sql);
        preparedStatement.setString(1, scientificWorkEntity.getTheme());
        preparedStatement.setDate(2, scientificWorkEntity.getDateWork());
        preparedStatement.setInt(3,scientificWorkEntity.getID_tch());
        preparedStatement.executeUpdate();
    }

    public ArrayList<String> getFIOtch() throws SQLException {
        String sql = "SELECT FIO FROM teachers ";
        if (faculty != 0){
            sql += "WHERE ID_KAF IN (SELECT ID_KAF FROM DEPARTMENT WHERE ID_FA = " + String.valueOf(faculty) + ")";
        }
        PreparedStatement preStatement = conn.prepareStatement(sql);
        ResultSet result = preStatement.executeQuery();
        ArrayList<String> teachersFIO = new ArrayList<String>();
        while (result.next()) {
            teachersFIO.add(result.getString(1));
        }
        return teachersFIO;
    }

    public void setRole(int role) {
        this.role = role;
    }
}
