package org.example.dataBase.model;

public class FacultetsEntity {
    private int ID_fa;
    private String name;
    private int ID_dean;
    private String FIOdean;

    public void setAll(int ID_fa, String name, int ID_dean){
        this.ID_fa = ID_dean;
        this.name = name;
        this.ID_dean = ID_dean;
    }

    public int getID_fa() {
        return ID_fa;
    }

    public void setID_fa(int ID_fa) {
        this.ID_fa = ID_fa;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getID_dean() {
        return ID_dean;
    }

    public void setID_dean(int ID_dean) {
        this.ID_dean = ID_dean;
    }

    public String getFIOdean() {
        return FIOdean;
    }

    public void setFIOdean(String FIOdean) {
        this.FIOdean = FIOdean;
    }
}
