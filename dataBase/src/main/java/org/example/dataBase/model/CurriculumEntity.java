package org.example.dataBase.model;

public class CurriculumEntity {
    private int ID_t;
    private String titleSub;
    private int ID_sub;
    private int course;
    private int num_gr;
    private String name_fa;
    private int ID_fa;
    private int semester;
    private int year;

    public void setAll(int ID_t, int ID_sub, int course, int num_gr,int ID_fa, int semester, int year){
        this.ID_t = ID_t;
        this.ID_sub = ID_sub;
        this.course = course;
        this.num_gr = num_gr;
        this.ID_fa = ID_fa;
        this.semester = semester;
        this.year = year;
    }

    public int getID_t() {
        return ID_t;
    }

    public void setID_t(int ID_t) {
        this.ID_t = ID_t;
    }

    public String getTitleSub() {
        return titleSub;
    }

    public void setTitleSub(String titleSub) {
        this.titleSub = titleSub;
    }

    public int getID_sub() {
        return ID_sub;
    }

    public void setID_sub(int ID_sub) {
        this.ID_sub = ID_sub;
    }

    public int getCourse() {
        return course;
    }

    public void setCourse(int course) {
        this.course = course;
    }

    public int getNum_gr() {
        return num_gr;
    }

    public void setNum_gr(int num_gr) {
        this.num_gr = num_gr;
    }

    public int getID_fa() {
        return ID_fa;
    }

    public void setID_fa(int ID_fa) {
        this.ID_fa = ID_fa;
    }

    public int getSemester() {
        return semester;
    }

    public void setSemester(int semester) {
        this.semester = semester;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public String getName_fa() {
        return name_fa;
    }

    public void setName_fa(String name_fa) {
        this.name_fa = name_fa;
    }
}
