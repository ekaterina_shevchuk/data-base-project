package org.example.dataBase.model;

public class JobEntity {

    private int ID_job;
    private String titleJob;
    private int wages;

    public void setAll(int ID_job, String titleJob, int wages){
        this.ID_job = ID_job;
        this.titleJob = titleJob;
        this.wages = wages;
    }

    public int getID_job() {
        return ID_job;
    }

    public void setID_job(int ID_job) {
        this.ID_job = ID_job;
    }

    public String getTitleJob() {
        return titleJob;
    }

    public void setTitleJob(String titleJob) {
        this.titleJob = titleJob;
    }

    public int getWages() {
        return wages;
    }

    public void setWages(int wages) {
        this.wages = wages;
    }
}
