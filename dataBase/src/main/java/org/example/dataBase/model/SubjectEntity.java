package org.example.dataBase.model;

public class SubjectEntity {
    private int ID_sub;
    private String title;
    private String occupation;
    private String formOfControl;
    private int numberHours;

    public void setAll(int ID_sub, String title, String formOfControl, int numberHours, String occupation){
        this.ID_sub = ID_sub;
        this.title = title;
        this.occupation = occupation;
        this.formOfControl = formOfControl;
        this.numberHours = numberHours;
    }

    public int getID_sub() {
        return ID_sub;
    }

    public void setID_sub(int ID_sub) {
        this.ID_sub = ID_sub;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getOccupation() {
        return occupation;
    }

    public void setOccupation(String occupation) {
        this.occupation = occupation;
    }

    public String getFormOfControl() {
        return formOfControl;
    }

    public void setFormOfControl(String formOfControl) {
        this.formOfControl = formOfControl;
    }

    public int getNumberHours() {
        return numberHours;
    }

    public void setNumberHours(int numberHours) {
        this.numberHours = numberHours;
    }
}
