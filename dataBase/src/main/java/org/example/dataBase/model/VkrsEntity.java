package org.example.dataBase.model;

public class VkrsEntity {
    private int ID_vkr;
    private int ID_tch;
    private String FIOStudent;
    private int ID_st;
    private String theme;
    private String FIOtch;

    public void setAll(int ID_vkr, int ID_st, String theme, int ID_tch){
        this.ID_vkr = ID_vkr;
        this.ID_tch = ID_tch;
        this.ID_st = ID_st;
        this.theme = theme;
    }

    public void setFIOtch(String FIOtch1){FIOtch = FIOtch1;}

    public String getFIOtch(){return FIOtch;}

    public int getID_tch() {
        return ID_tch;
    }

    public void setID_tch(int ID_tch) {
        this.ID_tch = ID_tch;
    }

    public int getID_vkr() {
        return ID_vkr;
    }

    public void setID_vkr(int ID_vkr) {
        this.ID_vkr = ID_vkr;
    }

    public String getFIOStudent() {
        return FIOStudent;
    }

    public void setFIOStudent(String FIOStudent) {
        this.FIOStudent = FIOStudent;
    }

    public int getID_st() {
        return ID_st;
    }

    public void setID_st(int ID_st) {
        this.ID_st = ID_st;
    }

    public String getTheme() {
        return theme;
    }

    public void setTheme(String theme) {
        this.theme = theme;
    }
}


