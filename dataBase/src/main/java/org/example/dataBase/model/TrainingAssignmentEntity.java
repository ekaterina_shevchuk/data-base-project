package org.example.dataBase.model;

public class TrainingAssignmentEntity {
    private int ID_p;
    private String titleSub;
    private int ID_sub;
    private String FIOtch;
    private int ID_tch;
    private int ID_t;

    public void setAll(int ID_p, int ID_sub, int ID_tch, int ID_t){
        this.ID_p = ID_p;
        this.ID_sub = ID_sub;
        this.ID_tch = ID_tch;
        this.ID_t = ID_t;
    }

    public int getID_p() {
        return ID_p;
    }

    public void setID_p(int ID_p) {
        this.ID_p = ID_p;
    }

    public String getTitleSub() {
        return titleSub;
    }

    public void setTitleSub(String titleSub) {
        this.titleSub = titleSub;
    }

    public int getID_sub() {
        return ID_sub;
    }

    public void setID_sub(int ID_sub) {
        this.ID_sub = ID_sub;
    }

    public String getFIOtch() {
        return FIOtch;
    }

    public void setFIOtch(String FIOtch) {
        this.FIOtch = FIOtch;
    }

    public int getID_tch() {
        return ID_tch;
    }

    public void setID_tch(int ID_tch) {
        this.ID_tch = ID_tch;
    }

    public int getID_t() {
        return ID_t;
    }

    public void setID_t(int ID_t) {
        this.ID_t = ID_t;
    }
}
