package org.example.dataBase.model;

public class DepartmentEntity {
    private int ID_kaf;
    private String title;
    private int ID_fa;
    private String name_fa;

    public void setAll(int ID_kaf, String title, int ID_fa){
        this.ID_kaf = ID_kaf;
        this.title = title;
        this.ID_fa = ID_fa;
    }

    public int getID_kaf() {
        return ID_kaf;
    }

    public void setID_kaf(int ID_kaf) {
        this.ID_kaf = ID_kaf;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getID_fa() {
        return ID_fa;
    }

    public void setID_fa(int ID_fa) {
        this.ID_fa = ID_fa;
    }

    public String getName_fa() {
        return name_fa;
    }

    public void setName_fa(String name_fa) {
        this.name_fa = name_fa;
    }
}
