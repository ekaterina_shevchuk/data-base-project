package org.example.dataBase.model;

import java.sql.Date;

public class ScientificWorkEntity {
    private int ID_sJob;
    private String theme;
    private Date dateWork;
    private int ID_tch;
    private String FIOtch;

    public void setAll(int ID_sJob, String theme, Date dateWork, int ID_tch){
        this.ID_sJob = ID_sJob;
        this.theme = theme;
        this.dateWork = dateWork;
        this.ID_tch = ID_tch;
    }

    public int getID_sJob() {
        return ID_sJob;
    }

    public void setID_sJob(int ID_sJob) {
        this.ID_sJob = ID_sJob;
    }

    public String getTheme() {
        return theme;
    }

    public void setTheme(String theme) {
        this.theme = theme;
    }

    public Date getDateWork() {
        return dateWork;
    }

    public void setDateWork(Date dateWork) {
        this.dateWork = dateWork;
    }

    public int getID_tch() {
        return ID_tch;
    }

    public void setID_tch(int ID_tch) {
        this.ID_tch = ID_tch;
    }

    public String getFIOtch() {
        return FIOtch;
    }

    public void setFIOtch(String FIOtch) {
        this.FIOtch = FIOtch;
    }
}
