package org.example.dataBase.model;

public class ScientificDegreeTchEntity {
    private int ID_uch;
    private String FIOtch;
    private int ID_tch;
    private String titleDegree;

    public void setAll(int ID_uch, int ID_tch, String titleDegree){
        this.ID_uch = ID_uch;
        this.ID_tch = ID_tch;
        this.titleDegree = titleDegree;
    }

    public int getID_uch() {
        return ID_uch;
    }

    public void setID_uch(int ID_uch) {
        this.ID_uch = ID_uch;
    }

    public String getFIOtch() {
        return FIOtch;
    }

    public void setFIOtch(String FIOtch) {
        this.FIOtch = FIOtch;
    }

    public int getID_tch() {
        return ID_tch;
    }

    public void setID_tch(int ID_tch) {
        this.ID_tch = ID_tch;
    }

    public String getTitleDegree() {
        return titleDegree;
    }

    public void setTitleDegree(String titleDegree) {
        this.titleDegree = titleDegree;
    }
}
