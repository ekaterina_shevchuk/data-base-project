package org.example.dataBase.model;

import java.sql.Date;

public class StudentsEntity {
    private int ID_st;
    private String FIO;
    private int num_gr;
    private String gender;
    private Date birthDay;
    private int checkChild;
    private int scholarship;

    public void setAll(int ID_st, String FIO, int num_gr, String gender, Date birthDay, int checkChild, int scholarship){
        this.ID_st = ID_st;
        this.FIO = FIO;
        this.num_gr = num_gr;
        this.gender = gender;
        this.birthDay = birthDay;
        this.checkChild = checkChild;
        this.scholarship = scholarship;
    }

    public int getID_st() {
        return ID_st;
    }

    public void setID_st(int ID_st) {
        this.ID_st = ID_st;
    }

    public String getFIO() {
        return FIO;
    }

    public void setFIO(String FIO) {
        this.FIO = FIO;
    }

    public int getNum_gr() {
        return num_gr;
    }

    public void setNum_gr(int num_gr) {
        this.num_gr = num_gr;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Date getBirthDay() {
        return birthDay;
    }

    public void setBirthDay(Date birthDay) {
        this.birthDay = birthDay;
    }

    public int getCheckChild() {
        return checkChild;
    }

    public void setCheckChild(int checkChild) {
        this.checkChild = checkChild;
    }

    public int getScholarship() {
        return scholarship;
    }

    public void setScholarship(int scholarship) {
        this.scholarship = scholarship;
    }
}
