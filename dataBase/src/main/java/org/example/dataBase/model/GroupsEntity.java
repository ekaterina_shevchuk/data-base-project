package org.example.dataBase.model;

public class GroupsEntity {
    private int num_gr;
    private int course;
    private int ID_fa;
    private String name_fa;

    public void setAll(int num_g, int course, int ID_fa){
        this.num_gr = num_g;
        this.course = course;
        this.ID_fa = ID_fa;
    }

    public int getNum_gr() {
        return num_gr;
    }

    public void setNum_gr(int num_gr) {
        this.num_gr = num_gr;
    }

    public int getCourse() {
        return course;
    }

    public void setCourse(int course) {
        this.course = course;
    }

    public int getID_fa() {
        return ID_fa;
    }

    public void setID_fa(int ID_fa) {
        this.ID_fa = ID_fa;
    }

    public String getName_fa() {
        return name_fa;
    }

    public void setName_fa(String name_fa) {
        this.name_fa = name_fa;
    }
}

