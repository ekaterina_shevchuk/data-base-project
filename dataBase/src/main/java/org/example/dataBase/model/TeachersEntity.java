package org.example.dataBase.model;

import java.sql.Date;

public class TeachersEntity {
    private int ID_tch;
    private String FIO;
    private int ID_kaf;
    private String name_kaf;
    private int ID_job;
    private String name_job;
    private String category;
    private String gender;
    private Date birthDate;
    private int numChildrens;

    public void setAll(int ID_tch, String FIO, int ID_kaf, int ID_job, String category, String gender, Date birthDate, int numChildrens){
        this.ID_tch = ID_tch;
        this.FIO = FIO;
        this.ID_kaf = ID_kaf;
        this.ID_job = ID_job;
        this.category = category;
        this.gender = gender;
        this.birthDate = birthDate;
        this.numChildrens = numChildrens;
    }

    public int getID_tch() {
        return ID_tch;
    }

    public void setID_tch(int ID_tch) {
        this.ID_tch = ID_tch;
    }

    public String getFIO() {
        return FIO;
    }

    public void setFIO(String FIO) {
        this.FIO = FIO;
    }

    public int getID_kaf() {
        return ID_kaf;
    }

    public void setID_kaf(int ID_kaf) {
        this.ID_kaf = ID_kaf;
    }

    public int getID_job() {
        return ID_job;
    }

    public void setID_job(int ID_job) {
        this.ID_job = ID_job;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public int getNumChildrens() {
        return numChildrens;
    }

    public void setNumChildrens(int numChildrens) {
        this.numChildrens = numChildrens;
    }

    public String getName_kaf() {
        return name_kaf;
    }

    public void setName_kaf(String name_kaf) {
        this.name_kaf = name_kaf;
    }

    public String getName_job() {
        return name_job;
    }

    public void setName_job(String name_job) {
        this.name_job = name_job;
    }
}
