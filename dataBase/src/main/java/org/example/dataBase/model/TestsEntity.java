package org.example.dataBase.model;

import java.sql.Date;

public class TestsEntity {
    private int ID_test;
    private String subject;
    private String FIOst;
    private int ID_st;
    private int num_gr;
    private String FIOtch;
    private int ID_tch;
    private int mark;
    private Date dateTest;
    private int ID_t;

    public void setAll( int ID_test,int ID_st, int ID_tch, int mark, Date dateTest, int ID_t){
        this.ID_test = ID_test;
        this.ID_st = ID_st;
        this.num_gr = num_gr;
        this.ID_tch = ID_tch;
        this.mark = mark;
        this.dateTest = dateTest;
        this.ID_t = ID_t;
    }

    public int getID_test() {
        return ID_test;
    }

    public void setID_test(int ID_test) {
        this.ID_test = ID_test;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getFIOst() {
        return FIOst;
    }

    public void setFIOst(String FIOst) {
        this.FIOst = FIOst;
    }

    public int getID_st() {
        return ID_st;
    }

    public void setID_st(int ID_st) {
        this.ID_st = ID_st;
    }

    public int getNum_gr() {
        return num_gr;
    }

    public void setNum_gr(int num_gr) {
        this.num_gr = num_gr;
    }

    public String getFIOtch() {
        return FIOtch;
    }

    public void setFIOtch(String FIOtch) {
        this.FIOtch = FIOtch;
    }

    public int getID_tch() {
        return ID_tch;
    }

    public void setID_tch(int ID_tch) {
        this.ID_tch = ID_tch;
    }

    public int getMark() {
        return mark;
    }

    public void setMark(int mark) {
        this.mark = mark;
    }

    public Date getDateTest() {
        return dateTest;
    }

    public void setDateTest(Date dateTest) {
        this.dateTest = dateTest;
    }

    public int getID_t() {
        return ID_t;
    }

    public void setID_t(int ID_t) {
        this.ID_t = ID_t;
    }
}
