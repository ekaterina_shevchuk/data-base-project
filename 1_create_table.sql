CREATE TABLE  "FACULTETS"
   (	"ID_FA" NUMBER(10,0),
	"NAME" VARCHAR2(100),
	"ID_DEAN" NUMBER(10,0),
	 CONSTRAINT "FAID" PRIMARY KEY ("ID_FA") ENABLE
   ) ;
CREATE TABLE  "GROUPS"
   (	"NUM_GR" NUMBER(10,0),
	"COURSE" NUMBER(10,0),
	"ID_FA" NUMBER(10,0),
	 CONSTRAINT "GRID" PRIMARY KEY ("NUM_GR") ENABLE
   ) ;
ALTER TABLE  "GROUPS" ADD CONSTRAINT "GRFA" FOREIGN KEY ("ID_FA")
	  REFERENCES  "FACULTETS" ("ID_FA") ENABLE;
CREATE TABLE  "STUDENTS"
   (	"ID_ST" NUMBER(10,0),
	"FIO" VARCHAR2(70),
	"NUM_GR" NUMBER(10,0),
	"GENDER" VARCHAR2(2),
	"BIRTHDAY" DATE,
	"CHECKCHILD" NUMBER(2,0),
	"SCHOLARSHIP" NUMBER(10,0),
	 CONSTRAINT "STID" PRIMARY KEY ("ID_ST") ENABLE
   ) ;
ALTER TABLE  "STUDENTS" ADD CONSTRAINT "STGR" FOREIGN KEY ("NUM_GR")
	  REFERENCES  "GROUPS" ("NUM_GR") ENABLE;
CREATE TABLE  "DEPARTMENT"
   (	"ID_KAF" NUMBER(10,0),
	"TITLE" VARCHAR2(70) NOT NULL ENABLE,
	"ID_FA" NUMBER(10,0),
	 CONSTRAINT "DEPID" PRIMARY KEY ("ID_KAF") ENABLE
   ) ;
ALTER TABLE  "DEPARTMENT" ADD CONSTRAINT "DEPFA" FOREIGN KEY ("ID_FA")
	  REFERENCES  "FACULTETS" ("ID_FA") ENABLE;
CREATE TABLE  "JOB"
   (	"ID_JOB" NUMBER(10,0),
	"TITLEJOB" VARCHAR2(50),
	"WAGES" NUMBER(10,0),
	 CONSTRAINT "JOBID" PRIMARY KEY ("ID_JOB") ENABLE
   ) ;
CREATE TABLE  "TEACHERS"
   (	"ID_TCH" NUMBER(10,0),
	"FIO" VARCHAR2(70) NOT NULL ENABLE,
	"ID_KAF" NUMBER(10,0),
	"ID_JOB" NUMBER(10,0),
	"CATEGORY" VARCHAR2(50),
	"GENDER" VARCHAR2(2),
	"BIRTHDATE" DATE,
	"NUMCHILDRENS" NUMBER(10,0),
	"PASS" VARCHAR2(15),
	 CONSTRAINT "TCHID" PRIMARY KEY ("ID_TCH") ENABLE
   ) ;
ALTER TABLE  "TEACHERS" ADD CONSTRAINT "TCHJOB" FOREIGN KEY ("ID_JOB")
	  REFERENCES  "JOB" ("ID_JOB") ENABLE;
ALTER TABLE  "TEACHERS" ADD CONSTRAINT "TCHKAF" FOREIGN KEY ("ID_KAF")
	  REFERENCES  "DEPARTMENT" ("ID_KAF") ENABLE;
CREATE TABLE  "SCIENTIFIC_WORK"
   (	"ID_SJOB" NUMBER(10,0),
	"THEME" VARCHAR2(160),
	"DATEWORK" DATE,
	"ID_TCH" NUMBER(10,0),
	 CONSTRAINT "SCWORK_ID" PRIMARY KEY ("ID_SJOB") ENABLE
   ) ;
ALTER TABLE  "SCIENTIFIC_WORK" ADD CONSTRAINT "SCWORKTCH" FOREIGN KEY ("ID_TCH")
	  REFERENCES  "TEACHERS" ("ID_TCH") ENABLE;
CREATE TABLE  "SCIENTIFIC_DEGREE_TCH"
   (	"ID_UCH" NUMBER(10,0),
	"ID_TCH" NUMBER(10,0),
	"TITLEDEGREE" VARCHAR2(50),
	 CONSTRAINT "DEGID" PRIMARY KEY ("ID_UCH") ENABLE
   ) ;
ALTER TABLE  "SCIENTIFIC_DEGREE_TCH" ADD CONSTRAINT "DEGTCH" FOREIGN KEY ("ID_TCH")
	  REFERENCES  "TEACHERS" ("ID_TCH") ENABLE;
CREATE TABLE  "VKRS"
   (	"ID_VKR" NUMBER(10,0),
	"ID_ST" NUMBER(10,0),
	"THEME" VARCHAR2(150),
	"ID_TCH" NUMBER(10,0),
	 CONSTRAINT "VKRID" PRIMARY KEY ("ID_VKR") ENABLE
   ) ;
ALTER TABLE  "VKRS" ADD CONSTRAINT "VKRST" FOREIGN KEY ("ID_ST")
	  REFERENCES  "STUDENTS" ("ID_ST") ENABLE;
ALTER TABLE  "VKRS" ADD CONSTRAINT "VKRTCH" FOREIGN KEY ("ID_TCH")
	  REFERENCES  "TEACHERS" ("ID_TCH") ENABLE;
CREATE TABLE  "SUBJECT"
   (	"ID_SUB" NUMBER(10,0),
	"TITLE" VARCHAR2(85),
	"FORMOFCONTROL" VARCHAR2(40),
	"NUMBERHOURS" NUMBER(3,0),
	"OCCUPATION" VARCHAR2(40),
	 CONSTRAINT "SIB_ID" PRIMARY KEY ("ID_SUB") ENABLE
   ) ;
CREATE TABLE  "CURRICULUM"
   (	"ID_T" NUMBER(10,0),
	"ID_SUB" NUMBER(10,0),
	"COURSE" NUMBER(5,0),
	"NUM_GR" NUMBER(10,0),
	"ID_FA" NUMBER(10,0),
	"SEMESTER" NUMBER(10,0),
	"YEAR" NUMBER(4,0),
	 CONSTRAINT "IDCUR" PRIMARY KEY ("ID_T") ENABLE
   ) ;
ALTER TABLE  "CURRICULUM" ADD CONSTRAINT "FACUR" FOREIGN KEY ("ID_FA")
	  REFERENCES  "FACULTETS" ("ID_FA") ENABLE;
ALTER TABLE  "CURRICULUM" ADD CONSTRAINT "GRCUR" FOREIGN KEY ("NUM_GR")
	  REFERENCES  "GROUPS" ("NUM_GR") ENABLE;
ALTER TABLE  "CURRICULUM" ADD CONSTRAINT "SUBCUR" FOREIGN KEY ("ID_SUB")
	  REFERENCES  "SUBJECT" ("ID_SUB") ENABLE;
CREATE TABLE  "TRAINING_ASSIGNMENT"
   (	"ID_P" NUMBER(10,0),
	"ID_SUB" NUMBER(10,0),
	"ID_TCH" NUMBER(10,0),
	"ID_T" NUMBER(10,0),
	 CONSTRAINT "TRAIN_ID" PRIMARY KEY ("ID_P") ENABLE
   ) ;
ALTER TABLE  "TRAINING_ASSIGNMENT" ADD CONSTRAINT "IDSTRAIN" FOREIGN KEY ("ID_SUB")
	  REFERENCES  "SUBJECT" ("ID_SUB") ENABLE;
ALTER TABLE  "TRAINING_ASSIGNMENT" ADD CONSTRAINT "TRT" FOREIGN KEY ("ID_T")
	  REFERENCES  "CURRICULUM" ("ID_T") ENABLE;
ALTER TABLE  "TRAINING_ASSIGNMENT" ADD CONSTRAINT "TRTCH" FOREIGN KEY ("ID_TCH")
	  REFERENCES  "TEACHERS" ("ID_TCH") ENABLE;
CREATE TABLE  "TESTS"
   (	"ID_TEST" NUMBER(10,0),
	"ID_ST" NUMBER(10,0),
	"ID_TCH" NUMBER(10,0),
	"MARK" NUMBER(1,0),
	"DATETEST" DATE,
	"ID_T" NUMBER(10,0),
	 CONSTRAINT "TESTID" PRIMARY KEY ("ID_TEST") ENABLE
   ) ;
ALTER TABLE  "TESTS" ADD CONSTRAINT "TESTST" FOREIGN KEY ("ID_ST")
	  REFERENCES  "STUDENTS" ("ID_ST") ENABLE;
ALTER TABLE  "TESTS" ADD CONSTRAINT "TESTT" FOREIGN KEY ("ID_T")
	  REFERENCES  "CURRICULUM" ("ID_T") ENABLE;
ALTER TABLE  "TESTS" ADD CONSTRAINT "TESTTCH" FOREIGN KEY ("ID_TCH")
	  REFERENCES  "TEACHERS" ("ID_TCH") ENABLE;
