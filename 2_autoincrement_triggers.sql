CREATE OR REPLACE TRIGGER tr_teachers_table before INSERT ON TEACHERS FOR each row
BEGIN
  SELECT sq_teachers_table.NEXTVAL
  INTO :new.ID_TCH
  FROM dual;
END;

CREATE OR REPLACE TRIGGER tr_facultets_table before INSERT ON FACULTETS FOR each row
BEGIN
  SELECT sq_facultets_table.NEXTVAL
  INTO :new.ID_FA
  FROM dual;
END;

CREATE OR REPLACE TRIGGER tr_departments_table before INSERT ON DEPARTMENT FOR each row
BEGIN
  SELECT sq_departments_table.NEXTVAL
  INTO :new.ID_KAF
  FROM dual;
END;

CREATE OR REPLACE TRIGGER tr_students_table before INSERT ON STUDENTS FOR each row
BEGIN
  SELECT sq_students_table.NEXTVAL
  INTO :new.ID_ST
  FROM dual;
END;

CREATE OR REPLACE TRIGGER tr_vkrs_table before INSERT ON VKRS FOR each row
BEGIN
  SELECT sq_vkrs_table.NEXTVAL
  INTO :new.ID_VKR
  FROM dual;
END;

CREATE OR REPLACE TRIGGER tr_jobs_table before INSERT ON JOB FOR each row
BEGIN
  SELECT sq_jobs_table.NEXTVAL
  INTO :new.ID_JOB
  FROM dual;
END;

CREATE OR REPLACE TRIGGER tr_sWork_table before INSERT ON SCIENTIFIC_WORK FOR each row
BEGIN
  SELECT sq_sWork_table.NEXTVAL
  INTO :new.ID_SJOB
  FROM dual;
END;

CREATE OR REPLACE TRIGGER tr_sDegree_table before INSERT ON SCIENTIFIC_DEGREE_TCH FOR each row
BEGIN
  SELECT sq_sDegree_table.NEXTVAL
  INTO :new.ID_UCH    
  FROM dual;
END;

CREATE OR REPLACE TRIGGER tr_curriculum_table before INSERT ON CURRICULUM FOR each row
BEGIN
  SELECT sq_curriculum_table.NEXTVAL
  INTO :new.ID_T
  FROM dual;
END;

CREATE OR REPLACE TRIGGER tr_subj_table before INSERT ON SUBJECT FOR each row
BEGIN
  SELECT sq_subj_table.NEXTVAL
  INTO :new.ID_SUB
  FROM dual;
END;

CREATE OR REPLACE TRIGGER tr_tests_table before INSERT ON TESTS FOR each row
BEGIN
  SELECT sq_tests_table.NEXTVAL
  INTO :new.ID_TEST
  FROM dual;
END;

CREATE OR REPLACE TRIGGER tr_trainAss_table before INSERT ON TRAINING_ASSIGNMENT FOR each row
BEGIN
  SELECT sq_trainAss_table.NEXTVAL
  INTO :new.ID_P
  FROM dual;
END;
